/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Metodos;

/**
 *
 * @author omarm
 */
public class user {
    int identification;
    String password;
    int idPerson;
    int idTypeOfUser;
    int idHotel;

    public user(int identification, String password, int idPerson, int idTypeOfUser, int idHotel) {
        this.identification = identification;
        this.password = password;
        this.idPerson = idPerson;
        this.idTypeOfUser = idTypeOfUser;
        this.idHotel = idHotel;
    }

    public user() {
    }

    public int getIdentification() {
        return identification;
    }

    public void setIdentification(int identification) {
        this.identification = identification;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getIdHotel() {
        return idHotel;
    }

    public void setIdHotel(int idHotel) {
        this.idHotel = idHotel;
    }
    
    

    public int getIdPerson() {
        return idPerson;
    }

    public void setIdPerson(int idPerson) {
        this.idPerson = idPerson;
    }

    public int getIdTypeOfUser() {
        return idTypeOfUser;
    }

    public void setIdTypeOfUser(int idTypeOfUser) {
        this.idTypeOfUser = idTypeOfUser;
    }
    
    
}
