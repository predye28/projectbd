/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Metodos;

/**
 *
 * @author omarm
 */
public class personConsulta {
    String name;
    String apellido;
    String cedIdentificacion;
    String nacionalidad;
    String checkin;
    String checkout;
    int price;
    String nameHabitacion;

    public personConsulta() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getCedIdentificacion() {
        return cedIdentificacion;
    }

    public void setCedIdentificacion(String cedIdentificacion) {
        this.cedIdentificacion = cedIdentificacion;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public String getCheckin() {
        return checkin;
    }

    public void setCheckin(String checkin) {
        this.checkin = checkin;
    }

    public String getCheckout() {
        return checkout;
    }

    public void setCheckout(String checkout) {
        this.checkout = checkout;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getNameHabitacion() {
        return nameHabitacion;
    }

    public void setNameHabitacion(String nameHabitacion) {
        this.nameHabitacion = nameHabitacion;
    }
    
}
