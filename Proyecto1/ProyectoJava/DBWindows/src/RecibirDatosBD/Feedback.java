/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package RecibirDatosBD;

/**
 *
 * @author omarm
 */
public class Feedback {
    int idFeedBack;
    String commentary;
    int idRating;

    public Feedback(int idFeedBack, String commentary, int idRating) {
        this.idFeedBack = idFeedBack;
        this.commentary = commentary;
        this.idRating = idRating;
    }

    public Feedback(int idFeedBack, int idRating) {
        this.idFeedBack = idFeedBack;
        this.idRating = idRating;
    }
    
    public Feedback() {
    }

    public int getIdFeedBack() {
        return idFeedBack;
    }

    public void setIdFeedBack(int idFeedBack) {
        this.idFeedBack = idFeedBack;
    }

    public String getCommentary() {
        return commentary;
    }

    public void setCommentary(String commentary) {
        this.commentary = commentary;
    }

    public int getIdRating() {
        return idRating;
    }

    public void setIdRating(int idRating) {
        this.idRating = idRating;
    }
    
}
