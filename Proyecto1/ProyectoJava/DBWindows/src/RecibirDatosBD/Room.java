/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package RecibirDatosBD;

/**
 *
 * @author omarm
 */
public class Room {
    int idRoom;
    String name;
    String discountCode;
    float discountPercentaje;
    int capacity;
    int price;
    int idHotel;

    public Room(int idRoom, String name, String discountCode, float discountPercentaje, int capacity, int price, int idHotel) {
        this.idRoom = idRoom;
        this.name = name;
        this.discountCode = discountCode;
        this.discountPercentaje = discountPercentaje;
        this.capacity = capacity;
        this.price = price;
        this.idHotel = idHotel;
    }

    public Room() {
    }
    
    public int getIdRoom() {
        return idRoom;
    }

    public void setIdRoom(int idRoom) {
        this.idRoom = idRoom;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDiscountCode() {
        return discountCode;
    }

    public void setDiscountCode(String discountCode) {
        this.discountCode = discountCode;
    }

    public float getDiscountPercentaje() {
        return discountPercentaje;
    }

    public void setDiscountPercentaje(float discountPercentaje) {
        this.discountPercentaje = discountPercentaje;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getIdHotel() {
        return idHotel;
    }

    public void setIdHotel(int idHotel) {
        this.idHotel = idHotel;
    }
    
    
}
