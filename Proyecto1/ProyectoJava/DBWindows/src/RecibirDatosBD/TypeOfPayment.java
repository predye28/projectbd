/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package RecibirDatosBD;

/**
 *
 * @author omarm
 */
public class TypeOfPayment {
    int idTypeOfPayment;
    String name;

    public TypeOfPayment(int idTypeOfPayment, String name) {
        this.idTypeOfPayment = idTypeOfPayment;
        this.name = name;
    }

    public int getIdTypeOfPayment() {
        return idTypeOfPayment;
    }

    public void setIdTypeOfPayment(int idTypeOfPayment) {
        this.idTypeOfPayment = idTypeOfPayment;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
}
