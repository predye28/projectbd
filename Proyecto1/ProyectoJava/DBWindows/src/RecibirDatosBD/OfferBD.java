/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package RecibirDatosBD;

/**
 *
 * @author omarm
 */
public class OfferBD {
    int id;

    
    String name;
    int diasMinimos;
    String descripcion;
    String starDate;
    String finishDate;
    int idRoom;
    int idHotel;
    
    public OfferBD(String name, int diasMinimos, String descripcion, String starDate, String finishDate, int idRoom, int idHotel) {
        this.name = name;
        this.diasMinimos = diasMinimos;
        this.descripcion = descripcion;
        this.starDate = starDate;
        this.finishDate = finishDate;
        this.idRoom = idRoom;
        this.idHotel = idHotel;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    public OfferBD() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDiasMinimos() {
        return diasMinimos;
    }

    public void setDiasMinimos(int diasMinimos) {
        this.diasMinimos = diasMinimos;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getStarDate() {
        return starDate;
    }

    public void setStarDate(String starDate) {
        this.starDate = starDate;
    }

    public String getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(String finishDate) {
        this.finishDate = finishDate;
    }

    public int getIdRoom() {
        return idRoom;
    }

    public void setIdRoom(int idRoom) {
        this.idRoom = idRoom;
    }

    public int getIdHotel() {
        return idHotel;
    }

    public void setIdHotel(int idHotel) {
        this.idHotel = idHotel;
    }
    
}
