/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package Vista;

import Metodos.person;
import Connect.ConnectDBUser1;
import Funciones.Validaciones;
import RecibirDatosBD.TypeIdentificacion;
import java.awt.Color;
import static java.awt.image.ImageObserver.ABORT;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author jerem
 */
public class RegistroUsuario3 extends javax.swing.JFrame {

    int xMouse, yMouse;
    static person per = new person();
    static ArrayList<TypeIdentificacion> typeId = new ArrayList<>();
    ConnectDBUser1 conex = new ConnectDBUser1();
    Validaciones vali = new Validaciones();
    public RegistroUsuario3(person pers) throws SQLException {
        this.per = pers;
        this.typeId = conex.getTypeId();
        initComponents();
        rellenarDatos();
    }
    public void rellenarDatos(){
        for(TypeIdentificacion eleTypeId : typeId){
            jComboBox2.addItem(eleTypeId.getMask());
        }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        exitbtn = new javax.swing.JPanel();
        exitxt = new javax.swing.JLabel();
        TitleUser1 = new javax.swing.JLabel();
        SelectGentxt1 = new javax.swing.JLabel();
        jComboBox2 = new javax.swing.JComboBox<>();
        SelectGentxt2 = new javax.swing.JLabel();
        Usertxt1 = new javax.swing.JTextField();
        SelectGentxt3 = new javax.swing.JLabel();
        Usertxt2 = new javax.swing.JTextField();
        SelectGentxt4 = new javax.swing.JLabel();
        Usertxt3 = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        jSeparator3 = new javax.swing.JSeparator();
        jSeparator4 = new javax.swing.JSeparator();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setLocationByPlatform(true);
        setUndecorated(true);
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setPreferredSize(new java.awt.Dimension(600, 500));
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel3.setBackground(new java.awt.Color(154, 32, 140));
        jPanel3.setToolTipText("");
        jPanel3.setPreferredSize(new java.awt.Dimension(220, 500));

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 220, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 500, Short.MAX_VALUE)
        );

        jPanel2.add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 0, -1, -1));

        jPanel4.setBackground(new java.awt.Color(245, 198, 236));
        jPanel4.setPreferredSize(new java.awt.Dimension(560, 40));
        jPanel4.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                jPanel4MouseDragged(evt);
            }
        });
        jPanel4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jPanel4MousePressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 560, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 40, Short.MAX_VALUE)
        );

        jPanel2.add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 0, -1, -1));

        exitbtn.setBackground(new java.awt.Color(245, 198, 236));
        exitbtn.setPreferredSize(new java.awt.Dimension(40, 40));

        exitxt.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        exitxt.setText("X");
        exitxt.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                exitxtMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                exitxtMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                exitxtMouseExited(evt);
            }
        });

        javax.swing.GroupLayout exitbtnLayout = new javax.swing.GroupLayout(exitbtn);
        exitbtn.setLayout(exitbtnLayout);
        exitbtnLayout.setHorizontalGroup(
            exitbtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, exitbtnLayout.createSequentialGroup()
                .addComponent(exitxt, javax.swing.GroupLayout.DEFAULT_SIZE, 34, Short.MAX_VALUE)
                .addContainerGap())
        );
        exitbtnLayout.setVerticalGroup(
            exitbtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(exitxt, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE)
        );

        jPanel2.add(exitbtn, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        TitleUser1.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        TitleUser1.setText("Registrar datos del usuario");
        jPanel2.add(TitleUser1, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 50, -1, -1));

        SelectGentxt1.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        SelectGentxt1.setText("Correo electrónico");
        jPanel2.add(SelectGentxt1, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 240, 170, -1));

        jPanel2.add(jComboBox2, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 110, 190, -1));

        SelectGentxt2.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        SelectGentxt2.setText("Seleccione su tipo de identificación");
        jPanel2.add(SelectGentxt2, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 90, 170, -1));

        Usertxt1.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        Usertxt1.setForeground(new java.awt.Color(102, 102, 102));
        Usertxt1.setText("Ingrese su correo");
        Usertxt1.setBorder(null);
        Usertxt1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Usertxt1MousePressed(evt);
            }
        });
        Usertxt1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Usertxt1ActionPerformed(evt);
            }
        });
        jPanel2.add(Usertxt1, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 260, 190, -1));

        SelectGentxt3.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        SelectGentxt3.setText("Identificación");
        jPanel2.add(SelectGentxt3, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 140, 170, -1));

        Usertxt2.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        Usertxt2.setForeground(new java.awt.Color(102, 102, 102));
        Usertxt2.setText("Ingrese su identificación");
        Usertxt2.setBorder(null);
        Usertxt2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Usertxt2MousePressed(evt);
            }
        });
        Usertxt2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Usertxt2ActionPerformed(evt);
            }
        });
        jPanel2.add(Usertxt2, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 160, 190, -1));

        SelectGentxt4.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        SelectGentxt4.setText("Télefono");
        jPanel2.add(SelectGentxt4, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 190, 170, -1));

        Usertxt3.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        Usertxt3.setForeground(new java.awt.Color(102, 102, 102));
        Usertxt3.setText("Ingrese su número");
        Usertxt3.setBorder(null);
        Usertxt3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Usertxt3MousePressed(evt);
            }
        });
        Usertxt3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Usertxt3ActionPerformed(evt);
            }
        });
        jPanel2.add(Usertxt3, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 210, 190, -1));

        jButton1.setBackground(new java.awt.Color(245, 198, 236));
        jButton1.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jButton1.setText("Atrás");
        jButton1.setPreferredSize(new java.awt.Dimension(90, 25));
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 390, -1, -1));

        jButton2.setBackground(new java.awt.Color(245, 198, 236));
        jButton2.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        jButton2.setText("Siguiente");
        jButton2.setPreferredSize(new java.awt.Dimension(90, 25));
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 390, -1, -1));
        jPanel2.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 180, -1, -1));

        jSeparator2.setForeground(new java.awt.Color(0, 0, 0));
        jPanel2.add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 280, 190, 10));

        jSeparator3.setForeground(new java.awt.Color(0, 0, 0));
        jPanel2.add(jSeparator3, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 180, 190, 10));

        jSeparator4.setForeground(new java.awt.Color(0, 0, 0));
        jPanel2.add(jSeparator4, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 230, 190, 10));

        getContentPane().add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void exitxtMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_exitxtMouseClicked
        System.exit(0);
    }//GEN-LAST:event_exitxtMouseClicked

    private void exitxtMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_exitxtMouseEntered
        exitbtn.setBackground(Color.red);
    }//GEN-LAST:event_exitxtMouseEntered

    private void exitxtMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_exitxtMouseExited
        exitbtn.setBackground(new Color(245,198,236));
    }//GEN-LAST:event_exitxtMouseExited

    private void jPanel4MouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel4MouseDragged
        int x = evt.getXOnScreen();
        int y = evt.getYOnScreen();
        this.setLocation(x-xMouse, y -yMouse);
    }//GEN-LAST:event_jPanel4MouseDragged

    private void jPanel4MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel4MousePressed
       xMouse = evt.getX();
       yMouse = evt.getY();
    }//GEN-LAST:event_jPanel4MousePressed

    private void Usertxt1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Usertxt1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_Usertxt1ActionPerformed

    private void Usertxt2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Usertxt2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_Usertxt2ActionPerformed

    private void Usertxt3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Usertxt3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_Usertxt3ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        RegistroUsuario2 x = null;
        try {
            x = new RegistroUsuario2(per);
        } catch (SQLException ex) {
            Logger.getLogger(RegistroUsuario3.class.getName()).log(Level.SEVERE, null, ex);
        }
        x.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jButton1ActionPerformed
    public boolean validarIdenti(String ident){
        //ArrayList<Mascara> arrayMascara = conex.getMascara();
        
        return ident.matches("\\d-0\\d{3}-0\\d{3}");
    }
    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        //VALIDAR
        String userInput = Usertxt3.getText();
        boolean validar = false;
        validar = validarIdenti(Usertxt2.getText());
        if(Usertxt2.getText().isEmpty() || Usertxt2.getText().equals("Ingrese su identificación")!= Usertxt1.getText().equals(ABORT)){
                 JOptionPane.showMessageDialog(null, "¡Opción inválida! Espacios vacios.", "Alerta", JOptionPane.WARNING_MESSAGE); 
        }
        else if(Usertxt3.getText().isEmpty() || Usertxt3.getText().equals("Ingrese su número")!= Usertxt3.getText().equals(ABORT)){
                 JOptionPane.showMessageDialog(null, "¡Opción inválida! Espacios vacios.", "Alerta", JOptionPane.WARNING_MESSAGE); 
        }
        else if(Usertxt1.getText().isEmpty() || Usertxt1.getText().equals("Ingrese su correo")!= Usertxt1.getText().equals(ABORT)){
                 JOptionPane.showMessageDialog(null, "¡Opción inválida! Espacios vacios.", "Alerta", JOptionPane.WARNING_MESSAGE); 
        }else if(!vali.validarNumero(Usertxt3.getText())){
            JOptionPane.showMessageDialog(null, "¡Opción inválida! El numero de telefono debe ser un numero.", "Alerta", JOptionPane.WARNING_MESSAGE); 
        }else if(!validar){
            JOptionPane.showMessageDialog(null, "¡Opción inválida! El formato de la cedula es incorrecto.", "Alerta", JOptionPane.WARNING_MESSAGE);
        }
        else if(userInput.length() != 8){
            JOptionPane.showMessageDialog(null, "¡Opción inválida! El numero debe de ser de 8 numeros.", "Alerta", JOptionPane.WARNING_MESSAGE); 
        }else if(!vali.ValidarEmail(Usertxt1.getText())){
            JOptionPane.showMessageDialog(null, "¡Opción inválida! El correo no tiene formato esperado.", "Alerta", JOptionPane.WARNING_MESSAGE); 
        }
        else{
            //SETEAR DATOS A PERSON
            for(TypeIdentificacion elety : typeId){
                if(jComboBox2.getSelectedItem().toString() == elety.getMask()){
                    per.setIdTypeId(elety.getId());
                }
            }
            per.setNumIdentification(Usertxt2.getText());
            per.setPhoneNumber(Usertxt3.getText());
            per.setEmail(Usertxt1.getText());

            RegistroUsuario4 x = null;
            try {
                x = new RegistroUsuario4(per);
            } catch (SQLException ex) {
                Logger.getLogger(RegistroUsuario3.class.getName()).log(Level.SEVERE, null, ex);
            }
            x.setVisible(true);
            this.dispose();
        }
        
    }//GEN-LAST:event_jButton2ActionPerformed

    private void Usertxt2MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Usertxt2MousePressed
        if (Usertxt2.getText().equals("Ingrese su identificación")){
            Usertxt2.setText("");
            Usertxt2.setForeground(Color.black);

        }
        if(String.valueOf(Usertxt3.getText()).isEmpty()){
            Usertxt3.setText("Ingrese su número");
            Usertxt3.setForeground(Color.gray);
        }
        if(String.valueOf(Usertxt1.getText()).isEmpty()){
            Usertxt1.setText("Ingrese su correo");
            Usertxt1.setForeground(Color.gray);
        }
        
    }//GEN-LAST:event_Usertxt2MousePressed

    private void Usertxt3MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Usertxt3MousePressed
       
        if (Usertxt3.getText().equals("Ingrese su número")){
            Usertxt3.setText("");
            Usertxt3.setForeground(Color.black);

        }
        if(String.valueOf(Usertxt2.getText()).isEmpty()){
            Usertxt2.setText("Ingrese su identificación");
            Usertxt2.setForeground(Color.gray);
        }
        if(String.valueOf(Usertxt1.getText()).isEmpty()){
            Usertxt1.setText("Ingrese su correo");
            Usertxt1.setForeground(Color.gray);
        }
        
  
    }//GEN-LAST:event_Usertxt3MousePressed

    private void Usertxt1MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Usertxt1MousePressed
         
        if (Usertxt1.getText().equals("Ingrese su correo")){
            Usertxt1.setText("");
            Usertxt1.setForeground(Color.black);

        }
        if(String.valueOf(Usertxt2.getText()).isEmpty()){
            Usertxt2.setText("Ingrese su identificación");
            Usertxt2.setForeground(Color.gray);
        }
        if(String.valueOf(Usertxt3.getText()).isEmpty()){
            Usertxt3.setText("Ingrese su número");
            Usertxt3.setForeground(Color.gray);
        }
        
    }//GEN-LAST:event_Usertxt1MousePressed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(RegistroUsuario3.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(RegistroUsuario3.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(RegistroUsuario3.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(RegistroUsuario3.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new RegistroUsuario3(per).setVisible(true);
                } catch (SQLException ex) {
                    Logger.getLogger(RegistroUsuario3.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel SelectGentxt1;
    private javax.swing.JLabel SelectGentxt2;
    private javax.swing.JLabel SelectGentxt3;
    private javax.swing.JLabel SelectGentxt4;
    private javax.swing.JLabel TitleUser1;
    private javax.swing.JTextField Usertxt1;
    private javax.swing.JTextField Usertxt2;
    private javax.swing.JTextField Usertxt3;
    private javax.swing.JPanel exitbtn;
    private javax.swing.JLabel exitxt;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JComboBox<String> jComboBox2;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    // End of variables declaration//GEN-END:variables
}
