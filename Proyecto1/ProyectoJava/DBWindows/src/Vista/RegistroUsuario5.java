/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package Vista;

import Connect.ConnectDBAdmin;
import Metodos.person;
import Connect.ConnectDBUser1;
import Funciones.Validaciones;
import Metodos.cifrado;
import java.awt.Color;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author jerem
 */
public class RegistroUsuario5 extends javax.swing.JFrame {
    int xMouse, yMouse;
    static person per = new person();
    ConnectDBUser1 conex = new ConnectDBUser1();
    cifrado cifra = new cifrado();
    static ConnectDBAdmin coneAdmin = new ConnectDBAdmin();
    ConnectDBUser1 cone = new ConnectDBUser1();
    Validaciones vali = new Validaciones();
    public RegistroUsuario5(person pers) throws SQLException, ParseException {
        this.per = pers;
        initComponents();
        
        //registrar nationality
    }
    public int registarPerson() throws SQLException, ParseException{
        return(conex.insertPerson(per));
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        exitbtn = new javax.swing.JPanel();
        exitxt = new javax.swing.JLabel();
        TitleUser1 = new javax.swing.JLabel();
        SelectGentxt5 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        jPasswordField3 = new javax.swing.JPasswordField();
        SelectGentxt6 = new javax.swing.JLabel();
        jSeparator3 = new javax.swing.JSeparator();
        Usertxt3 = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setLocationByPlatform(true);
        setUndecorated(true);
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setPreferredSize(new java.awt.Dimension(600, 500));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel3.setBackground(new java.awt.Color(154, 32, 140));
        jPanel3.setToolTipText("");
        jPanel3.setPreferredSize(new java.awt.Dimension(220, 500));

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 220, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 500, Short.MAX_VALUE)
        );

        jPanel1.add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 0, -1, -1));

        jPanel4.setBackground(new java.awt.Color(245, 198, 236));
        jPanel4.setPreferredSize(new java.awt.Dimension(560, 40));
        jPanel4.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                jPanel4MouseDragged(evt);
            }
        });
        jPanel4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jPanel4MousePressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 560, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 40, Short.MAX_VALUE)
        );

        jPanel1.add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 0, -1, -1));

        exitbtn.setBackground(new java.awt.Color(245, 198, 236));
        exitbtn.setPreferredSize(new java.awt.Dimension(40, 40));

        exitxt.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        exitxt.setText("X");
        exitxt.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                exitxtMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                exitxtMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                exitxtMouseExited(evt);
            }
        });

        javax.swing.GroupLayout exitbtnLayout = new javax.swing.GroupLayout(exitbtn);
        exitbtn.setLayout(exitbtnLayout);
        exitbtnLayout.setHorizontalGroup(
            exitbtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, exitbtnLayout.createSequentialGroup()
                .addComponent(exitxt, javax.swing.GroupLayout.DEFAULT_SIZE, 34, Short.MAX_VALUE)
                .addContainerGap())
        );
        exitbtnLayout.setVerticalGroup(
            exitbtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(exitxt, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE)
        );

        jPanel1.add(exitbtn, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        TitleUser1.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        TitleUser1.setText("Registrar usuario");
        jPanel1.add(TitleUser1, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 50, -1, -1));

        SelectGentxt5.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        SelectGentxt5.setText("Ingrese su numero de usuario:");
        jPanel1.add(SelectGentxt5, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 90, 170, -1));

        jButton2.setBackground(new java.awt.Color(245, 198, 236));
        jButton2.setFont(new java.awt.Font("Times New Roman", 0, 13)); // NOI18N
        jButton2.setText("Finalizado");
        jButton2.setPreferredSize(new java.awt.Dimension(90, 25));
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 240, -1, -1));

        jPasswordField3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jPasswordField3MousePressed(evt);
            }
        });
        jPanel1.add(jPasswordField3, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 170, 170, -1));

        SelectGentxt6.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        SelectGentxt6.setText("Ingrese su contraseña");
        jPanel1.add(SelectGentxt6, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 150, 170, -1));

        jSeparator3.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator3, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 130, 190, 10));

        Usertxt3.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        Usertxt3.setForeground(new java.awt.Color(102, 102, 102));
        Usertxt3.setBorder(null);
        Usertxt3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Usertxt3MousePressed(evt);
            }
        });
        Usertxt3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Usertxt3ActionPerformed(evt);
            }
        });
        jPanel1.add(Usertxt3, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 110, 190, -1));

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jPanel4MouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel4MouseDragged
        int x = evt.getXOnScreen();
        int y = evt.getYOnScreen();
        this.setLocation(x-xMouse, y -yMouse);
    }//GEN-LAST:event_jPanel4MouseDragged

    private void jPanel4MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel4MousePressed
        xMouse = evt.getX();
        yMouse = evt.getY();
    }//GEN-LAST:event_jPanel4MousePressed

    private void exitxtMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_exitxtMouseClicked
        System.exit(0);
    }//GEN-LAST:event_exitxtMouseClicked

    private void exitxtMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_exitxtMouseEntered
        exitbtn.setBackground(Color.red);
    }//GEN-LAST:event_exitxtMouseEntered

    private void exitxtMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_exitxtMouseExited
        exitbtn.setBackground(new Color(245,198,236));
    }//GEN-LAST:event_exitxtMouseExited

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        //validaciones normales
                
        //VALIDAR QUE EL USER NO ESTE INGRESADO
        
        // VALIDACIOENS NORMALES
        String userInput = Usertxt3.getText();
        String userInputPass = jPasswordField3.getText();
        if(userInput.isEmpty()){
            JOptionPane.showMessageDialog(null, "¡Opción inválida! EL usuario esta vacio", "Alerta", JOptionPane.WARNING_MESSAGE);
        }
        else if(userInputPass.isEmpty()){
            JOptionPane.showMessageDialog(null, "¡Opción inválida! La contraseña esta vacia", "Alerta", JOptionPane.WARNING_MESSAGE);
        }
        else if(!vali.validarNumero(Usertxt3.getText())){
            JOptionPane.showMessageDialog(null, "¡Opción inválida! El usuario solo esta conformado por numeros", "Alerta", JOptionPane.WARNING_MESSAGE);
        }
        else if(userInput.length() != 6){
            JOptionPane.showMessageDialog(null, "¡Opción inválida! El usuario solo pueden ser 6 numeros", "Alerta", JOptionPane.WARNING_MESSAGE);
        }else if(jPasswordField3.getText() == null){
            JOptionPane.showMessageDialog(null, "¡Opción inválida! La contraseña ingresada esta vacia.", "Alerta", JOptionPane.WARNING_MESSAGE);
        }else if (!userInputPass.matches("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@#$%]).+$")) {
            JOptionPane.showMessageDialog(null, "La contraseña no cumple con las reglas de validación.\nReglas de validacion:combinaciones de letras mayúsculas, minúsculas, números y caracteres.", "Alerta", JOptionPane.WARNING_MESSAGE);
        }
        
        else{
            boolean isUserIdentificacion = false;
            try {
                //validar que sea usuario // PENDIENTE
                isUserIdentificacion = cone.isUserOnlyIdentifi(Integer.parseInt(Usertxt3.getText()));
                System.out.println(isUserIdentificacion);
                //obtener la info del user ya que esta validado
            } catch (SQLException ex) {
                Logger.getLogger(Home.class.getName()).log(Level.SEVERE, null, ex);
            }
            if(isUserIdentificacion){
                JOptionPane.showMessageDialog(null, "¡Opción inválida! El usuario ingresado ya existe.", "Alerta", JOptionPane.WARNING_MESSAGE);
            }else{
                try {
            //ver si las keys estan generadas
            KeyPair keyPair = null;
            if(!coneAdmin.areKeysPresent()){
                try {
                    keyPair = cifra.generateKeyPair();
                } catch (Exception ex) {
                    Logger.getLogger(RegistroUsuario5.class.getName()).log(Level.SEVERE, null, ex);
                }
                coneAdmin.saveKeysToDatabase(keyPair);
                System.out.println("llaves creadas");
            }else{
                try {
                    keyPair = coneAdmin.loadKeysFromDatabase();
                } catch (NoSuchAlgorithmException ex) {
                    Logger.getLogger(RegistroUsuario5.class.getName()).log(Level.SEVERE, null, ex);
                } catch (InvalidKeySpecException ex) {
                    Logger.getLogger(RegistroUsuario5.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            PublicKey publicKey = keyPair.getPublic();
            PrivateKey privateKey = keyPair.getPrivate();

            byte[] encri;
            try {
                
                encri = cifra.encrypt(jPasswordField3.getText(),publicKey);
                
                String encodedMessage = Base64.getEncoder().encodeToString(encri);
                per.setUserIdentificacion(Integer.parseInt(Usertxt3.getText()));
                per.setPassword(encodedMessage);
           
            } catch (Exception ex) {
                Logger.getLogger(RegistroUsuario5.class.getName()).log(Level.SEVERE, null, ex);
            }
            
           
           
           registarPerson();
           JOptionPane.showMessageDialog(this, "El usuario se ha registrado correctamente", "Registro exitoso", JOptionPane.INFORMATION_MESSAGE);
           Home x = new Home();
           x.setVisible(true);
           this.dispose();
           
       } catch (SQLException ex) {
           Logger.getLogger(RegistroUsuario5.class.getName()).log(Level.SEVERE, null, ex);
       } catch (ParseException ex) {
           Logger.getLogger(RegistroUsuario5.class.getName()).log(Level.SEVERE, null, ex);
       }
            }
            
        }

        
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jPasswordField3MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPasswordField3MousePressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jPasswordField3MousePressed

    private void Usertxt3MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Usertxt3MousePressed

        if (Usertxt3.getText().equals("Ingrese su número")){
            Usertxt3.setText("");
            Usertxt3.setForeground(Color.black);

        }

    }//GEN-LAST:event_Usertxt3MousePressed

    private void Usertxt3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Usertxt3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_Usertxt3ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(RegistroUsuario5.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(RegistroUsuario5.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(RegistroUsuario5.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(RegistroUsuario5.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new RegistroUsuario5(per).setVisible(true);
                } catch (SQLException ex) {
                    Logger.getLogger(RegistroUsuario5.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ParseException ex) {
                    Logger.getLogger(RegistroUsuario5.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel SelectGentxt5;
    private javax.swing.JLabel SelectGentxt6;
    private javax.swing.JLabel TitleUser1;
    private javax.swing.JTextField Usertxt3;
    private javax.swing.JPanel exitbtn;
    private javax.swing.JLabel exitxt;
    private javax.swing.JButton jButton2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPasswordField jPasswordField3;
    private javax.swing.JSeparator jSeparator3;
    // End of variables declaration//GEN-END:variables
}
