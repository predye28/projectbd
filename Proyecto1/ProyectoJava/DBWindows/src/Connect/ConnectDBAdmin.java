/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Connect;


import Metodos.personConsulta;
import RecibirDatosBD.AmenidadesRoom;
import RecibirDatosBD.BookingBD;
import RecibirDatosBD.CommentaryBD;
import RecibirDatosBD.Hotel;
import RecibirDatosBD.OfferBD;
import RecibirDatosBD.PoliticaCancelacionBD;
import RecibirDatosBD.Room;
import RecibirDatosBD.StarHotel;
import RecibirDatosBD.TypeOfPayment;
import RecibirDatosBD.Venta;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleTypes;

/**
 *
 * @author omarm
 */
public class ConnectDBAdmin {
    static String host = "jdbc:oracle:thin:@localhost:1521:BDPRUEBA";
    static String uNameAdmin = "ADMIN";
    static String uPassAdmin = "proyecto1";

    public ConnectDBAdmin() {
    }
    public static ArrayList<Integer> getIdHotelFav(int idPerson) throws SQLException{
        
        Connection conexion = DriverManager.getConnection(host, uNameAdmin, uPassAdmin);
        CallableStatement cstmt = conexion.prepareCall("{? = call GETHOTELFAV(?)}");
        cstmt.registerOutParameter(1, OracleTypes.CURSOR);
        cstmt.setInt(2, idPerson);
        cstmt.execute();
        
        ResultSet rs  = ((OracleCallableStatement)cstmt).getCursor(1);
        
        ArrayList<Integer> results = new ArrayList<>();
        while (rs.next()) {
            results.add(rs.getInt(1));
        }
        cstmt.close();
        conexion.close();
        return results;
    }
    public static Hotel getHotelFull(int idhotel) throws SQLException{
        
        Connection conexion = DriverManager.getConnection(host, uNameAdmin, uPassAdmin);
        CallableStatement cstmt = conexion.prepareCall("{? = call getHotelFull(?)}");
        cstmt.registerOutParameter(1, OracleTypes.CURSOR);
        cstmt.setInt(2, idhotel);
        cstmt.execute();
        
        ResultSet rs  = ((OracleCallableStatement)cstmt).getCursor(1);
        
        Hotel resultHotel = new Hotel();
        while (rs.next()) {
            resultHotel.setIdHotel(rs.getInt(1));
            resultHotel.setName(rs.getString(2));
        }
        cstmt.close();
        conexion.close();
        return resultHotel;
    }
    public static Hotel getHotelFullForId(int idhotel) throws SQLException{
        
        Connection conexion = DriverManager.getConnection(host, uNameAdmin, uPassAdmin);
        CallableStatement cstmt = conexion.prepareCall("{? = call getHotelFullForId(?)}");
        cstmt.registerOutParameter(1, OracleTypes.CURSOR);
        cstmt.setInt(2, idhotel);
        cstmt.execute();
        
        ResultSet rs  = ((OracleCallableStatement)cstmt).getCursor(1);
        
        Hotel resultHotel = new Hotel();
        while (rs.next()) {
            resultHotel.setIdHotel(rs.getInt(1));
            resultHotel.setName(rs.getString(2));
            resultHotel.setIdDistrict(rs.getInt(3));
        }
        cstmt.close();
        conexion.close();
        return resultHotel;
    }
    public static ArrayList<personConsulta> getListaDePersonas(int idhotel) throws SQLException{
        Connection conexion = DriverManager.getConnection(host, uNameAdmin, uPassAdmin);
        CallableStatement cstmt = conexion.prepareCall("{? = call getPersonasInHotel(?)}");
        cstmt.registerOutParameter(1, OracleTypes.CURSOR);
        cstmt.setInt(2, idhotel);
        cstmt.execute();
        
        ResultSet rs  = ((OracleCallableStatement)cstmt).getCursor(1);
        
        ArrayList<personConsulta> resultPersona = new ArrayList<personConsulta>();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        while (rs.next()) {
            personConsulta perCons = new personConsulta();
            perCons.setName(rs.getString(1));
            perCons.setApellido(rs.getString(2));
            perCons.setCedIdentificacion(rs.getString(3));
            perCons.setNacionalidad(rs.getString(4));
            
            String fechaString = dateFormat.format(rs.getDate(5));
            perCons.setCheckin(fechaString);
            
            fechaString = dateFormat.format(rs.getDate(6));
            perCons.setCheckout(fechaString);
            
            perCons.setPrice(rs.getInt(7));
            perCons.setNameHabitacion(rs.getString(8));
            resultPersona.add(perCons);
        }
        cstmt.close();
        conexion.close();
        return resultPersona;
    }
    public static ArrayList<Room> getRoomFull(int idHotel) throws SQLException{
        
        Connection conexion = DriverManager.getConnection(host, uNameAdmin, uPassAdmin);
        CallableStatement cstmt = conexion.prepareCall("{? = call getRoom(?)}");
        cstmt.registerOutParameter(1, OracleTypes.CURSOR);
        cstmt.setInt(2, idHotel);
        cstmt.execute();
        
        ResultSet rs  = ((OracleCallableStatement)cstmt).getCursor(1);
        
        ArrayList<Room> resultRoom = new ArrayList<Room>();
        while (rs.next()) {
            Room room = new Room(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getFloat(4), rs.getInt(5), rs.getInt(6), rs.getInt(7));
            resultRoom.add(room);
        }
        cstmt.close();
        conexion.close();
        return resultRoom;
    }
    public static ArrayList<RecibirDatosBD.Hotel> getAllHotel() throws SQLException{
        
        Connection conexion = DriverManager.getConnection(host, uNameAdmin, uPassAdmin);
        CallableStatement cstmt = conexion.prepareCall("{? = call getHotelsExpecifics()}");
        cstmt.registerOutParameter(1, OracleTypes.CURSOR);
        cstmt.execute();
        
        ResultSet rs  = ((OracleCallableStatement)cstmt).getCursor(1);
        
        ArrayList<RecibirDatosBD.Hotel> resultHote = new ArrayList<RecibirDatosBD.Hotel>();
        while (rs.next()) {
            RecibirDatosBD.Hotel hote = new RecibirDatosBD.Hotel(rs.getInt(1),rs.getString(2),rs.getInt(3), rs.getDate("DATEREGISTER"));
            resultHote.add(hote);
        }
        cstmt.close();
        conexion.close();
        return resultHote;
    }
    public static ArrayList<TypeOfPayment> getTypeOfPayment() throws SQLException{
        
        Connection conexion = DriverManager.getConnection(host, uNameAdmin, uPassAdmin);
        CallableStatement cstmt = conexion.prepareCall("{? = call getPayment()}");
        cstmt.registerOutParameter(1, OracleTypes.CURSOR);
        cstmt.execute();
        
        ResultSet rs  = ((OracleCallableStatement)cstmt).getCursor(1);
        
        ArrayList<TypeOfPayment> resultType = new ArrayList<TypeOfPayment>();
        while (rs.next()) {
            TypeOfPayment type = new TypeOfPayment(rs.getInt(1),rs.getString(2));
            resultType.add(type);
        }
        cstmt.close();
        conexion.close();
        return resultType;
    }
    public static ArrayList<Venta> getMayorVenta(int idHotel) throws SQLException{
        
        Connection conexion = DriverManager.getConnection(host, uNameAdmin, uPassAdmin);
        CallableStatement cstmt = conexion.prepareCall("{? = call getMayoresVentas(?)}");
        cstmt.registerOutParameter(1, OracleTypes.CURSOR);
        cstmt.setInt(2, idHotel);
        cstmt.execute();
        
        ResultSet rs  = ((OracleCallableStatement)cstmt).getCursor(1);
        
        ArrayList<Venta> resultType = new ArrayList<Venta>();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        while (rs.next()) {
            String fechaString = dateFormat.format(rs.getDate(1));
            Venta type = new Venta(fechaString,rs.getInt(2));
            resultType.add(type);
        }
        cstmt.close();
        conexion.close();
        return resultType;
    }
    public static ArrayList<Venta> getMenorVenta(int idHotel) throws SQLException{
        
        Connection conexion = DriverManager.getConnection(host, uNameAdmin, uPassAdmin);
        CallableStatement cstmt = conexion.prepareCall("{? = call getMenoresVentas(?)}");
        cstmt.registerOutParameter(1, OracleTypes.CURSOR);
        cstmt.setInt(2, idHotel);
        cstmt.execute();
        
        ResultSet rs  = ((OracleCallableStatement)cstmt).getCursor(1);
        
        ArrayList<Venta> resultType = new ArrayList<Venta>();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        while (rs.next()) {
            String fechaString = dateFormat.format(rs.getDate(1));
            Venta type = new Venta(fechaString,rs.getInt(2));
            resultType.add(type);
        }
        cstmt.close();
        conexion.close();
        return resultType;
    }
    public static int insertBookingAndGetBooking(BookingBD book, int idPerson, int idRoom) throws SQLException, ParseException{

        if(book.getIdFeedback()!=0){
            Connection con = DriverManager.getConnection(host, uNameAdmin, uPassAdmin);
            CallableStatement stmt = con.prepareCall("{ ? = call INSERTBOOKING(?,?,?,?,?,?)}");

            stmt.registerOutParameter(1, Types.INTEGER); // Parámetro de salida para el valor de retorno

            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

            java.util.Date parsedDateCheckIn = dateFormat.parse(book.getCheckIn());
            java.sql.Date sqlDateCheckIn = new java.sql.Date(parsedDateCheckIn.getTime());
            stmt.setDate(2, sqlDateCheckIn);

            java.util.Date parsedDateCheckOut = dateFormat.parse(book.getCheckOut());
            java.sql.Date sqlDateCheckout = new java.sql.Date(parsedDateCheckOut.getTime());
            stmt.setDate(3, sqlDateCheckout);

            stmt.setInt(4, book.getPrice());
            stmt.setInt(5, idPerson);
            stmt.setInt(6, book.getIdFeedback());
            stmt.setInt(7, idRoom);

            stmt.execute();
            int returnValue = stmt.getInt(1);
            stmt.close();
            con.close();
            return returnValue; 
        }else{
            Connection con = DriverManager.getConnection(host, uNameAdmin, uPassAdmin);
            CallableStatement stmt = con.prepareCall("{ ? = call INSERTBOOKING(?,?,?,?,?,?)}");

            stmt.registerOutParameter(1, Types.INTEGER); // Parámetro de salida para el valor de retorno

            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

            java.util.Date parsedDateCheckIn = dateFormat.parse(book.getCheckIn());
            java.sql.Date sqlDateCheckIn = new java.sql.Date(parsedDateCheckIn.getTime());
            stmt.setDate(2, sqlDateCheckIn);

            java.util.Date parsedDateCheckOut = dateFormat.parse(book.getCheckOut());
            java.sql.Date sqlDateCheckout = new java.sql.Date(parsedDateCheckOut.getTime());
            stmt.setDate(3, sqlDateCheckout);

            stmt.setInt(4, book.getPrice());
            stmt.setInt(5, idPerson);
            stmt.setNull(6, Types.INTEGER);
            stmt.setInt(7, idRoom);

            stmt.execute();
            int returnValue = stmt.getInt(1);
            stmt.close();
            con.close();
            return returnValue; 
        }
        
    }
    public static ArrayList<StarHotel> getStarHotel() throws SQLException{
        
        Connection conexion = DriverManager.getConnection(host, uNameAdmin, uPassAdmin);
        CallableStatement cstmt = conexion.prepareCall("{? = call getStarHotel()}");
        cstmt.registerOutParameter(1, OracleTypes.CURSOR);
        cstmt.execute();
        
        ResultSet rs  = ((OracleCallableStatement)cstmt).getCursor(1);
        
        ArrayList<StarHotel> resultType = new ArrayList<StarHotel>();
        while (rs.next()) {
            StarHotel type = new StarHotel(rs.getInt(1),rs.getString(2));
            resultType.add(type);
        }
        cstmt.close();
        conexion.close();
        return resultType;
    }
    public static ArrayList<PoliticaCancelacionBD> getPoliticaCancelacion(int idHotel) throws SQLException{
        
        Connection conexion = DriverManager.getConnection(host, uNameAdmin, uPassAdmin);
        CallableStatement cstmt = conexion.prepareCall("{? = call getCancellacionP(?)}");
        cstmt.registerOutParameter(1, OracleTypes.CURSOR);
        cstmt.setInt(2, idHotel);
        cstmt.execute();
        
        ResultSet rs  = ((OracleCallableStatement)cstmt).getCursor(1);
        
        ArrayList<PoliticaCancelacionBD> resultType = new ArrayList<PoliticaCancelacionBD>();
        while (rs.next()) {
            PoliticaCancelacionBD type = new PoliticaCancelacionBD(rs.getInt(1),rs.getString(2),rs.getInt(3));
            resultType.add(type);
        }
        cstmt.close();
        conexion.close();
        return resultType;
    }
    public static int getIdRating(int number) throws SQLException{
        Connection conexion = DriverManager.getConnection(host, uNameAdmin, uPassAdmin);
        
        CallableStatement cstmt = conexion.prepareCall("{? = call insertRating(?)}");
        cstmt.registerOutParameter(1, Types.INTEGER);
        cstmt.setInt(2, number);
        
        cstmt.execute();
        int returnValue = cstmt.getInt(1);
        cstmt.close();
        conexion.close();
        return returnValue;
    }
    public static int getIdFeedBack(String commentary, int idRating) throws SQLException{
        Connection conexion = DriverManager.getConnection(host, uNameAdmin, uPassAdmin);
        
        CallableStatement cstmt = conexion.prepareCall("{? = call insertFeedBack(?,?)}");
        cstmt.registerOutParameter(1, Types.INTEGER);
        cstmt.setString(2, commentary);
        cstmt.setInt(3, idRating);
        
        cstmt.execute();
        int returnValue = cstmt.getInt(1);
        cstmt.close();
        conexion.close();
        return returnValue;
    }
    public static int getIdHotelInsertHotel(RecibirDatosBD.Hotel hotel) throws SQLException{
        Connection conexion = DriverManager.getConnection(host, uNameAdmin, uPassAdmin);
        
        CallableStatement cstmt = conexion.prepareCall("{? = call insertHotel(?,?)}");
        cstmt.registerOutParameter(1, Types.INTEGER);
        cstmt.setString(2, hotel.getName());
        cstmt.setInt(3, hotel.getIdDistrict());
        
        cstmt.execute();
        int returnValue = cstmt.getInt(1);
        cstmt.close();
        conexion.close();
        return returnValue;
    }
    public void insertAmenityHotel(String name, int idHotel) throws SQLException{
        Connection conexion = DriverManager.getConnection(host, uNameAdmin, uPassAdmin);
        
        CallableStatement cstmt = conexion.prepareCall("{call insertAmenityHotel(?,?)}");
        cstmt.setString(1, name);
        cstmt.setInt(2, idHotel);
        
        cstmt.execute();
        cstmt.close();
        conexion.close();
    }
    public void personFavHotel(int idPerson, int idHotel) throws SQLException{
        Connection conexion = DriverManager.getConnection(host, uNameAdmin, uPassAdmin);
        
        CallableStatement cstmt = conexion.prepareCall("{call personFavHotel(?,?)}");
        cstmt.setInt(1, idPerson);
        cstmt.setInt(2, idHotel);
        
        cstmt.execute();
        cstmt.close();
        conexion.close();
    }
    public static int getIdRoomInsertRoom(RecibirDatosBD.Room room) throws SQLException{
        Connection conexion = DriverManager.getConnection(host, uNameAdmin, uPassAdmin);
        
        CallableStatement cstmt = conexion.prepareCall("{? = call insertRoom(?,?,?,?)}");
        cstmt.registerOutParameter(1, Types.INTEGER);
        cstmt.setString(2, room.getName());
        cstmt.setInt(3, room.getCapacity());
        cstmt.setInt(4, room.getPrice());
        cstmt.setInt(5, room.getIdHotel());
        
        cstmt.execute();
        int returnValue = cstmt.getInt(1);
        cstmt.close();
        conexion.close();
        return returnValue;
    }
    public static int getcalcularPromedioCalifiacion(int idHotel) throws SQLException{
        Connection conexion = DriverManager.getConnection(host, uNameAdmin, uPassAdmin);
        
        CallableStatement cstmt = conexion.prepareCall("{? = call calcularPromedioCalifiacion(?)}");
        cstmt.registerOutParameter(1, Types.INTEGER);
        cstmt.setInt(2, idHotel);
        
        cstmt.execute();
        int returnValue = cstmt.getInt(1);
        cstmt.close();
        conexion.close();
        return returnValue;
    }
    public static ArrayList<CommentaryBD> getComentariosHotel(int idHotel) throws SQLException{
        Connection conexion = DriverManager.getConnection(host, uNameAdmin, uPassAdmin);
        CallableStatement cstmt = conexion.prepareCall("{? = call getCommentariosHotel(?)}");
        cstmt.registerOutParameter(1, OracleTypes.CURSOR);
        cstmt.setInt(2, idHotel);
        cstmt.execute();
        
        ResultSet rs  = ((OracleCallableStatement)cstmt).getCursor(1);
        
        ArrayList<CommentaryBD> resultType = new ArrayList<CommentaryBD>();
        while (rs.next()) {
            CommentaryBD type = new CommentaryBD();
            type.setEstrella(rs.getInt(1));
            type.setCommentary(rs.getString(2));
            resultType.add(type);
        }
        cstmt.close();
        conexion.close();
        return resultType;
    }
    public void insertAmenityRoom(String name) throws SQLException{
        Connection conexion = DriverManager.getConnection(host, uNameAdmin, uPassAdmin);
        
        CallableStatement cstmt = conexion.prepareCall("{call insertAmenityRoom(?)}");
        cstmt.setString(1, name);
        
        cstmt.execute();
        cstmt.close();
        conexion.close();
    }
    public static ArrayList<AmenidadesRoom> getAmenidyRoom() throws SQLException{
        
        Connection conexion = DriverManager.getConnection(host, uNameAdmin, uPassAdmin);
        CallableStatement cstmt = conexion.prepareCall("{? = call getAmenityRoom()}");
        cstmt.registerOutParameter(1, OracleTypes.CURSOR);
        cstmt.execute();
        
        ResultSet rs  = ((OracleCallableStatement)cstmt).getCursor(1);
        
        ArrayList<AmenidadesRoom> resultType = new ArrayList<AmenidadesRoom>();
        while (rs.next()) {
            AmenidadesRoom type = new AmenidadesRoom(rs.getInt(1),rs.getString(2));
            resultType.add(type);
        }
        cstmt.close();
        conexion.close();
        return resultType;
    }
    
    public static ArrayList<OfferBD> getOfferHotel(int idHotel) throws SQLException{
        
        Connection conexion = DriverManager.getConnection(host, uNameAdmin, uPassAdmin);
        CallableStatement cstmt = conexion.prepareCall("{? = call getOffer(?)}");
        cstmt.registerOutParameter(1, OracleTypes.CURSOR);
        cstmt.setInt(2, idHotel);
        cstmt.execute();
        
        ResultSet rs  = ((OracleCallableStatement)cstmt).getCursor(1);
        
        ArrayList<OfferBD> resultType = new ArrayList<OfferBD>();
        while (rs.next()) {
            OfferBD type = new OfferBD();
            type.setId(rs.getInt(1));
            type.setName(rs.getString(2));
            resultType.add(type);
        }
        cstmt.close();
        conexion.close();
        return resultType;
    }
    public static ArrayList<OfferBD> getOfferHotelForName(int idHotel, String name) throws SQLException{
        
        Connection conexion = DriverManager.getConnection(host, uNameAdmin, uPassAdmin);
        CallableStatement cstmt = conexion.prepareCall("{? = call getOfertasHotelForName(?,?)}");
        cstmt.registerOutParameter(1, OracleTypes.CURSOR);
        cstmt.setInt(2, idHotel);
        cstmt.setString(3, name);
        cstmt.execute();
        
        ResultSet rs  = ((OracleCallableStatement)cstmt).getCursor(1);
        
        ArrayList<OfferBD> resultType = new ArrayList<OfferBD>();
        while (rs.next()) {
            OfferBD type = new OfferBD();
            type.setName(rs.getString(1));
            type.setDescripcion(rs.getString(2));
            resultType.add(type);
        }
        cstmt.close();
        conexion.close();
        return resultType;
    }
    public void insertAmenityRoomDeLaTablaNXN(int idRoom, int idAmenity) throws SQLException{
        Connection conexion = DriverManager.getConnection(host, uNameAdmin, uPassAdmin);
        
        CallableStatement cstmt = conexion.prepareCall("{call insertRoomXAR(?,?)}");
        cstmt.setInt(1, idRoom);
        cstmt.setInt(2, idAmenity);
        cstmt.execute();
        cstmt.close();
        conexion.close();
    }
    public void deleteOffer(int idOffer, int idHotel) throws SQLException{
        Connection conexion = DriverManager.getConnection(host, uNameAdmin, uPassAdmin);
        
        CallableStatement cstmt = conexion.prepareCall("{call deleteOffer(?,?)}");
        System.out.println(idOffer);
        System.out.println(idHotel);
        cstmt.setInt(1, idOffer);
        cstmt.setInt(2, idHotel);
        cstmt.execute();
        cstmt.close();
        conexion.close();
    }
    public void insertCancelacion(String name, int porcentaje, int idHotel) throws SQLException{
        Connection conexion = DriverManager.getConnection(host, uNameAdmin, uPassAdmin);
        
        CallableStatement cstmt = conexion.prepareCall("{call INSERTCANCELLACIONP(?,?,?)}");
        cstmt.setString(1, name);
        cstmt.setInt(2, porcentaje);
        cstmt.setInt(3, idHotel);
        cstmt.execute();
        cstmt.close();
        conexion.close();
    }
    public void insertOffer(OfferBD offer) throws SQLException, ParseException{
        Connection conexion = DriverManager.getConnection(host, uNameAdmin, uPassAdmin);
        
        CallableStatement cstmt = conexion.prepareCall("{call insertOffer(?,?,?,?,?,?,?)}");
        
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        
        cstmt.setString(1, offer.getName());
        cstmt.setInt(2, offer.getDiasMinimos());
        cstmt.setString (3, offer.getDescripcion());
        
        java.util.Date parsedDateStar = dateFormat.parse(offer.getStarDate());
        java.sql.Date sqlDateStar = new java.sql.Date(parsedDateStar.getTime());
        cstmt.setDate(4, sqlDateStar);
        java.util.Date parsedDateFinish = dateFormat.parse(offer.getFinishDate());
        java.sql.Date sqlDateFinish = new java.sql.Date(parsedDateFinish.getTime());
        cstmt.setDate(5, sqlDateFinish);
        
        cstmt.setInt(6, offer.getIdRoom());
        cstmt.setInt(7, offer.getIdHotel());
        
        cstmt.execute();
        cstmt.close();
        conexion.close();
    }
    public static boolean areKeysPresent() throws SQLException {
        try (Connection connection = DriverManager.getConnection(host, uNameAdmin, uPassAdmin)) {
            // Verificar si hay registros en la tabla keys_table
            PreparedStatement statement = connection.prepareStatement("SELECT COUNT(*) FROM keys_table");
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                int count = resultSet.getInt(1);
                return count > 0;
            }
        }

        return false;
    }
    public static void saveKeysToDatabase(KeyPair keyPair) throws SQLException {
        // Obtener la conexión a la base de datos
        try (Connection connection = DriverManager.getConnection(host, uNameAdmin, uPassAdmin)) {
            // Insertar las claves en la tabla de la base de datos
            PreparedStatement statement = connection.prepareStatement("INSERT INTO keys_table (public_key, private_key) VALUES (?, ?)");

            // Obtener la representación de bytes de las claves
            byte[] publicKeyBytes = keyPair.getPublic().getEncoded();
            byte[] privateKeyBytes = keyPair.getPrivate().getEncoded();

            // Establecer los parámetros de la sentencia INSERT
            statement.setBytes(1, publicKeyBytes);
            statement.setBytes(2, privateKeyBytes);

            // Ejecutar la sentencia INSERT
            statement.executeUpdate();
        }
    }
    public static KeyPair loadKeysFromDatabase() throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException {
        // Obtener la conexión a la base de datos
        try (Connection connection = DriverManager.getConnection(host, uNameAdmin, uPassAdmin)) {
            // Recuperar las claves de la tabla de la base de datos
            PreparedStatement statement = connection.prepareStatement("SELECT public_key, private_key FROM keys_table");
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                // Obtener los bytes de las claves desde el resultado de la consulta
                byte[] publicKeyBytes = resultSet.getBytes("public_key");
                byte[] privateKeyBytes = resultSet.getBytes("private_key");

                // Construir las claves a partir de los bytes
                KeyFactory keyFactory = KeyFactory.getInstance("RSA");

                X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(publicKeyBytes);
                PublicKey publicKey = keyFactory.generatePublic(publicKeySpec);

                PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(privateKeyBytes);
                PrivateKey privateKey = keyFactory.generatePrivate(privateKeySpec);

                return new KeyPair(publicKey, privateKey);
            }
        }

        return null;
    }
}
