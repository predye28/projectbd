
--A
BEGIN
  DBMS_SCHEDULER.create_job (
    job_name => 'JobReport',
    job_type => 'PLSQL_BLOCK',
    job_action => 'BEGIN ROOMAVAILIBLE(); END;',--funcion
    start_date => trunc(SYSDATE) + interval '7' hour, 
    repeat_interval => 'FREQ=DAILY; BYHOUR=7; BYMINUTE=0; BYSECOND=0;',
    end_date => NULL,
    enabled => TRUE,
    comments => 'Mi primer job'
  );
END;

--creacion de la tabla y todo lo que conlleva eso
CREATE TABLE reportJob
(
    id NUMBER(6) CONSTRAINT reportJob_id_nn NOT NULL,
    currentDate DATE DEFAULT SYSDATE CONSTRAINT reportJob_currentDate_nn NOT NULL,
    OtherWeekdate DATE DEFAULT SYSDATE CONSTRAINT reportJob_OtherWeekdate_nn NOT NULL
);

CREATE TABLE roomReport
(
    id NUMBER(6) CONSTRAINT roomReport_id_nn NOT NULL,
    name VARCHAR2(25) CONSTRAINT roomReport_name_nn NOT NULL,
    idReport NUMBER(6) CONSTRAINT roomReport_idReport_nn NOT NULL
);


ALTER TABLE reportJob
ADD
CONSTRAINT pk_reportJob PRIMARY KEY (id)
USING INDEX
TABLESPACE ProyectoDBInd PCTFREE 20
STORAGE (INITIAL 10K NEXT 10K PCTINCREASE 0);

ALTER TABLE roomReport
ADD
CONSTRAINT pk_roomReport PRIMARY KEY (id)
USING INDEX
TABLESPACE ProyectoDBInd PCTFREE 20
STORAGE (INITIAL 10K NEXT 10K PCTINCREASE 0);

ALTER TABLE roomReport
ADD
CONSTRAINT fk_roomReport_reportJob FOREIGN KEY (id) REFERENCES reportJob(id);

--secuencias
CREATE SEQUENCE idreportJob
    INCREMENT BY 1
    MINVALUE 0
    MAXVALUE 100000
    NOCACHE
    NOCYCLE;
CREATE SEQUENCE idroomReport
    INCREMENT BY 1
    MINVALUE 0
    MAXVALUE 100000
    NOCACHE
    NOCYCLE;
--crear funcion que trae nombre de la habitacion libre
create or replace PROCEDURE RoomAvailible(pIdHotel NUMBER)
AS 
  l_id NUMBER;
  l_name VARCHAR2(100);

BEGIN

    INSERT INTO reportJob(id, CURRENTDATE, OTHERWEEKDATE)
    VALUES (idreportJob.NEXTVAL, SYSDATE, SYSDATE + 7) RETURNING id INTO l_id;

    -- Realizar el select y los inserts basados en el nombre obtenido
    FOR r_cursor IN (
        SELECT r.name
        FROM booking b
        INNER JOIN room r
        ON r.id = b.idRoom
        INNER JOIN hotel h
        ON h.id = r.idhotel
        WHERE (h.id = pIdHotel)AND (SYSDATE BETWEEN b.checkin AND b.checkout) AND (SYSDATE +7 BETWEEN b.checkin AND b.checkout)
    ) LOOP
        l_name := r_cursor.name;

        -- Insertar en otra tabla utilizando el ID y nombre obtenidos
        INSERT INTO roomReport(id, name, idReport)
        VALUES (idroomReport.NEXTVAL, l_name,l_id);
    END LOOP;

    COMMIT;
EXCEPTION
    WHEN OTHERS THEN
        ROLLBACK;
        RAISE;
END;



