--provincia SAN JOSE

--CANTON: san jose

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Carmen',1);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Merced',1);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Hospital',1);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Catedral',1);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Zapote',1);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'San Francisco de Dos R�os',1);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'La Uruca',1);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Mata Redonda',1);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Pavas',1);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Hatillo',1);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'San Sebasti�n',1);

--CANTON: escazu

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Escaz�',2);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'San Antonio',2);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'San Rafael',2);

--CANTON: desamparados

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Desamparados',3);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'San Miguel',3);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'San Juan de Dios',3);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'San Rafael Arriba',3);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'San Antonio',3);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Frailes',3);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Patarr�',3);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'San Crist�bal',3);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Rosario',3);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Damas',3);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'San Rafael Abajo',3);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Gravilias',3);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Los Guido',3);

--CANTON: PURISCAL
INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Santiago',4);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Mercedes Sur',4);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Barbacoas',4);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Grifo Alto',4);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'San Rafael',4);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Candelarita',4);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Desamparaditos',4);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'San Antonio',4);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Chires',4);

--CANTON: TARRAZU

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'San Marcos',5);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'San Lorenzo',5);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'San Carlos',5);

--CANTON: ASERRI

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Aserr�',6);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Tarbaca',6);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Vuelta de Jorco',6);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'San Gabriel',6);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Legua',6);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Monterrey',6);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Salitrillos',6);

--CANTON: MORA
INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Col�n',8);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Guayabo',8);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Tabarcia',8);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Piedras Negras',8);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Picagres',8);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Jaris',8);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Quitirris�',8);

--CANTON: GOICOECHEA
INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Guadalupe',9);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'San Francisco',9);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Calle Blancos',9);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Mata de Pl�tano',9);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Ip�s',9);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Rancho Redondo',9);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Purral',9);

--CANTON: SANTA ANA
INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Santa Ana',10);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Salitral',10);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Pozos',10);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Uruca',10);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Piedades',10);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Brasil',10);

--CANTON: ALAJUELITA

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Alajuelita',11);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'San Josecito',11);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'San Antonio',11);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Concepci�n',11);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'San Felipe',11);

--CANTON: VAZQUEZ DE CORONADO
INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'San Isidro',12);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'San Rafael',12);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Dulce Nombre de Jes�s',12);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Patalillo',12);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Cascajal',12);

--CANTON: ACOSTA
INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'San Ignacio',13);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Guaitil',13);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Palmichal',13);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Cangrejal',13);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Sabanillas',13);

--CANTON: TIBAS
INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'San Juan',14);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Cinco Esquinas',14);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Anselmo Llorente',14);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Le�n XIII',14);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Colima',14);

--CANTON: MORAVIA
INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'San Vicente',15);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'San Jer�nimo',15);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'La Trinidad',15);

--CANTON: MONTES DE OCA
INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'San Pedro',16);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Sabanilla',16);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Mercedes',16);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'San Rafael',16);

--CANTON: TURRUBARES
INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'San Pablo',17);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'San Pedro',17);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'San Juan de Mata',17);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'San Luis',17);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Carara',17);

--CANTON: DOTA
INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Santa Mar�a',18);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Jard�n',18);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Copey',18);
--CANTON: CURRIDABAT
INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Curridabat',19);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Granadilla',19);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'S�nchez',19);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Tirrases',19);
--CANTON: PEREZ ZELEDON
INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'San Isidro de El General',20);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'El General',20);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Daniel Flores',20);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Rivas',20);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'San Pedro',20);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Platanares',20);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Pejibaye',20);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Caj�n',20);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Bar�',20);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'R�o Nuevo',20);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'P�ramo',20);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'La Amistad',20);
--CANTON: LEON CORTES CASTRO
INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'San Pablo',21);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'San Andr�s',21);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Llano Bonito',21);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'San Isidro',21);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'Santa Cruz',21);

INSERT INTO district(id, name,idCanton)
VALUES (idDistrict.NEXTVAL,'San Antonio',21);

--alajuela

--CANTON ALAJUELA
DECLARE
  idCanton NUMBER := 22;
BEGIN
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'Alajuela', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'San Jos�', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'Carrizal', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'San Antonio', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'La Gu�cima', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'San Isidro', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'Sabanilla', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'San Rafael', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'R�o Segundo', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'Desamparados', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'Turr�cares', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'Tambor', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'La Garita', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'Sarapiqu�', idCanton);
END;

--canton san ramom
DECLARE
  idCanton NUMBER := 24;
BEGIN
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'San Ram�n', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'Santiago', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'San Juan', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'Piedades Norte', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'Piedades Sur', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'San Rafael', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'San Isidro', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, '�ngeles', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'Alfaro', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'Volio', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'Concepci�n', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'Zapotal', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'Pe�as Blancas', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'San Lorenzo', idCanton);
END;
--canton grecia
DECLARE
  idCanton NUMBER := 25;
BEGIN
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'Grecia', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'San Isidro', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'San Jos�', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'San Roque', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'Tacares', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'Puente de Piedra', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'Bol�var', idCanton);
END;

--canton san mateo
DECLARE
  idCanton NUMBER := 26;
BEGIN
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'San Mateo', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'Desmonte', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'Jes�s Mar�a', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'Labrador', idCanton);
END;

--cantona atenas
DECLARE
  idCanton NUMBER := 27;
BEGIN
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'Atenas', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'Jes�s', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'Mercedes', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'San Isidro', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'Concepci�n', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'San Jos�', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'Santa Eulalia', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'Escobal', idCanton);
END;



--cartago
DECLARE
  idCanton NUMBER := 28;
BEGIN
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'Oriental', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'Occidental', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'Carmen', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'San Nicol�s', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'Aguacaliente', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'Guadalupe', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'Corralillo', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'Tierra Blanca', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'Dulce Nombre', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'Llano Grande', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'Quebradilla', idCanton);
END;


--la union
DECLARE
  idCanton NUMBER := 30;
BEGIN
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'Tres R�os', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'San Diego', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'San Juan', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'San Rafael', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'Concepci�n', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'Dulce Nombre', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'San Ram�n', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'R�o Azul', idCanton);
END;

--heredia
DECLARE
  idCanton NUMBER := 31;
BEGIN
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'Heredia', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'Mercedes', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'San Francisco', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'Ulloa', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'Varablanca', idCanton);
END;
--barva
DECLARE
  idCanton NUMBER := 32;
BEGIN
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'Barva', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'San Pedro', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'San Pablo', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'San Roque', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'Santa Luc�a', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'San Jos� de la Monta�a', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'Puente Salas', idCanton);
END;
--santo domingo
DECLARE
  idCanton NUMBER := 33;
BEGIN
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'Santo Domingo', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'San Vicente', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'San Miguel', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'Paracito', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'Santo Tom�s', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'Santa Rosa', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'Tures', idCanton);
  
  INSERT INTO district(id, name, idCanton)
  VALUES (idDistrict.NEXTVAL, 'Par�', idCanton);
END;













