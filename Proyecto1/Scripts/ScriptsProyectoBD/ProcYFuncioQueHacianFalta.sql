--CURSOR QUE TRAE TODA LA INFO DEL HOTEL POR ID HOTEL // ADMIN
create or replace FUNCTION getHotelFull(pIdHotel IN NUMBER) RETURN SYS_REFCURSOR
IS 
  hotelFull SYS_REFCURSOR;

BEGIN 

  OPEN hotelFull FOR 
    SELECT h.id, h.name
    FROM hotel h
    WHERE h.id = pIdHotel;

  RETURN hotelFull; 

END getHotelFull;
--CURSOR QUE TRAE TODA LA INFORMACION DE USER1 // USER1 
create or replace FUNCTION getAllUser(pnumIdentification IN NUMBER) RETURN SYS_REFCURSOR
IS 
  userAll SYS_REFCURSOR;

BEGIN 

  OPEN userAll FOR 
    SELECT u.identification, u.password, u.idPerson, u.idTypeOfUser, u.idHotel
    FROM user1 u
    WHERE u.identification = pnumIdentification;

  RETURN userAll; 

END getAllUser;

--cambiar el atributo commentary de la tabla backfeed porq si puede ser null
ALTER TABLE backfeed
DROP CONSTRAINT BACKFEED_COMMENT_NN;

--asignar un usuario admin master
UPDATE user1
SET idtypeofuser = 3
WHERE idperson = 14;

--CURSOR QUE TRAE TODA LA INFORMACION  de amenity room// admin 
create or replace FUNCTION getAmenityRoom RETURN SYS_REFCURSOR
IS 
  amenityRoomCursor SYS_REFCURSOR;

BEGIN 

  OPEN amenityRoomCursor FOR 
    SELECT u.id, u.name
    FROM amenityroom u;

  RETURN amenityRoomCursor; 

END getAmenityRoom;

--cambiar el tama�o de el varchar2 de la password de user1 
ALTER TABLE user1
MODIFY (password VARCHAR2(4000));

--hacer que el segundo nombre de person pueda ser null
ALTER TABLE person
DROP CONSTRAINT PERSON_SECONDNAME_NN;

--funcion que retorna un string con la constrasena cifrada
create or replace FUNCTION getPasswordFromUser(
    pIdentification NUMBER
    
) RETURN VARCHAR2 AS
    returnPassword VARCHAR2;
BEGIN

    SELECT u.password INTO returnPassword
    FROM USER1 u
    WHERE u.identification = pIdentification;
  COMMIT;

  RETURN returnPassword;
END;
--funcion que retorna true si el usuario existe solo con el usuario
create or replace FUNCTION isUserOnlyIdentifi(pidenti NUMBER) RETURN BOOLEAN
IS
  cont NUMBER;
BEGIN
  SELECT COUNT(*) INTO cont
  FROM user1 u
  WHERE u.identification = pidenti;

  IF cont > 0 THEN
    RETURN TRUE;
  ELSE
    RETURN FALSE;
  END IF;
END isUserOnlyIdentifi;

--crear tabla en admin para guardar las llaves
CREATE SEQUENCE keys_table_seq START WITH 1 INCREMENT BY 1;

CREATE OR REPLACE TRIGGER keys_table_trigger
BEFORE INSERT ON keys_table
FOR EACH ROW
BEGIN
    SELECT keys_table_seq.NEXTVAL INTO :new.id FROM dual;
END;

CREATE TABLE keys_table (
    id NUMBER,
    public_key BLOB,
    private_key BLOB,
    PRIMARY KEY (id)
);


--crear funcion que me trae toda la infomracion completa del hotel que recibe por id
create or replace FUNCTION getHotelFullForId(pIdHotel IN NUMBER) RETURN SYS_REFCURSOR
IS 
  hotelFullId SYS_REFCURSOR;

BEGIN 

  OPEN hotelFullId FOR 
    SELECT h.id, h.name, h.idDistrict, h.dateRegister
    FROM hotel h
    WHERE h.id = pIdHotel;

  RETURN hotelFullId; 

END getHotelFullForId;


--ELIMINAR INFORMACION INECESARIO
DELETE FROM person;



