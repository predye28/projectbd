CREATE OR REPLACE FUNCTION getNationality RETURN SYS_REFCURSOR
IS 

  natio SYS_REFCURSOR;
   
BEGIN 

  OPEN natio FOR 
    SELECT n.id, n.name
    FROM nationality n; 

  RETURN natio; 

END getNationality;
--gender
CREATE OR REPLACE FUNCTION getGender RETURN SYS_REFCURSOR
IS 

  gende SYS_REFCURSOR;
   
BEGIN 

  OPEN gende FOR 
    SELECT n.id, n.name
    FROM gender n; 

  RETURN gende; 

END getGender;

CREATE OR REPLACE FUNCTION getTypeid RETURN SYS_REFCURSOR
IS 

  typeidc SYS_REFCURSOR;
   
BEGIN 

  OPEN typeidc FOR 
    SELECT n.id, n.mask, n.identification
    FROM typeid n; 

  RETURN typeidc; 

END getTypeid;

--retornar typeofuser
CREATE OR REPLACE FUNCTION getTypeOfUser RETURN SYS_REFCURSOR
IS 
  typeOU SYS_REFCURSOR;
BEGIN 

  OPEN typeOU FOR 
    SELECT t.id, t.name
    FROM typeOfuser t; 

  RETURN typeOU; 

END getTypeOfUser;

--retornar provincias
CREATE OR REPLACE FUNCTION getProvince RETURN SYS_REFCURSOR
IS 
  provi SYS_REFCURSOR;
BEGIN 

  OPEN provi FOR 
    SELECT p.id, p.name
    FROM province p; 

  RETURN provi; 

END getProvince;

--cantones de la provincia recibida
CREATE OR REPLACE FUNCTION getCanton(pIdProvince IN NUMBER) RETURN SYS_REFCURSOR
IS 
  canto SYS_REFCURSOR;
BEGIN 

  OPEN canto FOR 
    SELECT c.id, c.name
    FROM canton c
    WHERE c.IdProvince = pIdProvince;

  RETURN canto; 

END getCanton;

--DISTRitos del canton recibido
CREATE OR REPLACE FUNCTION getDistrict(pIdCanton IN NUMBER) RETURN SYS_REFCURSOR
IS 
  distr SYS_REFCURSOR;
BEGIN 

  OPEN distr FOR 
    SELECT d.id, d.name
    FROM district d
    WHERE d.idCanton = pIdCanton;

  RETURN distr; 

END getDistrict;



