--INSERTAR OFERTAS
INSERT INTO offer(id, name, miniumDay, description, startDate, finishDate, billedamount,idRoom, idHotel)
VALUES (idOffer.NEXTVAL,'Oferta Verano',2,'Aprovecha esta oferta en verano en familia',TO_DATE('05/01/2024', 'DD/MM/YY'),TO_DATE('20/01/2024', 'DD/MM/YY'),0,1,1);

INSERT INTO offer(id, name, miniumDay, description, startDate, finishDate, billedamount, idRoom, idHotel)
VALUES (idOffer.NEXTVAL,'Oferta Invierno',3,'Aprovecha en esta navidad!!!',TO_DATE('01/12/2023', 'DD/MM/YY'),TO_DATE('30/12/2023', 'DD/MM/YY'),0,3,1);


INSERT INTO offer(id, name, miniumDay, description, startDate, finishDate, billedamount, idRoom, idHotel)
VALUES (idOffer.NEXTVAL,'Ahora una noche',5,'El mayor precio y comodidad en esta habitaicion',TO_DATE('01/06/2023', 'DD/MM/YY'),TO_DATE('01/08/2023', 'DD/MM/YY'),0,2,2);


--SUMAR AL MONTO FACTURADO A UNA OFERTA
UPDATE offer
SET billedAmount = billedAmount + 42000
WHERE id = 8;

UPDATE offer
SET billedAmount = billedAmount + 42000
WHERE id = 10;
--INSERTAR AMENIDADES ROOM
INSERT INTO amenityRoom(id, name)
VALUES (idAmenityRoom.NEXTVAL,'Aire acondicionado');

INSERT INTO amenityRoom(id, name)
VALUES (idAmenityRoom.NEXTVAL,'Caja fuerte');

--insertar tipos de pagos para la reserva
INSERT INTO typeofpaymentm(id, name)
VALUES (idtypeofpaymentm.NEXTVAL,'Visa');

INSERT INTO typeofpaymentm(id, name)
VALUES (idtypeofpaymentm.NEXTVAL,'Paypal');

--El idperson de hotel son las personas que tienen favoritos su hotel
INSERT INTO hotel(id, name, idDistrict)
VALUES (idHotel.NEXTVAL,'Carmen Lira',4);

INSERT INTO cancellacionP(id, name, percentage, idHotel)
VALUES (idCancellacionP.NEXTVAL,'Pago Adelantado',50,1);

INSERT INTO amenityHotel(id, name,idHotel)
VALUES (idAmenityHotel.NEXTVAL,'Wifi',1);

INSERT INTO amenityHotel(id, name,idHotel)
VALUES (idAmenityHotel.NEXTVAL,'Piscina',1);

INSERT INTO photo(id, name,idHotel)
VALUES (idPhoto.NEXTVAL,'hotelCarmen.jpg',1);

INSERT INTO photo(id, name,idHotel)
VALUES (idPhoto.NEXTVAL,'hotelCarmen2.jpg',1);

--rooms de cada hotel
INSERT INTO room(id, name,discountCode,discountPercentage, capacity, recommendPrice,idHotel)
VALUES (idRoom.NEXTVAL, 'B1', 'CODE100', 0.50, 4, 86000,1);

INSERT INTO roomXar(idRoom, idAmenityRoom)
VALUES (1,2);
--
INSERT INTO room(id, name, capacity, recommendPrice,idHotel)
VALUES (idRoom.NEXTVAL, 'B2', 1, 20000,1);

INSERT INTO roomXar(idRoom, idAmenityRoom)
VALUES (3,1);

--INSERTAR RESERVA/BOOKING


INSERT INTO booking(id, checkin,checkout, priceadministrator,idPerson,idRoom)
VALUES (idBooking.NEXTVAL,TO_DATE('27/04/2023', 'DD/MM/YY'),TO_DATE('30/04/2023', 'DD/MM/YY'),86000,2,1);
--
INSERT INTO booking(id, checkin,checkout, priceadministrator,idPerson,idRoom)
VALUES (idBooking.NEXTVAL,TO_DATE('04/05/2023', 'DD/MM/YY'),TO_DATE('08/05/2023', 'DD/MM/YY'),86000,3,1);

INSERT INTO booking(id, checkin,checkout, priceadministrator,idPerson,idRoom)
VALUES (idBooking.NEXTVAL,TO_DATE('09/05/2023', 'DD/MM/YY'),TO_DATE('12/05/2023', 'DD/MM/YY'),20000,3,3);

--agregar booking
INSERT INTO booking(id, checkin,checkout, priceadministrator,idPerson,idRoom)
VALUES (idBooking.NEXTVAL,TO_DATE('09/05/2023', 'DD/MM/YY'),TO_DATE('12/05/2023', 'DD/MM/YY'),20000,2,1);

INSERT INTO booking(id, checkin,checkout, priceadministrator,idPerson,idRoom)
VALUES (idBooking.NEXTVAL,TO_DATE('09/05/2023', 'DD/MM/YY'),TO_DATE('12/05/2023', 'DD/MM/YY'),20000,1,3);

INSERT INTO booking(id, checkin,checkout, priceadministrator,idPerson,idRoom)
VALUES (idBooking.NEXTVAL,TO_DATE('27/04/2023', 'DD/MM/YY'),TO_DATE('3/04/2023', 'DD/MM/YY'),20000,2,1);

--agregar otro hotel

INSERT INTO hotel(id, name, idDistrict)
VALUES (idHotel.NEXTVAL,'Palmera Caribe�a',1);

INSERT INTO cancellacionP(id, name, percentage, idHotel)
VALUES (idCancellacionP.NEXTVAL,'Pago Adelantado',20,2);

INSERT INTO amenityHotel(id, name,idHotel)
VALUES (idAmenityHotel.NEXTVAL,'Wifi',2);

INSERT INTO photo(id, name,idHotel)
VALUES (idPhoto.NEXTVAL,'palmera.jpg',2);

--rooms de cada hotel
INSERT INTO room(id, name, capacity, recommendPrice,idHotel)
VALUES (idRoom.NEXTVAL, 'A1', 2, 42000,2);

INSERT INTO roomXar(idRoom, idAmenityRoom)
VALUES (2,1);
--reserva en ese hotel nuevo
INSERT INTO booking(id, checkin,checkout, priceadministrator,idPerson,idRoom)
VALUES (idBooking.NEXTVAL,TO_DATE('07/06/2023', 'DD/MM/YY'),TO_DATE('09/06/2023', 'DD/MM/YY'),42000,3,2);




--insertar hotel fav a person
INSERT INTO personXHotelFAV(idPerson, idHotel)
VALUES (2,1);
INSERT INTO personXHotelFAV(idPerson, idHotel)
VALUES (2,2);