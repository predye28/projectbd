--funcion que muestre los hoteles
CREATE OR REPLACE FUNCTION getHotelTotal RETURN SYS_REFCURSOR
IS 
  hote SYS_REFCURSOR;
   
BEGIN 

  OPEN hote FOR 
    SELECT h.id, h.name
    FROM hotel h; 

  RETURN hote; 

END getHotelTotal;
--funcion para comprobar si el usuario ya ha registrado en ese hotel
CREATE OR REPLACE FUNCTION userRegisterBooking(pid NUMBER,pIdHotel NUMBER) RETURN BOOLEAN
IS
  cont NUMBER;
BEGIN
  SELECT COUNT(*) INTO cont
  FROM booking b
  INNER JOIN room r
  ON b.idRoom = r.id
  INNER JOIN hotel h
  ON r.idHotel = h.id
  WHERE b.idPerson = pid AND h.id = pIdHotel;

  IF cont > 0 THEN
    RETURN TRUE;
  ELSE
    RETURN FALSE;
  END IF;
  
END userRegisterBooking;

--procedimiento que agrega el hotel fav de una persona
CREATE OR REPLACE PROCEDURE personFavHotel(
    pIdPerson NUMBER,
    pIdHotel NUMBER
) AS

BEGIN
    INSERT INTO personXhotelFav(idPerson, idHotel)
    VALUES (pIdPerson,pIdHotel);
  COMMIT;
END;

--funcion que muestre las habitaciones
CREATE OR REPLACE FUNCTION getRoom(pIdHotel NUMBER) RETURN SYS_REFCURSOR
IS 
  roomCursor SYS_REFCURSOR;
BEGIN 

  OPEN roomCursor FOR 
    SELECT r.id, r.name, r.recommendPrice
    FROM room r; 

  RETURN roomCursor; 

END getRoom; 

--funcion que muestre toda la informacion de una habitacion en especifico
CREATE OR REPLACE FUNCTION getRoomFull(pIdRoom NUMBER) RETURN SYS_REFCURSOR
IS 
  roomFull SYS_REFCURSOR;
BEGIN 

  OPEN roomFull FOR 
    SELECT r.id, r.name, r.discountCode, r.capacity, r.recommendPRice
    FROM room r
    WHERE r.id = pIdRoom;
  RETURN roomFull; 

END getRoomFull; 

--FUNCION  que valide si ingreso el codigo de descuento correcto y actualice esa room
CREATE OR REPLACE FUNCTION checkCodeDiscount(pIdRoom NUMBER, pcode VARCHAR2) RETURN BOOLEAN
IS
  cont NUMBER;
BEGIN
  SELECT COUNT(*) INTO cont
  FROM room r
  WHERE r.id= pidRoom AND r.discountCode = pcode;

  IF cont > 0 THEN
    RETURN TRUE;
  ELSE
    RETURN FALSE;
  END IF;
  
END checkCodeDiscount;

--procedimiento que haga update al room actual del precio con el descuento 
CREATE OR REPLACE PROCEDURE updateRoomCodeDiscount(
    pIdRoom NUMBER
) AS

BEGIN
    UPDATE room r
    SET r.recommendPrice = r.recommendPrice *r.discountPercentage
    WHERE r.id = pIdRoom;
  COMMIT;
END;


--FUNCION QUE MUESTRE LOS TIPOS DE PAGOS
CREATE OR REPLACE FUNCTION getPayment RETURN SYS_REFCURSOR
IS 
  cursoTypeOfPayment SYS_REFCURSOR;
BEGIN 

  OPEN cursoTypeOfPayment FOR 
    SELECT t.id, t.name
    FROM typeOfPaymentm t;
  RETURN cursoTypeOfPayment; 

END getPayment; 

--funcion que registre la nueva reserva
CREATE OR REPLACE FUNCTION insertBooking(
    pCheckIn DATE,
    pCheckOut DATE,
    pPriceAdministrator NUMBER,
    pIdPerson NUMBER,
    pIdFeedBack NUMBER,
    pIdRoom NUMBER
    
) RETURN NUMBER AS
    reIdBookin NUMBER;
BEGIN
    --agregar hotel
    INSERT INTO booking(id, checkIn, checkout, priceAdministrator, IdPerson, idFeedBack,idRoom, payday)
    VALUES (idBooking.NEXTVAL, pCheckIn, pCheckOut, pPriceAdministrator, pIdPerson, pIdFeedBack,pIdRoom,SYSDATE);
    
    SELECT idBooking.CURRVAL INTO reIdBookin FROM dual;

  COMMIT;
  
  RETURN reIdBookin;
END;

--PROCEDIMENTO q actualize el preciorecomendado de room
CREATE OR REPLACE PROCEDURE updateRoomPrce(
    pIdRoom NUMBER,
    pNewPrice NUMBER
) AS

BEGIN
    UPDATE room r
    SET r.recommendPrice = pNewPrice
    WHERE r.id = pIdRoom;
  COMMIT;
END;

--FUNCION QUE MUESTRA LOS DATOS DE LA RESERVA DESEADA
CREATE OR REPLACE FUNCTION getbookin(pIdBooking NUMBER) RETURN SYS_REFCURSOR
IS 
  bookingCursor SYS_REFCURSOR;
BEGIN 

  OPEN bookingCursor FOR 
    SELECT b.id, b.checkin, b.checkout, b.priceadministrator
    FROM booking b 
    WHERE b.id = pIdBooking;

  RETURN bookingCursor; 

END getbookin; 

--funcion que registre la cantidad de estrellas y retorne su id
CREATE OR REPLACE FUNCTION insertRating(
    pName NUMBER
    
) RETURN NUMBER AS
    reIdRating NUMBER;
BEGIN
    --agregar hotel
    INSERT INTO RATING(id, name)
    VALUES (idRating.NEXTVAL, pName);
    
    SELECT idRating.CURRVAL INTO reIdRating FROM dual;

  COMMIT;
  
  RETURN reIdRating;
END;
--FUNCION QUE REGISTRE FEEDBACK 
CREATE OR REPLACE FUNCTION insertFeedBack(
    pCommentary VARCHAR2,
    pIdRating NUMBER
    
) RETURN NUMBER AS
    reIdFeedback NUMBER;
BEGIN
    --agregar hotel
    INSERT INTO backfeed(id, commentary, idrating)
    VALUES (idBackFeed.NEXTVAL, pCommentary, pIdRating);
    
    SELECT idBackFeed.CURRVAL INTO reIdFeedback FROM dual;

  COMMIT;
  
  RETURN reIdFeedback;
END;

--PROCEDIMIENTO QUE ACTUALIZE EL IDFeedBAck en booking
CREATE OR REPLACE PROCEDURE updateFeedBackBooking(
    pIdFeedBack NUMBER,
    pIdBooking NUMBER
) AS

BEGIN
    UPDATE booking b
    SET b.idFeedBack = pIdFeedBack
    WHERE b.id = pIdBooking;
  COMMIT;
END;

--funcion que retorne las estrellas que tiene el hotel establecido para que califiquen este mismo 
CREATE OR REPLACE FUNCTION getStarHotel RETURN SYS_REFCURSOR
IS 
  cursorStar SYS_REFCURSOR;
BEGIN 

  OPEN cursorStar FOR 
    SELECT b.id, b.name
    FROM bookingStar b;
  RETURN cursorStar; 

END getStarHotel; 

--funcion que valida si la reserva ingresada esta disponible
create or replace FUNCTION validBooking(pCheckIN DATE,pCheckOut DATE, pIdRoom NUMBER) RETURN BOOLEAN
IS
  cont NUMBER;
BEGIN
  SELECT COUNT(*) INTO cont
  FROM booking b
  WHERE (b.idRoom = pIdRoom) AND ((pCheckIN BETWEEN b.checkin AND b.checkout) OR (pCheckOut BETWEEN b.checkin AND b.checkout)) ;

  IF cont > 0 THEN
    RETURN FALSE;
  ELSE
    RETURN TRUE;
  END IF;
  
END validBooking;