--user
--Asignación de fks de la tabla user
ALTER TABLE user1
ADD
CONSTRAINT fk_user1 FOREIGN KEY(idPerson) REFERENCES person(id); 

ALTER TABLE user1
ADD
CONSTRAINT fk_user0 FOREIGN KEY(idTypeOfUser) REFERENCES typeOfUser(id); 

ALTER TABLE user1
ADD
CONSTRAINT fk_user2 FOREIGN KEY(idHotel) REFERENCES admin.hotel(id); 
-------------------------------------------------------------------------------------
--Asignación de fks de la tabla person
ALTER TABLE person
ADD
CONSTRAINT fk_person0 FOREIGN KEY(idDistrict) REFERENCES district(id); 

ALTER TABLE person
ADD
CONSTRAINT fk_person1 FOREIGN KEY(idGender) REFERENCES gender(id);

ALTER TABLE person
ADD
CONSTRAINT fk_person2 FOREIGN KEY(idTypeId) REFERENCES typeId(id);

-------------------------------------------------------------------------------------
--Asignación de las fks de la tabla phone

ALTER TABLE phone
ADD
CONSTRAINT fk_phone FOREIGN KEY(idPerson) REFERENCES person(id); 
------------------------------------------------------------------------------------
--Asignación de las fks de la tabla email
ALTER TABLE email 
ADD
CONSTRAINT fk_email FOREIGN KEY(idPerson) REFERENCES person(id); 
---------------------------------------------------------------------------------------
--Asignación de las fks de la tabla province
ALTER TABLE province 
ADD
CONSTRAINT fk_province FOREIGN KEY(idCountry) REFERENCES country(id); 
---------------------------------------------------------------------------------------
--Asignación de las fks de la tabla canton
ALTER TABLE canton
ADD
CONSTRAINT fk_canton FOREIGN KEY(idProvince) REFERENCES province(id); 
----------------------------------------------------------------------------------------
--Asignación de las fks de la tabla district
ALTER TABLE district
ADD
CONSTRAINT fk_district FOREIGN KEY(idCanton) REFERENCES canton(id); 
----------------------------------------------------------------------------------------
--Asiganación de las fks de la tabla nationalityXPerson
ALTER TABLE nationalityXP
ADD
CONSTRAINT fk_nationalityXP0 FOREIGN KEY(idPerson) REFERENCES person(id); 

ALTER TABLE nationalityXP
ADD
CONSTRAINT fk_nationalityXP1 FOREIGN KEY(idNationality) REFERENCES nationality(id);






--admiin




--------------------------------------------------------------------------------------------
----Asiganación de las fks de la tabla offer
ALTER TABLE offer
ADD
CONSTRAINT fk_offer0 FOREIGN KEY(idRoom) REFERENCES room(id); 

ALTER TABLE offer
ADD
CONSTRAINT fk_offer1 FOREIGN KEY(idHotel) REFERENCES hotel(id); 
-------------------------------------------------------------------------------------------
----Asiganación de las fks de la tabla feedBack
ALTER TABLE Backfeed
ADD
CONSTRAINT fk_Backfeed FOREIGN KEY(idRating) REFERENCES rating(id); 
-------------------------------------------------------------------------------------------
----Asignación de las fks de la tabla room
ALTER TABLE room
ADD
CONSTRAINT fk_room FOREIGN KEY(idHotel) REFERENCES hotel(id); 
--------------------------------------------------------------------------------------------
----Asignación de las fks de la tabla roomXAmenityRoom
ALTER TABLE roomXAR
ADD
CONSTRAINT fk_roomXAR0 FOREIGN KEY(idRoom) REFERENCES room(id); 

ALTER TABLE roomXAR
ADD
CONSTRAINT fk_roomXAR1 FOREIGN KEY(idAmenityRoom) REFERENCES amenityRoom(id); 
-----------------------------------------------------------------------------------------
----Asignación de las fks de la tabla amenityHotel
ALTER TABLE amenityHotel
ADD
CONSTRAINT fk_amenityHotel FOREIGN KEY(idHotel) REFERENCES hotel(id); 
-----------------------------------------------------------------------------------------
----Asignación de las fks de la tabla booking
ALTER TABLE booking
ADD
CONSTRAINT fk_booking0 FOREIGN KEY(idPerson) REFERENCES user1.person(id); 

ALTER TABLE booking
ADD
CONSTRAINT fk_booking1 FOREIGN KEY(idFeedBack) REFERENCES backFeed(id);  

ALTER TABLE booking
ADD
CONSTRAINT fk_booking2 FOREIGN KEY(idBookingStar) REFERENCES bookingStar(id); 

ALTER TABLE booking
ADD
CONSTRAINT fk_booking3 FOREIGN KEY(idRoom) REFERENCES room(id); 

-----------------------------------------------------------------------------------------
----Asignación de las fks de la tabla typeOfPaymentMethodXBooking

ALTER TABLE typeOfPayMXB
ADD
CONSTRAINT fk_typeOfPayMXB0 FOREIGN KEY(idBooking) REFERENCES booking(id); 

ALTER TABLE typeOfPayMXB
ADD
CONSTRAINT fk_typeOfPayMXB1 FOREIGN KEY(idtypeOfPayMXB) REFERENCES typeOfPaymentM(id); 
-----------------------------------------------------------------------------------------
----Asignación de las fks de la tabla hotel
ALTER TABLE hotel
ADD
CONSTRAINT fk_hotel0 FOREIGN KEY(idDistrict) REFERENCES user1.district(id); 

-----------------------------------------------------------------------------------------
----Asignación de las fks de la tabla cancellacionPolicy

ALTER TABLE cancellacionP
ADD
CONSTRAINT fk_cancellacionP FOREIGN KEY(idHotel) REFERENCES hotel(id); 

-----------------------------------------------------------------------------------------
----Asignación de las fks de la tabla cancellacionPolicyXBooking

ALTER TABLE cancelPXB
ADD
CONSTRAINT fk_cancelPXB0 FOREIGN KEY(idCancelP) REFERENCES cancellacionP(id); 

ALTER TABLE cancelPXB
ADD
CONSTRAINT fk_cancelPXB1 FOREIGN KEY(idBooking) REFERENCES booking(id); 

-----------------------------------------------------------------------------------------
----Asignación de las fks de la tabla cancellacionPolicyXBooking

ALTER TABLE photo
ADD
CONSTRAINT fk_photo FOREIGN KEY(idHotel) REFERENCES hotel(id); 

--------------------------
ALTER TABLE personXHotelFav
ADD
CONSTRAINT fk_personXHotelFav0 FOREIGN KEY(idPerson) REFERENCES user1.person(id); 

ALTER TABLE personXHotelFav
ADD
CONSTRAINT fk_personXHotelFav1 FOREIGN KEY(idHotel) REFERENCES hotel(id); 