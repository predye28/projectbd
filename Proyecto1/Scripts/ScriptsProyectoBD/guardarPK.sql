ALTER TABLE user1
ADD
CONSTRAINT pk_user1 PRIMARY KEY(identification)
USING INDEX
TABLESPACE ProyectoDBInd PCTFREE 20
STORAGE (INITIAL 10K NEXT 10K PCTINCREASE 0);

ALTER TABLE typeOfUser
ADD
CONSTRAINT pk_typeOfUser PRIMARY KEY(id)
USING INDEX
TABLESPACE ProyectoDBInd PCTFREE 20
STORAGE (INITIAL 10K NEXT 10K PCTINCREASE 0);

ALTER TABLE person
ADD
CONSTRAINT pk_person PRIMARY KEY(id)
USING INDEX
TABLESPACE ProyectoDBInd PCTFREE 20
STORAGE (INITIAL 10K NEXT 10K PCTINCREASE 0);
-----------------------------------------------------
ALTER TABLE gender
ADD
CONSTRAINT pk_gender PRIMARY KEY(id)
USING INDEX
TABLESPACE ProyectoDBInd PCTFREE 20
STORAGE (INITIAL 10K NEXT 10K PCTINCREASE 0);
-----------------------------------------------------
ALTER TABLE typeId
ADD
CONSTRAINT pk_typeId PRIMARY KEY(id)
USING INDEX
TABLESPACE ProyectoDBInd PCTFREE 20
STORAGE (INITIAL 10K NEXT 10K PCTINCREASE 0);
-----------------------------------------------------
ALTER TABLE phone
ADD
CONSTRAINT pk_phone PRIMARY KEY(id)
USING INDEX
TABLESPACE ProyectoDBInd PCTFREE 20
STORAGE (INITIAL 10K NEXT 10K PCTINCREASE 0);
------------------------------------------------------
ALTER TABLE email
ADD
CONSTRAINT pk_email PRIMARY KEY(id)
USING INDEX
TABLESPACE ProyectoDBInd PCTFREE 20
STORAGE (INITIAL 10K NEXT 10K PCTINCREASE 0);
------------------------------------------------------
ALTER TABLE country
ADD
CONSTRAINT pk_country PRIMARY KEY(id)
USING INDEX
TABLESPACE ProyectoDBInd PCTFREE 20
STORAGE (INITIAL 10K NEXT 10K PCTINCREASE 0);
-----------------------------------------------------
ALTER TABLE province
ADD
CONSTRAINT pk_province PRIMARY KEY(id)
USING INDEX
TABLESPACE ProyectoDBInd PCTFREE 20
STORAGE (INITIAL 10K NEXT 10K PCTINCREASE 0);
-----------------------------------------------------
ALTER TABLE canton
ADD
CONSTRAINT pk_canton PRIMARY KEY(id)
USING INDEX
TABLESPACE ProyectoDBInd PCTFREE 20
STORAGE (INITIAL 10K NEXT 10K PCTINCREASE 0);
-----------------------------------------------------
ALTER TABLE district
ADD
CONSTRAINT pk_district PRIMARY KEY(id)
USING INDEX
TABLESPACE ProyectoDBInd PCTFREE 20
STORAGE (INITIAL 10K NEXT 10K PCTINCREASE 0);
----------------------------------------------------
ALTER TABLE nationality
ADD
CONSTRAINT pk_nationality PRIMARY KEY(id)
USING INDEX
TABLESPACE ProyectoDBInd PCTFREE 20
STORAGE (INITIAL 10K NEXT 10K PCTINCREASE 0);
------------------------------------------------------
ALTER TABLE nationalityXP
ADD
CONSTRAINT pk_nationalityXP PRIMARY KEY(idPerson,idNationality)
USING INDEX
TABLESPACE ProyectoDBInd PCTFREE 20
STORAGE (INITIAL 10K NEXT 10K PCTINCREASE 0);

-----------------------------------------ADMIN

---------------------------------------------
---------------------------------------
ALTER TABLE parameter
ADD
CONSTRAINT pk_parameter PRIMARY KEY(id)
USING INDEX
TABLESPACE ProyectoDBInd PCTFREE 20
STORAGE (INITIAL 10K NEXT 10K PCTINCREASE 0);
---------------------------------------------------------
ALTER TABLE typeOfPaymentM
ADD
CONSTRAINT pk_typeOfPaymentM PRIMARY KEY(id)
USING INDEX
TABLESPACE ProyectoDBInd PCTFREE 20
STORAGE (INITIAL 10K NEXT 10K PCTINCREASE 0);
------------------------------------------------------
ALTER TABLE offer
ADD
CONSTRAINT pk_offer PRIMARY KEY(id)
USING INDEX
TABLESPACE ProyectoDBInd PCTFREE 20
STORAGE (INITIAL 10K NEXT 10K PCTINCREASE 0);
-----------------------------------------------------
ALTER TABLE backFeed
ADD
CONSTRAINT pk_backFeed PRIMARY KEY(id)
USING INDEX
TABLESPACE ProyectoDBInd PCTFREE 20
STORAGE (INITIAL 10K NEXT 10K PCTINCREASE 0);
-----------------------------------------------------
ALTER TABLE rating
ADD
CONSTRAINT pk_rating PRIMARY KEY(id)
USING INDEX
TABLESPACE ProyectoDBInd PCTFREE 20
STORAGE (INITIAL 10K NEXT 10K PCTINCREASE 0);
------------------------------------------------------
ALTER TABLE bookingStar
ADD
CONSTRAINT pk_bookingStar PRIMARY KEY(id)
USING INDEX
TABLESPACE ProyectoDBInd PCTFREE 20
STORAGE (INITIAL 10K NEXT 10K PCTINCREASE 0);
------------------------------------------------------
ALTER TABLE room
ADD
CONSTRAINT pk_room PRIMARY KEY(id)
USING INDEX
TABLESPACE ProyectoDBInd PCTFREE 20
STORAGE (INITIAL 10K NEXT 10K PCTINCREASE 0);
------------------------------------------------------
ALTER TABLE roomXAR
ADD
CONSTRAINT pk_roomXAR PRIMARY KEY(idRoom,idAmenityRoom)
USING INDEX
TABLESPACE ProyectoDBInd PCTFREE 20
STORAGE (INITIAL 10K NEXT 10K PCTINCREASE 0);
-------------------------------------------------------
ALTER TABLE amenityRoom
ADD
CONSTRAINT pk_amenityRoom PRIMARY KEY(id)
USING INDEX
TABLESPACE ProyectoDBInd PCTFREE 20
STORAGE (INITIAL 10K NEXT 10K PCTINCREASE 0);
---------------------------------------------------------
ALTER TABLE amenityHotel
ADD
CONSTRAINT pk_amenityHotel PRIMARY KEY(id)
USING INDEX
TABLESPACE ProyectoDBInd PCTFREE 20
STORAGE (INITIAL 10K NEXT 10K PCTINCREASE 0);
--------------------------------------------------------
ALTER TABLE booking
ADD
CONSTRAINT pk_booking PRIMARY KEY(id)
USING INDEX
TABLESPACE ProyectoDBInd PCTFREE 20
STORAGE (INITIAL 10K NEXT 10K PCTINCREASE 0);
------------------------------------------------------
ALTER TABLE typeOfPayMXB
ADD
CONSTRAINT pk_typeOfPayMXB PRIMARY KEY(id)
USING INDEX
TABLESPACE ProyectoDBInd PCTFREE 20
STORAGE (INITIAL 10K NEXT 10K PCTINCREASE 0);
------------------------------------------------------
ALTER TABLE hotel
ADD
CONSTRAINT pk_hotel PRIMARY KEY(id)
USING INDEX
TABLESPACE ProyectoDBInd PCTFREE 20
STORAGE (INITIAL 10K NEXT 10K PCTINCREASE 0);
-----------------------------------------------------
ALTER TABLE cancellacionP
ADD
CONSTRAINT pk_cancellacionP PRIMARY KEY(id)
USING INDEX
TABLESPACE ProyectoDBInd PCTFREE 20
STORAGE (INITIAL 10K NEXT 10K PCTINCREASE 0);
-------------------------------------------------------
ALTER TABLE cancelPXB
ADD
CONSTRAINT pk_cancelPXB PRIMARY KEY(id)
USING INDEX
TABLESPACE ProyectoDBInd PCTFREE 20
STORAGE (INITIAL 10K NEXT 10K PCTINCREASE 0);
-------------------------------------------------------
ALTER TABLE photo
ADD
CONSTRAINT pk_photo PRIMARY KEY(id)
USING INDEX
TABLESPACE ProyectoDBInd PCTFREE 20
STORAGE (INITIAL 10K NEXT 10K PCTINCREASE 0);
--------------------------
ALTER TABLE personXHotelFav
ADD
CONSTRAINT pk_personXHotelFav PRIMARY KEY(idPerson,idHotel)
USING INDEX
TABLESPACE ProyectoDBInd PCTFREE 20
STORAGE (INITIAL 10K NEXT 10K PCTINCREASE 0);