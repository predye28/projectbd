
CREATE TABLE user1
(
    identification NUMBER(6) CONSTRAINT user_identification_nn NOT NULL,
    password VARCHAR(25) CONSTRAINT user_password_nn NOT NULL,
    idPerson NUMBER(6) CONSTRAINT user_idPerson_nn NOT NULL,
    idTypeOfUser NUMBER(6) CONSTRAINT user_idTypeOfUser_nn NOT NULL,
    idHotel NUMBER(6) 
);

CREATE TABLE TypeOfUser
(
    id NUMBER(6) CONSTRAINT TypeOfUser_id_nn NOT NULL,
    name VARCHAR(25) CONSTRAINT TypeOfUser_name_nn NOT NULL
);

CREATE TABLE person
(
    id NUMBER(6) CONSTRAINT person_id_nn NOT NULL,
    firstName VARCHAR(25) CONSTRAINT person_firstName_nn NOT NULL,
    secondName VARCHAR(25) CONSTRAINT person_secondName_nn NOT NULL,
    firstLastName VARCHAR(25) CONSTRAINT person_firstLastName_nn NOT NULL,
    secondLastName VARCHAR(25) CONSTRAINT person_secondLastName_nn NOT NULL,
    birthDate DATE DEFAULT SYSDATE CONSTRAINT person_birthDate_nn NOT NULL,
    photo VARCHAR(25),
    idDistrict NUMBER(6) CONSTRAINT person_idDistrict_nn NOT NULL,
    idGender NUMBER(6) CONSTRAINT person_idGender_nn NOT NULL,
    idTypeId NUMBER(6) CONSTRAINT person_idTypeId_nn NOT NULL,
    idHotel NUMBER(6) CONSTRAINT person_idHote_nn NOT NULL
);

--ALTER TABLE person DROP CONSTRAINT person_photo_nn;


CREATE TABLE gender
(
    id NUMBER(6) CONSTRAINT gender_id_nn NOT NULL,
    name VARCHAR(25) CONSTRAINT gender_name_nn NOT NULL
);

CREATE TABLE typeId
(
    id NUMBER(6) CONSTRAINT typeId_id_nn NOT NULL,
    mask VARCHAR(25) CONSTRAINT typeId_mask_nn NOT NULL,
    identification VARCHAR(25) CONSTRAINT typeId_identification_nn NOT NULL
);


CREATE TABLE phone
(
    id NUMBER(6) CONSTRAINT phone_id_nn NOT NULL,
    phoneNumber VARCHAR(25) CONSTRAINT phone_phoneNumber_nn NOT NULL,
    idPerson NUMBER(6) CONSTRAINT phone_idPerson_nn NOT NULL
);

CREATE TABLE email
(
    id NUMBER(6) CONSTRAINT email_id_nn NOT NULL,
    name VARCHAR(25) CONSTRAINT email_name_nn NOT NULL,
    idPerson NUMBER(6) CONSTRAINT email_idPerson_nn NOT NULL
);

CREATE TABLE country
(
    id NUMBER(6) CONSTRAINT country_id_nn NOT NULL,
    name VARCHAR(25) CONSTRAINT country_name_nn NOT NULL
);

CREATE TABLE province
(
    id NUMBER(6) CONSTRAINT province_id_nn NOT NULL,
    name VARCHAR(25) CONSTRAINT province_name_nn NOT NULL,
    idCountry NUMBER(6) CONSTRAINT province_idCountry_nn NOT NULL
);

CREATE TABLE canton
(
    id NUMBER(6) CONSTRAINT canton_id_nn  NOT NULL,
    name VARCHAR(25) CONSTRAINT canton_name_nn NOT NULL,
    idProvince NUMBER(6) CONSTRAINT canton_idProvince_nn NOT NULL
);

CREATE TABLE district 
(
    id NUMBER(6) CONSTRAINT district_id_nn NOT NULL,
    name VARCHAR(25) CONSTRAINT district_name_nn NOT NULL,
    idCanton NUMBER(6) CONSTRAINT district_idCaton_nn NOT NULL
);

CREATE TABLE nationality  
(
    id NUMBER(6) CONSTRAINT nationality_id_nn NOT NULL,
    name VARCHAR(25) CONSTRAINT nationality_name_nn NOT NULL
);

--Esto es nationalityXperson pero el sql tiene un limite de caracteres para el modelo entonces básicamente lo dejamos "nationalityXp"
CREATE TABLE nationalityXP
(
    idPerson NUMBER(6) CONSTRAINT nationalityXP_idPerson_nn NOT NULL,
    idNationality NUMBER(6) CONSTRAINT nationalityXP_idNationality_nn NOT NULL
);
CREATE TABLE personXHotelFav
(
    idPerson NUMBER(6) CONSTRAINT personXHotelFav_idPerson_nn NOT NULL,
    idHotel NUMBER(6) CONSTRAINT personXHotelFav_idHotel_nn NOT NULL
);


