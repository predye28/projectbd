--GENDER
INSERT INTO gender(id, name)
VALUES (idGender.NEXTVAL,'Femenino');

INSERT INTO gender(id, name)
VALUES (idGender.NEXTVAL,'Masculino');

INSERT INTO gender(id, name)
VALUES (idGender.NEXTVAL,'Otro');

--typID
INSERT INTO typeId(id, mask, identification)
VALUES (idtypeId.NEXTVAL,'Cedula','0-0000-0000');
--tyeOFUser
INSERT INTO typeOfUser(id, name)
VALUES (idTypeOfUser.NEXTVAL,'User');

INSERT INTO typeOfUser(id, name)
VALUES (idTypeOfUser.NEXTVAL,'Admin');
--Nationality

INSERT INTO nationality(id, name)
VALUES (idNationality.NEXTVAL,'Costarricense');

INSERT INTO nationality(id, name)
VALUES (idNationality.NEXTVAL,'Salvadore�o');


--UBICACION GEOGRAFICA

--pais
INSERT INTO country(id, name)
VALUES (idCountry.NEXTVAL,'Costa Rica');

--provincia

INSERT INTO province(id, name,idCountry)
VALUES (idProvince.NEXTVAL,'San Jos�',1);

INSERT INTO province(id, name,idCountry)
VALUES (idProvince.NEXTVAL,'Alajuela',1);

INSERT INTO province(id, name,idCountry)
VALUES (idProvince.NEXTVAL,'Cartago',1);

INSERT INTO province(id, name,idCountry)
VALUES (idProvince.NEXTVAL,'Heredia',1);

INSERT INTO province(id, name,idCountry)
VALUES (idProvince.NEXTVAL,'Guanacaste',1);

INSERT INTO province(id, name,idCountry)
VALUES (idProvince.NEXTVAL,'Puntarenas',1);

INSERT INTO province(id, name,idCountry)
VALUES (idProvince.NEXTVAL,'Lim�n',1);



