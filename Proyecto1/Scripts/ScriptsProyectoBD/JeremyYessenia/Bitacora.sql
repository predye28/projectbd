--Tabla bitácora
CREATE TABLE binnacle(
    idBinnacle NUMBER(10 ) CONSTRAINT bitacora_nn_id NOT NULL,
	nameScheme VARCHAR(10) CONSTRAINT bitacora_nn_nameScheme NOT NULL,
	nameTable VARCHAR2(30) CONSTRAINT bitacora_nn_nameTable NOT NULL,
    nameField VARCHAR2(30) CONSTRAINT bitacora_nn_nameField  NOT NULL,
	valueBefore VARCHAR2(100),
	valueCurrent VARCHAR2(100)CONSTRAINT bitacora_nn_valueCurrent NOT NULL,
	creationDate DATE DEFAULT SYSDATE,
	lastUpdate DATE DEFAULT SYSDATE,
	creationBy VARCHAR2(10)CONSTRAINT bitacora_nn_creationBy NOT NULL,
	updateBy VARCHAR2(10) NULL
	
);

--Alteración de la pk
ALTER TABLE Binnacle
ADD
CONSTRAINT pk_idBinnacle PRIMARY KEY(idBinnacle)
USING INDEX
TABLESPACE ProyectoDBInd PCTFREE 20
STORAGE (INITIAL 10K NEXT 10K PCTINCREASE 0);


--Secuencia del id de la bitácora
CREATE SEQUENCE idBinnacle
    INCREMENT BY 1
    MINVALUE 0
    MAXVALUE 100000
    NOCACHE
    NOCYCLE;

--Trigger room para las actualizaciones del precio recomendado
create or replace TRIGGER beforeUpDateRecommendPrice
BEFORE INSERT OR UPDATE
OF recommendPrice
ON admin.room
FOR EACH ROW
BEGIN 
    INSERT INTO binnacle(idBinnacle,nameScheme,nameTable,nameField,lastUpdate,valueBefore,valueCurrent,CreationBY)
    VALUES(idBinnacle.nextval,'admin','room','recommendPrice',SYSDATE, :old.recommendPrice, :new.recommendPrice,USER);
END beforeUpDateRecommendPrice;
--Trigger amenityHotel- NO
create or replace TRIGGER beforeUpDateAmenityHotel
BEFORE INSERT OR UPDATE
OF name
ON amenityHotel
FOR EACH ROW
BEGIN 
    INSERT INTO binnacle(idBinnacle,nameScheme,nameTable,nameField,lastUpdate,valueBefore,valueCurrent,CreationBY)
    VALUES(idBinnacle.nextval,'admin','amenityHotel','name',SYSDATE, :old.name, :new.name,USER);
END beforeUpDateAmenityHotel;
----Trigger amenityRoom- NO
create or replace TRIGGER beforeUpDateAmenityRoom
BEFORE INSERT OR UPDATE
OF name
ON amenityRoom
FOR EACH ROW
BEGIN 
    INSERT INTO binnacle(idBinnacle,nameScheme,nameTable,nameField,lastUpdate,valueBefore,valueCurrent,CreationBY)
    VALUES(idBinnacle.nextval,'admin','amenityRoom','name',SYSDATE, :old.name, :new.name,USER);
END beforeUpDateAmenityRoom;
---------------Trigger booking checkIn --NO
create or replace TRIGGER beforeUpDateBookingCheckIn
BEFORE INSERT OR UPDATE
OF checkIn
ON booking
FOR EACH ROW
BEGIN 
    INSERT INTO binnacle(idBinnacle,nameScheme,nameTable,nameField,lastUpdate,valueBefore,valueCurrent,CreationBY)
    VALUES(idBinnacle.nextval,'admin','booking','checkIn',SYSDATE, :old.checkIn, :new.checkIn,USER);
END beforeUpDateBookingCheckIn;
--------Trigger booking checkOut -- NO
create or replace TRIGGER beforeUpDateBookingCheckOut
BEFORE INSERT OR UPDATE
OF checkOut
ON booking
FOR EACH ROW
BEGIN 
    INSERT INTO binnacle(idBinnacle,nameScheme,nameTable,nameField,lastUpdate,valueBefore,valueCurrent,CreationBY)
    VALUES(idBinnacle.nextval,'admin','booking','checkOut',SYSDATE, :old.checkOut, :new.checkOut,USER);
END beforeUpDateBookingCheckOut;
------Trigger booking priceAdministrator
create or replace TRIGGER beforeUpDateBookingPrice
BEFORE INSERT OR UPDATE
OF priceAdministrator
ON booking
FOR EACH ROW
BEGIN 
    INSERT INTO binnacle(idBinnacle,nameScheme,nameTable,nameField,lastUpdate,valueBefore,valueCurrent,CreationBY)
    VALUES(idBinnacle.nextval,'admin','booking','priceAdministrator',SYSDATE, :old.priceAdministrator, :new.priceAdministrator,USER);
END beforeUpDateBookingPrice;
--------Trigger CancellacionP name --Pendiente
create or replace TRIGGER beforeUpDateCancellacionP
BEFORE INSERT OR UPDATE
OF name
ON cancellacionP
FOR EACH ROW
BEGIN 
    INSERT INTO binnacle(idBinnacle,nameScheme,nameTable,nameField,lastUpdate,valueBefore,valueCurrent,CreationBY)
    VALUES(idBinnacle.nextval,'admin','cancellacionP','name',SYSDATE, :old.name, :new.name,USER);
END beforeUpDateCancellacionP;
--------Trigger CancellacionP percentage -- NO
create or replace TRIGGER beforeUpDatePercentage
BEFORE INSERT OR UPDATE
OF percentage
ON cancellacionP
FOR EACH ROW
BEGIN 
    INSERT INTO binnacle(idBinnacle,nameScheme,nameTable,nameField,lastUpdate,valueBefore,valueCurrent,CreationBY)
    VALUES(idBinnacle.nextval,'admin','cancellacionP','percentage',SYSDATE, :old.percentage, :new.percentage,USER);
END beforeUpDatePercentage;
----------Trigger Offer name -- NO
create or replace TRIGGER beforeUpDateOfferName
BEFORE INSERT OR UPDATE
OF name
ON Offer
FOR EACH ROW
BEGIN 
    INSERT INTO binnacle(idBinnacle,nameScheme,nameTable,nameField,lastUpdate,valueBefore,valueCurrent,CreationBY)
    VALUES(idBinnacle.nextval,'admin','Offer','name',SYSDATE, :old.name, :new.name,USER);
END beforeUpDateOfferName;
------Trigger Offer minimunDay --NO Parametrizable
create or replace TRIGGER beforeUpDateOfferMiniumDay
BEFORE INSERT OR UPDATE
OF miniumDay
ON Offer
FOR EACH ROW
BEGIN 
    INSERT INTO binnacle(idBinnacle,nameScheme,nameTable,nameField,lastUpdate,valueBefore,valueCurrent,CreationBY)
    VALUES(idBinnacle.nextval,'admin','Offer','miniumDay',SYSDATE, :old.miniumDay, :new.miniumDay,USER);
END beforeUpDateOfferMiniumDay;
------Trigger Offer description --No
create or replace TRIGGER beforeUpDateOfferDescription
BEFORE INSERT OR UPDATE
OF description
ON Offer
FOR EACH ROW
BEGIN 
    INSERT INTO binnacle(idBinnacle,nameScheme,nameTable,nameField,lastUpdate,valueBefore,valueCurrent,CreationBY)
    VALUES(idBinnacle.nextval,'admin','Offer','miniumDay',SYSDATE, :old.description, :new.description,USER);
END beforeUpDateOfferDescription;
-----Trigger Offer StartDate --NO
create or replace TRIGGER beforeUpDateOfferStartDate
BEFORE INSERT OR UPDATE
OF startDate
ON Offer
FOR EACH ROW
BEGIN 
    INSERT INTO binnacle(idBinnacle,nameScheme,nameTable,nameField,lastUpdate,valueBefore,valueCurrent,CreationBY)
    VALUES(idBinnacle.nextval,'admin','Offer','startDate',SYSDATE, :old.startDate, :new.startDate,USER);
END beforeUpDateOfferStartDate;
----Trigger Offer finishDate --NO
create or replace TRIGGER beforeUpDateOfferFinishDate
BEFORE INSERT OR UPDATE
OF finishDate
ON Offer
FOR EACH ROW
BEGIN 
    INSERT INTO binnacle(idBinnacle,nameScheme,nameTable,nameField,lastUpdate,valueBefore,valueCurrent,CreationBY)
    VALUES(idBinnacle.nextval,'admin','Offer','finishDate',SYSDATE, :old.finishDate, :new.finishDate,USER);
END beforeUpDateOfferFinishDate;
----Trigger parameter name --NO parametrizable
create or replace TRIGGER beforeUpDateParameterName
BEFORE INSERT OR UPDATE
OF name
ON parameter
FOR EACH ROW
BEGIN 
    INSERT INTO binnacle(idBinnacle,nameScheme,nameTable,nameField,lastUpdate,valueBefore,valueCurrent,CreationBY)
    VALUES(idBinnacle.nextval,'admin','Offer','name',SYSDATE, :old.name, :new.name,USER);
END beforeUpDateParameterName;
----Trigger parameter value -- NO parametrizable
create or replace TRIGGER beforeUpDateParameterValue
BEFORE INSERT OR UPDATE
OF value
ON parameter
FOR EACH ROW
BEGIN 
    INSERT INTO binnacle(idBinnacle,nameScheme,nameTable,nameField,lastUpdate,valueBefore,valueCurrent,CreationBY)
    VALUES(idBinnacle.nextval,'admin','Parameter','value',SYSDATE, :old.value, :new.value,USER);
END beforeUpDateParameterValue;
------Trigger photo -- NO parametrizable
create or replace TRIGGER beforeUpDatePhoto
BEFORE INSERT OR UPDATE
OF name
ON photo
FOR EACH ROW
BEGIN 
    INSERT INTO binnacle(idBinnacle,nameScheme,nameTable,nameField,lastUpdate,valueBefore,valueCurrent,CreationBY)
    VALUES(idBinnacle.nextval,'admin','Photo','name',SYSDATE, :old.name, :new.name,USER);
END beforeUpDatePhoto;
-----Trigger Room Name -No parametrizable
create or replace TRIGGER beforeUpDateRoomName
BEFORE INSERT OR UPDATE
OF name
ON room
FOR EACH ROW
BEGIN 
    INSERT INTO binnacle(idBinnacle,nameScheme,nameTable,nameField,lastUpdate,valueBefore,valueCurrent,CreationBY)
    VALUES(idBinnacle.nextval,'admin','Room','name',SYSDATE, :old.name, :new.name,USER);
END beforeUpDateRoomName;
----Trigger Room discountCode -No parametrizable
create or replace TRIGGER beforeUpDateRoomDiscountCode
BEFORE INSERT OR UPDATE
OF discountCode
ON room
FOR EACH ROW
BEGIN 
    INSERT INTO binnacle(idBinnacle,nameScheme,nameTable,nameField,lastUpdate,valueBefore,valueCurrent,CreationBY)
    VALUES(idBinnacle.nextval,'admin','Room','discountCode',SYSDATE, :old.discountCode, :new.discountCode,USER);
END beforeUpDateRoomDiscountCode;
-----Trigger Room discountPercentage --No parametrizable
create or replace TRIGGER beforeUpDateRoomDp
BEFORE INSERT OR UPDATE
OF discountPercentage
ON room
FOR EACH ROW
BEGIN 
    INSERT INTO binnacle(idBinnacle,nameScheme,nameTable,nameField,lastUpdate,valueBefore,valueCurrent,CreationBY)
    VALUES(idBinnacle.nextval,'admin','Room','discountPercentage',SYSDATE, :old.discountPercentage, :new.discountPercentage,USER);
END beforeUpDateRoomDP;
----------Trigger Room capacity -No parametrizable
create or replace TRIGGER beforeUpDateRoomC
BEFORE INSERT OR UPDATE
OF capacity
ON room
FOR EACH ROW
BEGIN 
    INSERT INTO binnacle(idBinnacle,nameScheme,nameTable,nameField,lastUpdate,valueBefore,valueCurrent,CreationBY)
    VALUES(idBinnacle.nextval,'admin','Room','capacity',SYSDATE, :old.capacity, :new.capacity,USER);
END beforeUpDateRoomC;
-----Trigger TypeOfPaymentM name -No parametrizable
create or replace TRIGGER beforeUpDateTypeOfPaymentM 
BEFORE INSERT OR UPDATE
OF name
ON TypeOfPaymentM 
FOR EACH ROW
BEGIN 
    INSERT INTO binnacle(idBinnacle,nameScheme,nameTable,nameField,lastUpdate,valueBefore,valueCurrent,CreationBY)
    VALUES(idBinnacle.nextval,'admin','TypeOfPaymentM ','name',SYSDATE, :old.name, :new.name,USER);
END beforeUpDateTypeOfPaymentM ;