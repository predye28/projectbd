-------------creación de triggers de user
CREATE OR REPLACE TRIGGER user1.beforeInsertCanton
BEFORE INSERT 
ON user1.canton
FOR EACH ROW
BEGIN 
    :new.creationBy := USER;
    :new.creationDate := SYSDATE;
   
END beforeInsertCanton;

CREATE OR REPLACE TRIGGER user1.beforeUpdateCanton

BEFORE UPDATE 
ON user1.canton
FOR EACH ROW
BEGIN 
    :new.lastUpdate :=  SYSDATE;
    :new.updateby := USER;
END beforeUpdateCanton;
---------------------------------------------------------------
CREATE OR REPLACE TRIGGER user1.beforeInsertCountry
BEFORE INSERT 
ON user1.country
FOR EACH ROW
BEGIN 
    :new.creationBy := USER;
    :new.creationDate := SYSDATE;
   
END beforeInsertCountry;

CREATE OR REPLACE TRIGGER user1.beforeUpdateCountry

BEFORE UPDATE 
ON user1.country
FOR EACH ROW
BEGIN 
    :new.lastUpdate :=  SYSDATE;
    :new.updateby := USER;
END beforeUpdateCountry;
-----------------------------------------
CREATE OR REPLACE TRIGGER user1.beforeInsertDistrict
BEFORE INSERT 
ON user1.district
FOR EACH ROW
BEGIN 
    :new.creationBy := USER;
    :new.creationDate := SYSDATE;
   
END beforeInsertDistrict;

CREATE OR REPLACE TRIGGER user1.beforeUpdateDistrict
BEFORE UPDATE 
ON user1.district
FOR EACH ROW
BEGIN 
    :new.lastUpdate :=  SYSDATE;
    :new.updateby := USER;
END beforeUpdateDistrict;
----------------------------------------------------
CREATE OR REPLACE TRIGGER user1.beforeInsertEmail
BEFORE INSERT 
ON user1.email
FOR EACH ROW
BEGIN 
    :new.creationBy := USER;
    :new.creationDate := SYSDATE;
   
END beforeInsertEmail;

CREATE OR REPLACE TRIGGER user1.beforeUpdateEmail
BEFORE UPDATE 
ON user1.email
FOR EACH ROW
BEGIN 
    :new.lastUpdate :=  SYSDATE;
    :new.updateby := USER;
END beforeUpdateEmail;
----------------------------------------------------
CREATE OR REPLACE TRIGGER user1.beforeInsertGender
BEFORE INSERT 
ON user1.gender
FOR EACH ROW
BEGIN 
    :new.creationBy := USER;
    :new.creationDate := SYSDATE;
   
END beforeInsertGender;

CREATE OR REPLACE TRIGGER user1.beforeUpdateGender
BEFORE UPDATE 
ON user1.gender
FOR EACH ROW
BEGIN 
    :new.lastUpdate :=  SYSDATE;
    :new.updateby := USER;
END beforeUpdateGender;
--------------------------------------------------
CREATE OR REPLACE TRIGGER user1.beforeInsertNationality
BEFORE INSERT 
ON user1.nationality
FOR EACH ROW
BEGIN 
    :new.creationBy := USER;
    :new.creationDate := SYSDATE;
   
END beforeInsertNationality;

CREATE OR REPLACE TRIGGER user1.beforeUpdateNationality
BEFORE UPDATE 
ON user1.nationality
FOR EACH ROW
BEGIN 
    :new.lastUpdate :=  SYSDATE;
    :new.updateby := USER;
END beforeUpdateNationality;
------------------------------------------------------
CREATE OR REPLACE TRIGGER user1.beforeInsertNationalityXP
BEFORE INSERT 
ON user1.nationalityxp
FOR EACH ROW
BEGIN 
    :new.creationBy := USER;
    :new.creationDate := SYSDATE;
   
END beforeInsertNationalityXP;

CREATE OR REPLACE TRIGGER user1.beforeUpdateNationalityXP
BEFORE UPDATE 
ON user1.nationalityxp
FOR EACH ROW
BEGIN 
    :new.lastUpdate :=  SYSDATE;
    :new.updateby := USER;
END beforeUpdateNationalityXP;
------------------------------------------------------
CREATE OR REPLACE TRIGGER user1.beforeInsertPerson
BEFORE INSERT 
ON user1.person
FOR EACH ROW
BEGIN 
    :new.creationBy := USER;
    :new.creationDate := SYSDATE;
   
END beforeInsertPerson;

CREATE OR REPLACE TRIGGER user1.beforeUpdatePerson
BEFORE UPDATE 
ON user1.person
FOR EACH ROW
BEGIN 
    :new.lastUpdate :=  SYSDATE;
    :new.updateby := USER;
END beforeUpdatePerson;

---------------------------------------------------------
CREATE OR REPLACE TRIGGER user1.beforeInsertPhone
BEFORE INSERT 
ON user1.phone
FOR EACH ROW
BEGIN 
    :new.creationBy := USER;
    :new.creationDate := SYSDATE;
   
END beforeInsertPhone;

CREATE OR REPLACE TRIGGER user1.beforeUpdatePhone
BEFORE UPDATE 
ON user1.phone
FOR EACH ROW
BEGIN 
    :new.lastUpdate :=  SYSDATE;
    :new.updateby := USER;
END beforeUpdatePhone;
-----------------------------------------------------------
CREATE OR REPLACE TRIGGER user1.beforeInsertProvince
BEFORE INSERT 
ON user1.province
FOR EACH ROW
BEGIN 
    :new.creationBy := USER;
    :new.creationDate := SYSDATE;
   
END beforeInsertProvince;

CREATE OR REPLACE TRIGGER user1.beforeUpdateProvince
BEFORE UPDATE 
ON user1.province
FOR EACH ROW
BEGIN 
    :new.lastUpdate :=  SYSDATE;
    :new.updateby := USER;
END beforeUpdateProvince;
-----------------------------------------------------------
CREATE OR REPLACE TRIGGER user1.beforeInsertTypeId
BEFORE INSERT 
ON user1.typeId
FOR EACH ROW
BEGIN 
    :new.creationBy := USER;
    :new.creationDate := SYSDATE;
   
END beforeInsertTypeId;

CREATE OR REPLACE TRIGGER user1.beforeUpdateTypeId
BEFORE UPDATE 
ON user1.typeId
FOR EACH ROW
BEGIN 
    :new.lastUpdate :=  SYSDATE;
    :new.updateby := USER;
END beforeUpdateTypeId;
-----------------------------------------------------------
CREATE OR REPLACE TRIGGER user1.beforeInsertTypeOfUser
BEFORE INSERT 
ON user1.typeOfUser
FOR EACH ROW
BEGIN 
    :new.creationBy := USER;
    :new.creationDate := SYSDATE;
   
END beforeInsertTypeId;

CREATE OR REPLACE TRIGGER user1.beforeUpdateTypeOfUser
BEFORE UPDATE 
ON user1.typeOfUser
FOR EACH ROW
BEGIN 
    :new.lastUpdate :=  SYSDATE;
    :new.updateby := USER;
END beforeUpdateTypeOfUser;
-----------------------------------------------------------
CREATE OR REPLACE TRIGGER user1.beforeInsertUser1
BEFORE INSERT 
ON user1.user1
FOR EACH ROW
BEGIN 
    :new.creationBy := USER;
    :new.creationDate := SYSDATE;
   
END beforeInsertUser1;

CREATE OR REPLACE TRIGGER user1.beforeUpdateUser1
BEFORE UPDATE 
ON user1.user1
FOR EACH ROW
BEGIN 
    :new.lastUpdate :=  SYSDATE;
    :new.updateby := USER;
END beforeUpdateUser1;