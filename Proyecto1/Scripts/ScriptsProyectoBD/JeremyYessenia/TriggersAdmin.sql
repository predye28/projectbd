--Creación Triggers
--Tabla AmenityHotel
CREATE OR REPLACE TRIGGER admin.beforeInsertAmenityHotel
BEFORE INSERT 
ON admin.AmenityHotel
FOR EACH ROW
BEGIN 
    :new.creationBy := USER;
    :new.creationDate := SYSDATE;

END beforeInsertAmenityHotel;

CREATE OR REPLACE TRIGGER admin.beforeUpdateAmenityHotel
BEFORE UPDATE 
ON admin.AmenityHotel
FOR EACH ROW
BEGIN 
    :new.lastUpdate :=  SYSDATE;
    :new.updateby := USER;
END beforeUpdateAmenityHotel;
--------------------------------------------------------------
--Tabla AmenityRoom
CREATE OR REPLACE TRIGGER admin.beforeInsertAmenityRoom
BEFORE INSERT 
ON admin.AmenityRoom
FOR EACH ROW
BEGIN 
    :new.creationBy := USER;
    :new.creationDate := SYSDATE;

END beforeInsertAmenityRoom;

CREATE OR REPLACE TRIGGER admin.beforeUpdateAmenityRoom
BEFORE UPDATE 
ON admin.AmenityRoom
FOR EACH ROW
BEGIN 
    :new.lastUpdate :=  SYSDATE;
    :new.updateby := USER;
END beforeUpdateAmenityRoom;
--------------------------------------------------------------
--Tabla Backfeed
CREATE OR REPLACE TRIGGER admin.beforeInsertBackfeed
BEFORE INSERT 
ON admin.Backfeed
FOR EACH ROW
BEGIN 
    :new.creationBy := USER;
    :new.creationDate := SYSDATE;

END beforeInsertBackfeed;

CREATE OR REPLACE TRIGGER admin.beforeUpdateBackfeed
BEFORE UPDATE 
ON admin.Backfeed
FOR EACH ROW
BEGIN 
    :new.lastUpdate :=  SYSDATE;
    :new.updateby := USER;
END beforeUpdateBackfeed;
--------------------------------------------------------------
--Tabla Booking
CREATE OR REPLACE TRIGGER admin.beforeInsertBooking
BEFORE INSERT 
ON admin.Booking
FOR EACH ROW
BEGIN 
    :new.creationBy := USER;
    :new.creationDate := SYSDATE;

END beforeInsertBackfeed;

CREATE OR REPLACE TRIGGER admin.beforeUpdateBooking
BEFORE UPDATE 
ON admin.Booking
FOR EACH ROW
BEGIN 
    :new.lastUpdate :=  SYSDATE;
    :new.updateby := USER;
END beforeUpdateBooking;
--------------------------------------------------------------
--Tabla BookingStar
CREATE OR REPLACE TRIGGER admin.beforeInsertBookingStar
BEFORE INSERT 
ON admin.BookingStar
FOR EACH ROW
BEGIN 
    :new.creationBy := USER;
    :new.creationDate := SYSDATE;

END beforeInsertBookingStar;

CREATE OR REPLACE TRIGGER admin.beforeUpdateBookingStar
BEFORE UPDATE 
ON admin.BookingStar
FOR EACH ROW
BEGIN 
    :new.lastUpdate :=  SYSDATE;
    :new.updateby := USER;
END beforeUpdateBookingStar;
--------------------------------------------------------------
--Tabla CancellacionP
CREATE OR REPLACE TRIGGER admin.beforeInsertCancellacionP
BEFORE INSERT 
ON admin.CancellacionP
FOR EACH ROW
BEGIN 
    :new.creationBy := USER;
    :new.creationDate := SYSDATE;

END beforeInsertCancellacionP;

CREATE OR REPLACE TRIGGER admin.beforeUpdateCancellacionP
BEFORE UPDATE 
ON admin.CancellacionP
FOR EACH ROW
BEGIN 
    :new.lastUpdate :=  SYSDATE;
    :new.updateby := USER;
END beforeUpdateCancellacionP;
--------------------------------------------------------------
--Tabla CancelPxB
CREATE OR REPLACE TRIGGER admin.beforeInsertCancelPxB
BEFORE INSERT 
ON admin.CancelPxB
FOR EACH ROW
BEGIN 
    :new.creationBy := USER;
    :new.creationDate := SYSDATE;

END beforeInsertCancelPxB;

CREATE OR REPLACE TRIGGER admin.beforeUpdateCancelPxB
BEFORE UPDATE 
ON admin.CancelPxB
FOR EACH ROW
BEGIN 
    :new.lastUpdate :=  SYSDATE;
    :new.updateby := USER;
END beforeUpdateCancelPxB;
--------------------------------------------------------------
--Tabla Hotel
CREATE OR REPLACE TRIGGER admin.beforeInsertHotel
BEFORE INSERT 
ON admin.Hotel
FOR EACH ROW
BEGIN 
    :new.creationBy := USER;
    :new.creationDate := SYSDATE;

END beforeInsertHotel;

CREATE OR REPLACE TRIGGER admin.beforeUpdateHotel
BEFORE UPDATE 
ON admin.Hotel
FOR EACH ROW
BEGIN 
    :new.lastUpdate :=  SYSDATE;
    :new.updateby := USER;
END beforeUpdateHotel;
--------------------------------------------------------------
--Tabla Offer
CREATE OR REPLACE TRIGGER admin.beforeInsertOffer
BEFORE INSERT 
ON admin.Offer
FOR EACH ROW
BEGIN 
    :new.creationBy := USER;
    :new.creationDate := SYSDATE;

END beforeInsertOffer;

CREATE OR REPLACE TRIGGER admin.beforeUpdateOffer
BEFORE UPDATE 
ON admin.Offer
FOR EACH ROW
BEGIN 
    :new.lastUpdate :=  SYSDATE;
    :new.updateby := USER;
END beforeUpdateOffer;
--------------------------------------------------------------
--Tabla Parameter
CREATE OR REPLACE TRIGGER admin.beforeInsertParameter
BEFORE INSERT 
ON admin.Parameter
FOR EACH ROW
BEGIN 
    :new.creationBy := USER;
    :new.creationDate := SYSDATE;

END beforeInsertParameter;

CREATE OR REPLACE TRIGGER admin.beforeUpdateParameter
BEFORE UPDATE 
ON admin.Parameter
FOR EACH ROW
BEGIN 
    :new.lastUpdate :=  SYSDATE;
    :new.updateby := USER;
END beforeUpdateParameter;

--------------------------------------------------------------
--Tabla PersonXhotelFav
CREATE OR REPLACE TRIGGER admin.beforeInsertPersonXhotelFav
BEFORE INSERT 
ON admin.PersonXhotelFav
FOR EACH ROW
BEGIN 
    :new.creationBy := USER;
    :new.creationDate := SYSDATE;

END beforeInsertPersonXhotelFav;

CREATE OR REPLACE TRIGGER admin.beforeUpdatePersonXhotelFav
BEFORE UPDATE 
ON admin.PersonXhotelFav
FOR EACH ROW
BEGIN 
    :new.lastUpdate :=  SYSDATE;
    :new.updateby := USER;
END beforeUpdatePersonXhotelFav;
--------------------------------------------------------------
--Tabla Photo
CREATE OR REPLACE TRIGGER admin.beforeInsertPhoto
BEFORE INSERT 
ON admin.Photo
FOR EACH ROW
BEGIN 
    :new.creationBy := USER;
    :new.creationDate := SYSDATE;

END beforeInsertPhoto;

CREATE OR REPLACE TRIGGER admin.beforeUpdatePhoto
BEFORE UPDATE 
ON admin.Photo
FOR EACH ROW
BEGIN 
    :new.lastUpdate :=  SYSDATE;
    :new.updateby := USER;
END beforeUpdatePhoto;
--------------------------------------------------------------
--Tabla Rating
CREATE OR REPLACE TRIGGER admin.beforeInsertRating
BEFORE INSERT 
ON admin.Rating
FOR EACH ROW
BEGIN 
    :new.creationBy := USER;
    :new.creationDate := SYSDATE;

END beforeInsertRating;

CREATE OR REPLACE TRIGGER admin.beforeUpdateRating
BEFORE UPDATE 
ON admin.Rating
FOR EACH ROW
BEGIN 
    :new.lastUpdate :=  SYSDATE;
    :new.updateby := USER;
END beforeUpdateRating;
--------------------------------------------------------------
--Tabla Room
CREATE OR REPLACE TRIGGER admin.beforeInsertRoom
BEFORE INSERT 
ON admin.Room
FOR EACH ROW
BEGIN 
    :new.creationBy := USER;
    :new.creationDate := SYSDATE;

END beforeInsertRoom;

CREATE OR REPLACE TRIGGER admin.beforeUpdateRoom
BEFORE UPDATE 
ON admin.Room
FOR EACH ROW
BEGIN 
    :new.lastUpdate :=  SYSDATE;
    :new.updateby := USER;
END beforeUpdateRoom;
--------------------------------------------------------------
--Tabla RoomXar
CREATE OR REPLACE TRIGGER admin.beforeInsertRoomXar
BEFORE INSERT 
ON admin.RoomXar
FOR EACH ROW
BEGIN 
    :new.creationBy := USER;
    :new.creationDate := SYSDATE;

END beforeInsertRoomXar;

CREATE OR REPLACE TRIGGER admin.beforeUpdateRoomXar
BEFORE UPDATE 
ON admin.RoomXar
FOR EACH ROW
BEGIN 
    :new.lastUpdate :=  SYSDATE;
    :new.updateby := USER;
END beforeUpdateRoomXar;
--------------------------------------------------------------
--Tabla TypeOfPaymentm
CREATE OR REPLACE TRIGGER admin.beforeInsertTypeOfPaymentm
BEFORE INSERT 
ON admin.TypeOfPaymentm
FOR EACH ROW
BEGIN 
    :new.creationBy := USER;
    :new.creationDate := SYSDATE;

END beforeInsertTypeOfPaymentm;

CREATE OR REPLACE TRIGGER admin.beforeUpdateTypeOfPaymentm
BEFORE UPDATE 
ON admin.TypeOfPaymentm
FOR EACH ROW
BEGIN 
    :new.lastUpdate :=  SYSDATE;
    :new.updateby := USER;
END beforeUpdateTypeOfPaymentm;
--------------------------------------------------------------
--Tabla TypeOfPayMXB
CREATE OR REPLACE TRIGGER admin.beforeInsertTypeOfPayMXB
BEFORE INSERT 
ON admin.TypeOfPayMXB
FOR EACH ROW
BEGIN 
    :new.creationBy := USER;
    :new.creationDate := SYSDATE;

END beforeInsertTypeOfPayMXB;

CREATE OR REPLACE TRIGGER admin.beforeUpdateTypeOfPayMXB
BEFORE UPDATE 
ON admin.TypeOfPayMXB
FOR EACH ROW
BEGIN 
    :new.lastUpdate :=  SYSDATE;
    :new.updateby := USER;
END beforeUpdateTypeOfPayMXB;