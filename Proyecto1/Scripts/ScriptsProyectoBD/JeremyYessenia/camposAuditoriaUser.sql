-----------Creación de los campos de auditoria en usuario.
ALTER TABLE canton
ADD creationDate DATE DEFAULT SYSDATE NOT NULL;

ALTER TABLE canton
ADD lastUpdate DATE DEFAULT SYSDATE NOT NULL;

ALTER TABLE canton
ADD creationBy VARCHAR2(25);

ALTER TABLE canton
ADD updateBy VARCHAR2(25);

--------------------------------
ALTER TABLE country
ADD creationDate DATE DEFAULT SYSDATE NOT NULL;

ALTER TABLE country
ADD lastUpdate DATE DEFAULT SYSDATE NOT NULL;

ALTER TABLE country
ADD creationBy VARCHAR2(25);

ALTER TABLE country
ADD updateBy VARCHAR2(25);
---------------------------------
ALTER TABLE district
ADD creationDate DATE DEFAULT SYSDATE NOT NULL;

ALTER TABLE district
ADD lastUpdate DATE DEFAULT SYSDATE NOT NULL;

ALTER TABLE district
ADD creationBy VARCHAR2(25);

ALTER TABLE district
ADD updateBy VARCHAR2(25);
---------------------------
ALTER TABLE email
ADD creationDate DATE DEFAULT SYSDATE NOT NULL;

ALTER TABLE email
ADD lastUpdate DATE DEFAULT SYSDATE NOT NULL;

ALTER TABLE email
ADD creationBy VARCHAR2(25);

ALTER TABLE email
ADD updateBy VARCHAR2(25);
----------------------------
ALTER TABLE gender
ADD creationDate DATE DEFAULT SYSDATE NOT NULL;

ALTER TABLE gender
ADD lastUpdate DATE DEFAULT SYSDATE NOT NULL;

ALTER TABLE gender
ADD creationBy VARCHAR2(25);

ALTER TABLE gender
ADD updateBy VARCHAR2(25);
----------------------------
ALTER TABLE nationalityxp
ADD creationDate DATE DEFAULT SYSDATE NOT NULL;

ALTER TABLE nationalityxp
ADD lastUpdate DATE DEFAULT SYSDATE NOT NULL;

ALTER TABLE nationalityxp
ADD creationBy VARCHAR2(25);

ALTER TABLE nationalityxp
ADD updateBy VARCHAR2(25);
----------------------------
ALTER TABLE nationality
ADD creationDate DATE DEFAULT SYSDATE NOT NULL;

ALTER TABLE nationality
ADD lastUpdate DATE DEFAULT SYSDATE NOT NULL;

ALTER TABLE nationality
ADD creationBy VARCHAR2(25);

ALTER TABLE nationality
ADD updateBy VARCHAR2(25);
------------------------------------
ALTER TABLE person
ADD creationDate DATE DEFAULT SYSDATE NOT NULL;

ALTER TABLE person
ADD lastUpdate DATE DEFAULT SYSDATE NOT NULL;

ALTER TABLE person
ADD creationBy VARCHAR2(25);

ALTER TABLE person
ADD updateBy VARCHAR2(25);
-----------------------------------------

ALTER TABLE phone
ADD creationDate DATE DEFAULT SYSDATE NOT NULL;

ALTER TABLE phone
ADD lastUpdate DATE DEFAULT SYSDATE NOT NULL;

ALTER TABLE phone
ADD creationBy VARCHAR2(25);

ALTER TABLE phone
ADD updateBy VARCHAR2(25);
---------------------------------------------
ALTER TABLE province
ADD creationDate DATE DEFAULT SYSDATE NOT NULL;

ALTER TABLE province
ADD lastUpdate DATE DEFAULT SYSDATE NOT NULL;

ALTER TABLE province
ADD creationBy VARCHAR2(25);

ALTER TABLE province
ADD updateBy VARCHAR2(25);
---------------------------------------------
ALTER TABLE typeId
ADD creationDate DATE DEFAULT SYSDATE NOT NULL;

ALTER TABLE typeId
ADD lastUpdate DATE DEFAULT SYSDATE NOT NULL;

ALTER TABLE typeId
ADD creationBy VARCHAR2(25);

ALTER TABLE typeId
ADD updateBy VARCHAR2(25);
---------------------------------------------
ALTER TABLE typeOfUser
ADD creationDate DATE DEFAULT SYSDATE NOT NULL;

ALTER TABLE typeOfUser
ADD lastUpdate DATE DEFAULT SYSDATE NOT NULL;

ALTER TABLE typeOfUser
ADD creationBy VARCHAR2(25);

ALTER TABLE typeOfUser
ADD updateBy VARCHAR2(25);
---------------------------------------------
ALTER TABLE user1
ADD creationDate DATE DEFAULT SYSDATE NOT NULL;

ALTER TABLE  user1
ADD lastUpdate DATE DEFAULT SYSDATE NOT NULL;

ALTER TABLE  user1
ADD creationBy VARCHAR2(25);

ALTER TABLE  user1
ADD updateBy VARCHAR2(25);