CREATE USER admin
  IDENTIFIED BY proyecto1
  DEFAULT TABLESPACE proyectoDB
  TEMPORARY TABLESPACE temp
  QUOTA 5M ON system;
 GRANT connect TO admin;
-----------------------
GRANT create session to admin;
----------------------------
GRANT create table to admin;
-------------------------
grant create view to admin;
-----------------------
GRANT create procedure to admin;
-----------------------
GRANT unlimited tablespace to admin;
-----------------------
GRANT drop any table to admin;
-----------------------
GRANT comment any table to admin;
-----------------------
GRANT create sequence to admin;

CREATE USER user1
  IDENTIFIED BY proyecto2
  DEFAULT TABLESPACE proyectoDB
  TEMPORARY TABLESPACE temp
  QUOTA 5M ON system;

   GRANT connect TO user1;
-----------------------
GRANT create session to user1;
----------------------------
GRANT create table to user1;
-------------------------
grant create view to user1; 

GRANT create procedure to user1;
-----------------------
GRANT unlimited tablespace to user1;
-----------------------
GRANT drop any table to user1;
-----------------------
GRANT comment any table to user1;
-----------------------
GRANT create sequence to user1;
 