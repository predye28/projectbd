--userCorrecto
CREATE OR REPLACE FUNCTION isUser(pidenti NUMBER,pPasword VARCHAR2) RETURN BOOLEAN
IS
  cont NUMBER;
BEGIN
  SELECT COUNT(*) INTO cont
  FROM user1 u
  WHERE u.identification = pidenti AND u.password = pPasword;

  IF cont > 0 THEN
    RETURN TRUE;
  ELSE
    RETURN FALSE;
  END IF;
END isUser;

--retornar el idPerson para cuando entra al sistema con el usuario y la contraseņa 
CREATE OR REPLACE FUNCTION getIdPersonFromUser(
    pIdentification NUMBER,
    pPasword VARCHAR2
    
) RETURN NUMBER AS
    returnIdperson NUMBER;
BEGIN

    SELECT u.idPerson INTO returnIdperson
    FROM USER1 u
    WHERE u.identification = pIdentification AND u.password = pPasword;
  COMMIT;
  
  RETURN returnIdperson;
END;
--retornar el iTypeOfUser para cuando entra al sistema con el usuario y la contraseņa 
CREATE OR REPLACE FUNCTION getIdTypeOfUser(
    pIdentification NUMBER,
    pPasword VARCHAR2
    
) RETURN NUMBER AS
    returnIdTypeOfUser NUMBER;
BEGIN

    SELECT u.idTypeOfuser INTO returnIdTypeOfUSer
    FROM USER1 u
    WHERE u.identification = pIdentification AND u.password = pPasword;
  COMMIT;
  
  RETURN returnIdTypeOfUser;
END;