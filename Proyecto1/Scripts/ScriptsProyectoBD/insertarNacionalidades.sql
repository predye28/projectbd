INSERT INTO nationality(id, name)
VALUES (idNationality.NEXTVAL,'Costarricense');

INSERT INTO nationality(id, name)
VALUES (idNationality.NEXTVAL,'Salvadore�o');

INSERT INTO nationality (id, name)
VALUES (idNationality.NEXTVAL, 'Afgano');

INSERT INTO nationality (id, name)
VALUES (idNationality.NEXTVAL, 'Alban�s');

INSERT INTO nationality (id, name)
VALUES (idNationality.NEXTVAL, 'Alem�n');

INSERT INTO nationality (id, name)
VALUES (idNationality.NEXTVAL, 'Argentino');
-- Nacionalidades 1-10
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Andorrano');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Angole�o');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Argelino');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Armenio');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Australiano');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Austriaco');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Azer�');

-- Nacionalidades 11-20
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Bahame�o');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Banglades�');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Barbadense');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Belga');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Belice�o');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Benin�s');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Bielorruso');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Birmano');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Boliviano');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Bosnio');

-- Nacionalidades 21-30
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Brasile�o');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Brit�nico');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Bruneano');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'B�lgaro');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Burkin�s');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Burund�s');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Cabo Verdiano');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Camboyano');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Camerun�s');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Canadiense');

-- Nacionalidades 31-40
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Catar�');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Chadiano');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Chileno');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Chino');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Chipriota');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Colombiano');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Comorense');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Congole�o');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Croata');

-- Nacionalidades 41-50
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Cubano');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Dan�s');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Dominicano');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Ecuatoriano');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Egipcio');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Emirat�');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Eritreo');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Escoc�s');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Eslovaco');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Esloveno');

-- Nacionalidades 51-60
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Espa�ol');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Estonio');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Et�ope');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Filipino');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Finland�s');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Franc�s');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Gabon�s');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Gambiano');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Georgiano');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Ghan�s');

-- Nacionalidades 61-70
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Granadino');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Griego');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Guatemalteco');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Guineano');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Guineano-ecuatorial');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Guyan�s');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Haitiano');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Holand�s');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Hondure�o');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'H�ngaro');

-- Nacionalidades 71-80
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Indio');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Indonesio');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Ingl�s');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Iran�');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Iraqu�');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Irland�s');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Island�s');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Israel�');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Italiano');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Jamaiquino');

-- Nacionalidades 81-90
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Japon�s');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Jordano');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Kazajo');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Keniano');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Kirgu�s');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Kiribatiano');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Kuwait�');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Laosiano');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Lesotense');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Let�n');

-- Nacionalidades 91-100
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Liban�s');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Liberiano');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Libio');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Liechtensteiniano');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Lituano');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Luxemburgu�s');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Malgache');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Malasio');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Malau�');
INSERT INTO nationality (id, name) VALUES (idNationality.NEXTVAL, 'Maldivo');


