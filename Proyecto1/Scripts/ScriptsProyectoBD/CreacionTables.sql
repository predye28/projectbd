
CREATE TABLE parameter
(
    id NUMBER(6) CONSTRAINT paramater_id_nn NOT NULL,
    name VARCHAR(25) CONSTRAINT parameter_name_nn NOT NULL,
    value NUMBER(25) CONSTRAINT parameter_values_nn NOT NULL
);

 --Aqu� igual b�sicamente lo de nationality
CREATE TABLE typeOfPaymentM
(
    id NUMBER(6) CONSTRAINT typeOfPaymentM_id_nn NOT NULL,
    name VARCHAR2(25) CONSTRAINT typeOfPaymentM_name_nn NOT NULL
);


CREATE TABLE offer

(
    id NUMBER(6) CONSTRAINT offer_id_nn NOT NULL,
    name VARCHAR2(25) CONSTRAINT offer_name_nn NOT NULL,
    miniumDay NUMBER(6) CONSTRAINT offer_miniumDay_nn NOT NULL,
    description VARCHAR(60) CONSTRAINT offer_description_nn NOT NULL,
    startDate DATE DEFAULT SYSDATE CONSTRAINT offer_startDate_nn NOT NULL,
    finishDate DATE DEFAULT SYSDATE CONSTRAINT offer_finishDate_nn NOT NULL,
    billedAmount NUMBER(8),
    idRoom NUMBER(6) CONSTRAINT offer_idRoom_nn NOT NULL,
    idHotel NUMBER(6) CONSTRAINT offer_idHotel_nn NOT NULL
);

--esto era feedBack ahora es backFeed porque era reservado
--comentary era comment pero estaba reservado igual
CREATE TABLE backFeed
(
    id NUMBER(6) CONSTRAINT backFeed_id_nn NOT NULL,
    commentary VARCHAR(30) CONSTRAINT backFeed_comment_nn NOT NULL,
    idRating NUMBER(6) CONSTRAINT backFeed_idRating_nn NOT NULL
);

CREATE TABLE bookingStar
(
    id NUMBER(6) CONSTRAINT bookingStar_id_nn NOT NULL,
    name NUMBER(5) CONSTRAINT bookingStar_name_nn NOT NULL
);

CREATE TABLE rating 
(
    id NUMBER(6) CONSTRAINT rating_id_nn NOT NULL,
    name NUMBER(6) CONSTRAINT rating_name_nn NOT NULL
);

CREATE TABLE room
(
    id NUMBER(6) CONSTRAINT room_id_nn NOT NULL,
    name VARCHAR(25) CONSTRAINT room_name_nn NOT NULL,
    discountCode VARCHAR(25),
    discountPercentage NUMBER(8,2),
    capacity NUMBER(20) CONSTRAINT room_capacity_nn NOT NULL,
    recommendPrice NUMBER(20) CONSTRAINT room_recommendPrice_nn NOT NULL,
    idHotel NUMBER(6) CONSTRAINT room_idHotel_nn NOT NULL
);

--lo mismo de los bytes paso de "roomXAmenityRoom" a "roomXAR"
CREATE TABLE roomXAR
(
    idRoom NUMBER(6) CONSTRAINT roomXAR_idRoom_nn NOT NULL,
    idAmenityRoom NUMBER(6) CONSTRAINT roomXAR_idAmenityRoom_nn NOT NULL
);

CREATE TABLE amenityRoom
(
    id NUMBER(6) CONSTRAINT amenity_id_nn NOT NULL,
    name VARCHAR(25) CONSTRAINT amenityRoom_name_nn NOT NULL
);

CREATE TABLE amenityHotel
(
    id NUMBER(6) CONSTRAINT amenityHotel_id_nn NOT NULL,
    name VARCHAR(25) CONSTRAINT amenityHotel_name_nn NOT NULL,
    idHotel NUMBER(6) CONSTRAINT amenityHotel_idHotel_nn NOT NULL
);

CREATE TABLE booking 
(
    id NUMBER(6) CONSTRAINT booking_id_nn NOT NULL,
    checkIn DATE DEFAULT SYSDATE CONSTRAINT booking_checkIn_nn NOT NULL,
    checkOut DATE DEFAULT SYSDATE CONSTRAINT booking_checkOut_nn NOT NULL,
    priceAdministrator NUMBER(20) CONSTRAINT booking_priceAdministrator_nn NOT NULL,
    payday DATE DEFAULT SYSDATE CONSTRAINT booking_payday_nn NOT NULL,
    idPerson NUMBER(6) CONSTRAINT booking_idPerson_nn NOT NULL,
    idFeedBack NUMBER(6),
    idBookingStar NUMBER(6),
    idRoom NUMBER(6) CONSTRAINT booking_idRoom_nn NOT NULL
);


--igual lo de los bytes paso de "typeOfPaymentMethodXBooking" a typeOfPayMXB
CREATE TABLE typeOfPayMXB
(
    id NUMBER(6) CONSTRAINT typeOfPayMXB_id_nn NOT NULL,
    idBooking NUMBER(6) CONSTRAINT typeOfPayMXB_idBooking_nn NOT NULL,
    idtypeOfPayMXB NUMBER(6) CONSTRAINT typeOfPayMXB_idtypeOfPayMXB_nn NOT NULL
);

CREATE TABLE hotel
(
    id NUMBER(6) CONSTRAINT hotel_id_nn NOT NULL,
    name VARCHAR(25) CONSTRAINT hotel_name_nn NOT NULL,
    dateRegister DATE DEFAULT SYSDATE CONSTRAINT hotel_dateReg_nn NOT NULL,
    idDistrict NUMBER(6) CONSTRAINT hotel_idDistrict_nn NOT NULL
);

CREATE TABLE personXHotelFAV
(
    idPerson NUMBER(6) CONSTRAINT personXHotelFAV_idPerson_nn NOT NULL,
    idHotel NUMBER(6) CONSTRAINT personXHotelFAV_idHotel_nn NOT NULL
);
--igual los bytes paso de "cancellacionPolicy" a "cancellacionP" 
CREATE TABLE cancellacionP
(
    id NUMBER(6) CONSTRAINT cancellacionP_id_nn NOT NULL,
    name VARCHAR(25) CONSTRAINT cancellacionP_name_nn NOT NULL,
    percentage NUMBER(25) CONSTRAINT cancellacionP_percentage_nn NOT NULL,
    idHotel NUMBER(6) CONSTRAINT cancellacionP_idHotel_nn NOT NULL
);
--igual los bytes
CREATE TABLE cancelPXB
(
    id NUMBER(6) CONSTRAINT cancelPXB_id_nn NOT NULL,
    idCancelP NUMBER(6) CONSTRAINT cancelPXB_idCancelP_nn NOT NULL,
    idBooking NUMBER(6) CONSTRAINT cancelPXB_idBooking_nn NOT NULL
);

CREATE TABLE photo
(
    id NUMBER(6) CONSTRAINT photo_id_nn NOT NULL,
    name VARCHAR(25) CONSTRAINT photo_name_nn NOT NULL,
    idHotel NUMBER(6) CONSTRAINT photo_idHotel_nn NOT NULL
);