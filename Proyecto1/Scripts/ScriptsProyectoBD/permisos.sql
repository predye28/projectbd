GRANT CREATE ANY INDEX TO admin;
GRANT UNLIMITED TABLESPACE TO admin;
GRANT connect to admin;
GRANT create session to admin;
GRANT create table to admin;
GRANT create view to admin;
GRANT CREATE SEQUENCE TO admin;
GRANT CREATE PROCEDURE TO admin;
GRANT CREATE TRIGGER TO admin;


GRANT CREATE ANY INDEX TO user1;
GRANT UNLIMITED TABLESPACE TO user1;
GRANT connect to user1;
GRANT create session to user1;
GRANT create table to user1;
GRANT create view to user1;
GRANT CREATE SEQUENCE TO user1;
GRANT CREATE PROCEDURE TO user1;
GRANT CREATE TRIGGER TO user1;

--admin
GRANT SELECT ON user1.person TO admin;

GRANT SELECT ON user1.nationalityxp TO admin;

GRANT SELECT ON user1.nationality TO admin;


--PERMISOS PARA LAS REFERENCIAS
--user1
GRANT REFERENCES ON admin.hotel TO user1;
--admin1
GRANT REFERENCES ON user1.person TO admin;
GRANT REFERENCES ON user1.district TO admin;
GRANT REFERENCES ON user1.user1 TO admin;