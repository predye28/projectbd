--funcion que retorna el id del hotel desde user1
--funcion guardada en user1
CREATE OR REPLACE FUNCTION getIdHotelFromUser(
    pIdentification NUMBER,
    pPasword VARCHAR2
    
) RETURN NUMBER AS
    returnIdhotel NUMBER;
BEGIN

    SELECT u.idHotel INTO returnIdhotel
    FROM USER1 u
    WHERE u.identification = pIdentification AND u.password = pPasword;
  COMMIT;
  
  RETURN returnIdhotel;
END;

--procedimiento que inserta una oferta
CREATE OR REPLACE PROCEDURE insertOffer(
    pName VARCHAR2,
    PMiniumDay NUMBER,
    pDescription VARCHAR2,
    pstarDate DATE,
    pFinishDate DATE,
    pIdRoom NUMBER,
    pidHotel NUMBER
) AS

BEGIN
    INSERT INTO offer(id, name, miniumDay, description, startDate, finishDate,billedAMount, idRoom, idHotel)
    VALUES (idOffer.NEXTVAL,pName,PMiniumDay,pDescription,pstarDate,pFinishDate,0,pIdRoom,pidHotel);

  COMMIT;
END;

--funcion que retorna todas las ofertas del hotel
CREATE OR REPLACE FUNCTION getOffer(pIdHotel NUMBER) RETURN SYS_REFCURSOR
IS 
  offerCursor SYS_REFCURSOR;
BEGIN 

  OPEN offerCursor FOR 
    SELECT o.id, o.name
    FROM offer o
    WHERE o.idHotel = pidHotel;

  RETURN offerCursor; 

END getOffer; 

--eliminar oferta del hotel
CREATE OR REPLACE PROCEDURE deleteOffer(
    pIdOffer NUMBER,
    pIdhotel NUMBER
) AS

BEGIN
    DELETE FROM offer o
    WHERE o.idRoom = pIdOffer AND o.idHotel = pIdhotel;

  COMMIT;
END;

--FUNCION QUE retorna las politicas del hotel
CREATE OR REPLACE FUNCTION getCancellacionP(pIdHotel NUMBER) RETURN SYS_REFCURSOR
IS 
  CancellacionCursor SYS_REFCURSOR;
BEGIN 

  OPEN CancellacionCursor FOR 
    SELECT c.id, c.name, c.percentage
    FROM cancellacionP c
    WHERE c.idHotel = pidHotel;

  RETURN CancellacionCursor; 

END getCancellacionP; 
--procedimiento que crea nuevas politicas del hotel
CREATE OR REPLACE PROCEDURE insertCancellacionP(
    pName VARCHAR2,
    pPercentage NUMBER,
    pIdHotel NUMBER
) AS

BEGIN
    INSERT INTO cancellacionP(id, name, percentage, idhotel)
    VALUES (idCancellacionP.NEXTVAL,pName,pPercentage,pIdHotel);

  COMMIT;
END;