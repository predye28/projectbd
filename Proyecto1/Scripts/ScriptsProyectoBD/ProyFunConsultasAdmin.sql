--A lISTADO DE PERSONAS
create or replace FUNCTION getPersonasInHotel(pIdHotel IN NUMBER) RETURN SYS_REFCURSOR
IS 
  personasHotelCursor SYS_REFCURSOR;

BEGIN 

  OPEN personasHotelCursor FOR 
    SELECT p.firstName, p.firstLastName, p.numIdentification, n.name, b.checkin, b.checkout, b.priceadministrator, r.name, h.name
    FROM booking b
    INNER JOIN room r ON b.idRoom = r.id
    INNER JOIN hotel h ON r.idHotel = h.id
    INNER JOIN user1.person p ON b.idPerson = p.id
    INNER JOIN user1.nationalityXP nxp ON p.id = nxp.idPerson
    INNER JOIN user1.nationality n ON nxp.idNationality = n.id
    WHERE h.id = 2;

  RETURN personasHotelCursor; 

END getPersonasInHotel;





--B) LISTADO DE OFERTAS

--FILTRO POR NOMBRE
create or replace FUNCTION getOfertasHotelForName(pIdHotel IN NUMBER,pName VARCHAR2) RETURN SYS_REFCURSOR
IS 
  offertasHotel SYS_REFCURSOR;

BEGIN 

  OPEN offertasHotel FOR 
    SELECT o.name, o.description
    
    FROM offer o
    WHERE o.idHotel = pIdHotel and o.name = pName;

  RETURN offertasHotel; 

END getOfertasHotelForName;

--C) PROMEDIO DE CALIFICACION

CREATE OR REPLACE FUNCTION calcularPromedioCalifiacion(pIdHotel IN NUMBER)
  RETURN NUMBER
IS
  promedio NUMBER;
BEGIN
  SELECT SUM(rat.name)/COUNT(rat.name) INTO promedio
  FROM booking b
  INNER JOIN room r ON b.idRoom = r.id
  INNER JOIN hotel h ON h.id = r.idHotel
  INNER JOIN backfeed bf ON bf.id = b.idFeedBack
  INNER JOIN rating rat ON rat.id = bf.idRating
  WHERE h.id = pIdHotel;
  
  RETURN promedio;
END calcularPromedioCalifiacion;
--todos los comentarios de ese hotel
create or replace FUNCTION getCommentariosHotel(pIdHotel IN NUMBER) RETURN SYS_REFCURSOR
IS 
  comentariosHotel SYS_REFCURSOR;

BEGIN 

  OPEN comentariosHotel FOR 
    SELECT  rat.name ,bf.commentary
      FROM booking b
      INNER JOIN room r ON b.idRoom = r.id
      INNER JOIN hotel h ON h.id = r.idHotel
      INNER JOIN backfeed bf
      ON bf.id = b.idFeedBack
      INNER JOIN rating rat ON rat.id = bf.idRating
      WHERE h.id = pIdHotel;

  RETURN comentariosHotel; 

END getCommentariosHotel;

--D) reporte de mayores dias de venta
create or replace FUNCTION getMayoresVentas(pIdHotel IN NUMBER) RETURN SYS_REFCURSOR
IS 
  mayorVentasHotel SYS_REFCURSOR;

BEGIN 

  OPEN mayorVentasHotel FOR 
    
    SELECT fecha, COUNT(*) AS total_reservas
    FROM (
        SELECT TRUNC(b.payday) AS fecha
        FROM booking b
        INNER JOIN room r ON b.idRoom = r.id
        INNER JOIN hotel h ON h.id = r.idHotel
        WHERE h.id = pIdHotel
    )
    GROUP BY fecha
    ORDER BY total_reservas DESC;

  RETURN mayorVentasHotel; 

END getMayoresVentas;

--E)REPORTE DE MENORES DIAS DE VENTA
create or replace FUNCTION getMenoresVentas(pIdHotel IN NUMBER) RETURN SYS_REFCURSOR
IS 
  menoresVentasHotel SYS_REFCURSOR;

BEGIN 

  OPEN menoresVentasHotel FOR 
    
    SELECT fecha, COUNT(*) AS total_reservas
    FROM (
        SELECT TRUNC(b.payday) AS fecha
        FROM booking b
        INNER JOIN room r ON b.idRoom = r.id
        INNER JOIN hotel h ON h.id = r.idHotel
        WHERE h.id = pIdHotel
    )
    GROUP BY fecha
    ORDER BY total_reservas asc;

  RETURN menoresVentasHotel; 

END getMenoresVentas;





