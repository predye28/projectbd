--PERSON
INSERT INTO person(id, firstName, secondName, firstLastName, secondLastName, birthdate, idDistrict, idGender, idTypeid,numIdentification)
VALUES (idPerson.NEXTVAL, 'Omar', 'Alexis', 'Madrigal', 'Rodriguez',TO_DATE('14/07/2002', 'DD/MM/YY'),7, 2, 1, '7-0293-0776');

INSERT INTO phone(id, phoneNumber, idPerson)
VALUES (idPhone.NEXTVAL, 62748637, 2);

INSERT INTO email(id, name, idPerson)
VALUES (idEmail.NEXTVAL, 'omarmr14.02@gmail.com', 2);

INSERT INTO nationalityXP(idPerson, idNationality)
VALUES (2, 3);

INSERT INTO user1(identification, password, idPerson, idTypeOfUser)
VALUES (111111, 'proyecto1',2,1);

--otra persona
INSERT INTO person(id, firstName, secondName, firstLastName, secondLastName, birthdate, idDistrict, idGender, idTypeid,numIdentification)
VALUES (idPerson.NEXTVAL, 'Pablo', 'Alejandro', 'Castro', 'Uma�a',TO_DATE('18/09/2003', 'DD/MM/YY'),9, 2, 1, '1-0653-0746');

INSERT INTO phone(id, phoneNumber, idPerson)
VALUES (idPhone.NEXTVAL, 87541232, 3);

INSERT INTO email(id, name, idPerson)
VALUES (idEmail.NEXTVAL, 'pabloaa13@gmail.com', 3);

INSERT INTO nationalityXP(idPerson, idNationality)
VALUES (3, 2);

INSERT INTO user1(identification, password, idPerson, idTypeOfUser)
VALUES (123412, 'password1',3,1);

drop function gethotelsExpecifics;