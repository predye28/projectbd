--funcion q devuelve true si el user indicado es master // USER1
CREATE OR REPLACE FUNCTION isMaster(pIdentification NUMBER, pPassword VARCHAR2, pTypeOfUser NUMBER) RETURN BOOLEAN
IS
  cont NUMBER;
BEGIN
  SELECT COUNT(*) INTO cont
  FROM user1.user1 u
  WHERE u.identification= pIdentification AND u.password = pPassword AND u.idtypeOfUser = pTypeOfUser;

  IF cont > 0 THEN
    RETURN TRUE;
  ELSE
    RETURN FALSE;
  END IF;
  
END isMaster;

--funcion que devuelve true si el usuario existe por medio de la cedula //USER1
CREATE OR REPLACE FUNCTION userExist(pNumIdentification VARCHAR2) RETURN BOOLEAN
IS
  cont NUMBER;
BEGIN
  SELECT COUNT(*) INTO cont
  FROM person p
  WHERE p.numidentification= pNumIdentification;

  IF cont > 0 THEN
    RETURN TRUE;
  ELSE
    RETURN FALSE;
  END IF;
  
END userExist;
--funcion que devuelve el idPerson con la cedula

CREATE OR REPLACE FUNCTION getIdPersonFromNumIde(
    pNumIdentificacion VARCHAR2
    
) RETURN NUMBER AS
    returnIdPerson NUMBER;
BEGIN

    SELECT p.id INTO returnIdPerson
    FROM person p
    WHERE p.Numidentification = pNumIdentificacion;
  COMMIT;
  
  RETURN returnIdPerson;
END getIdPersonFromNumIde;

--procedimiento que crea el admin de ese hotel 
CREATE OR REPLACE PROCEDURE assignAdmin(
    pNumIdentificacion VARCHAR2,
    pIdhotel NUMBER
) AS
    vIdPerson NUMBER;
BEGIN
    vIdPerson := getIdPersonFromNumIde(pNumIdentificacion);
    UPDATE user1 u
    SET u.IdtypeOfUser = 2, u.IdHotel = pIdhotel
    WHERE u.idPerson = vIdPerson;

  COMMIT;
END;

--funcion que muestra todos los hoteles //ADMIN
CREATE OR REPLACE FUNCTION getHotelsExpecifics RETURN SYS_REFCURSOR
IS 
  hotCursor SYS_REFCURSOR;
BEGIN 

  OPEN hotCursor FOR 
    SELECT h.id, h.Name, h.idDIstrict, h.dateRegister
    FROM hotel h;

  RETURN hotCursor; 

END getHotelsExpecifics;

--funcion que muestre el total d reservas, total facturado de el Idhotel selecionado //ADMIN
CREATE OR REPLACE FUNCTION getAllInfoHotel(pIdHotel NUMBER) RETURN SYS_REFCURSOR
IS 
  allInfoCurso SYS_REFCURSOR;
BEGIN 

  OPEN allInfoCurso FOR 
    SELECT COUNT(b.idROOm), SUM(b.priceADministrator)
    FROM booking b
    INNER JOIN room r
    ON r.id = b.idRoom
    INNER JOIN hotel h
    ON h.id = r.idHotel
    WHERE h.id = pIdHotel;

  RETURN allInfoCurso; 

END getAllInfoHotel;
--funcion que muestre el total d habitaciones de el Idhotel selecionado //ADMIN
CREATE OR REPLACE FUNCTION getAmountRoomfoHotel(pIdHotel NUMBER) RETURN SYS_REFCURSOR
IS 
  amountRoomCurso SYS_REFCURSOR;
BEGIN 

  OPEN amountRoomCurso FOR 
    SELECT COUNT(*)
    FROM room r
    INNER JOIN hotel h
    ON h.id = r.idHotel
    WHERE h.id = pIdHotel;

  RETURN amountRoomCurso; 

END getAmountRoomfoHotel;

--funcion que reciba un nombre y muestre el hotel
CREATE OR REPLACE FUNCTION getHotelsFilterNAme(pName VARCHAR2) RETURN SYS_REFCURSOR
IS 
  hotNmaCursor SYS_REFCURSOR;
BEGIN 

  OPEN hotNmaCursor FOR 
    SELECT h.id, h.Name, h.idDIstrict, h.dateRegister
    FROM hotel h
    WHERE h.name = pName;

  RETURN hotNmaCursor; 

END getHotelsFilterNAme;
--funcion que reciba una fecha de registro y retorne los hoteles registraados con esa fecha
CREATE OR REPLACE FUNCTION getHotelsFilDateRe(pREgister DATE DEFAULT SYSDATE) RETURN SYS_REFCURSOR
IS 
  hotDateCursor SYS_REFCURSOR;
BEGIN 

  OPEN hotDateCursor FOR 
    SELECT h.id, h.Name, h.idDIstrict, h.dateRegister
    FROM hotel h
    WHERE h.dateRegister = pREgister;

  RETURN hotDateCursor; 

END getHotelsFilDateRe;

--falta profe


