CREATE OR REPLACE FUNCTION getHotelFav(pIdPerson IN NUMBER) RETURN SYS_REFCURSOR
IS 
  hotelfav SYS_REFCURSOR;
   
BEGIN 

  OPEN hotelfav FOR 
    SELECT pxh.idHotel
    FROM personXhotelFav pxh
    WHERE pxh.idPerson = pIdPerson;
    
  RETURN hotelfav; 

END getHotelFav;

