--PROCEDIMIENTO GUARDAR PERSONAS
SELECT insertPerson('Maria','Ana','Lopez','Cruz','jdasdphoto.png',TO_DATE('22/09/2001', 'DD/MM/YY'),12,1,1,'7-0231-0234','72318321','casdad1@gmail.com') FROM dual;
CREATE OR REPLACE FUNCTION insertPerson(
    pFirstName VARCHAR2,
    pSecondName VARCHAR2,
    pFirstLastName VARCHAR2,
    pSecondLastName VARCHAR2,
    pPhoto VARCHAR2,
    pbirthdate DATE,
    pIdDistrict NUMBER,
    pIdGender NUMBER,
    pidTypeID NUMBER,
    pnumIdentification VARCHAR2,
    --phone
    pPhoneNumber VARCHAR2,
    --email
    pEmail VARCHAR2
) RETURN NUMBER AS
    reIdPerson NUMBER;
BEGIN
    --agregar a person
    INSERT INTO person(id, firstName, secondName, firstLastName, secondLastName,photo, birthdate, idDistrict, idGender, idTypeid,numIdentification)
    VALUES (idPerson.NEXTVAL, pFirstName, pSecondName, pFirstLastName, pSecondLastName, pPhoto,pbirthdate,pIdDistrict, pIdGender, pidTypeID, pnumIdentification);
    
    SELECT idPerson.CURRVAL INTO reIdPerson FROM dual;
    
    --agregar a phone
    INSERT INTO phone(id, phoneNumber, idPerson)
    VALUES (idPhone.NEXTVAL, pPhoneNumber,reIdPerson);
    --agregar email
    INSERT INTO email(id, name, idPerson)
    VALUES (idEmail.NEXTVAL, pEmail, reIdPerson);
  COMMIT;
  
  RETURN reIdPerson;
END;

--PROCEDIMIENTO GUARDA NATIONALITYXP
CREATE OR REPLACE PROCEDURE insertNationalityXP(pIdPerson NUMBER,
  pIdNationality NUMBER
) AS

BEGIN
  INSERT INTO nationalityXP(idPerson, idNationality)
  VALUES (pIdPerson, pIdNationality);
  COMMIT;
END;

--PROCEDIMIENTO GUARDA USER1
CREATE OR REPLACE PROCEDURE insertUser1(
    pIdentification NUMBER,
    pPassword VARCHAR2,
    pIdPerson NUMBER,
    pIdTypeOfUser NUMBER,
    pIdHotel NUMBER
) AS

BEGIN
    INSERT INTO user1(identification, password, idPerson, idTypeOfUser,idHotel)
    VALUES (pIdentification, pPassword,pIdPerson,pIdTypeOfUser,pIdHotel);
  COMMIT;
END;

--probar procedimientos y fucniones

DECLARE
    result NUMBER;
BEGIN
    --result := insertPerson('Ricardo','Castro','Rojas','Picado',NUll,TO_DATE('20/12/2002', 'DD/MM/YY'),2,2,1,'2-0311-0555','89321231','ricadrr1@gmail.com');
    updateRoomCodeDiscount(1);
    --insertUser1(312312,'preaq3d',result,1,null);
    COMMIT;
END;




















