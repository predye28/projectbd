--guardar hotel
CREATE OR REPLACE FUNCTION insertHotel(
    pName VARCHAR2,
    pIdDistrict NUMBER
) RETURN NUMBER AS
    reIdHotel NUMBER;
BEGIN
    --agregar hotel
    INSERT INTO hotel(id, name, idDistrict)
    VALUES (idHotel.NEXTVAL, pName, pIdDistrict);
    
    SELECT idHotel.CURRVAL INTO reIdHotel FROM dual;

  COMMIT;
  
  RETURN reIdHotel;
END;

--agregar photo de un hotel
CREATE OR REPLACE PROCEDURE insertPhoto(
    pPhoto VARCHAR2,
    PidHotel NUMBER
) AS

BEGIN
    INSERT INTO photo(id, name, idHotel)
    VALUES (idPhoto.NEXTVAL, pPhoto,PidHotel);
  COMMIT;
END;


--agregar amenidades
CREATE OR REPLACE PROCEDURE insertAmenityHotel(
    pName VARCHAR2,
    pIdHotel NUMBER
) AS

BEGIN
    INSERT INTO amenityHotel(id, name, idHotel)
    VALUES (idAmenityHotel.NEXTVAL, pName,pIdHotel);
  COMMIT;
END;

--REGISTRAR ROOM
CREATE OR REPLACE FUNCTION insertRoom(
    pName VARCHAR2,
    pCapacity NUMBER,
    pRecomPrice NUMBER,
    pIdHotel NUMBER
) RETURN NUMBER AS
    reRoom NUMBER;
BEGIN
    INSERT INTO room(id, name, capacity, recommendPrice, idHotel)
    VALUES (idRoom.NEXTVAL, pName, pCapacity, pRecomPrice, pIdHotel);
    
    SELECT idRoom.CURRVAL INTO reRoom FROM dual;

  COMMIT;
  
  RETURN reRoom;
END;

--agregar amenidad a amenityRoom
CREATE OR REPLACE PROCEDURE insertAmenityRoom(
    pName VARCHAR2
) AS

BEGIN
    INSERT INTO amenityRoom(id, name)
    VALUES (idAmenityRoom.NEXTVAL, pName);
  COMMIT;
END;

--agregar amenidad a roomXar
CREATE OR REPLACE PROCEDURE insertRoomXAR(
    pIdRoom NUMBER,
    pIdAmenityRoom NUMBER
) AS

BEGIN
    INSERT INTO roomXAR(idRoom, idAmenityRoom)
    VALUES (pIdRoom, pIdAmenityRoom);
  COMMIT;
END;



