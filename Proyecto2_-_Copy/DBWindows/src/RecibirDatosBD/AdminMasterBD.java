/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package RecibirDatosBD;

/**
 *
 * @author omarm
 */
public class AdminMasterBD {
    int identificacion;
    String password;

    public AdminMasterBD(int identificacion, String password) {
        this.identificacion = identificacion;
        this.password = password;
    }

    public AdminMasterBD() {
    }

    public int getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(int identificacion) {
        this.identificacion = identificacion;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
}
