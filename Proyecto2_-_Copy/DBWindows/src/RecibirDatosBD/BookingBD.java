/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package RecibirDatosBD;

import java.util.Date;

/**
 *
 * @author omarm
 */
public class BookingBD {
    String CheckIn;
    String CheckOut;
    int price;
    int idPerson;
    int idFeedback;
    int idRoom;
    int idBooking;
    
    public BookingBD(String CheckIn, String CheckOut, int price, int idPerson, int idFeedback, int idRoom) {
        this.CheckIn = CheckIn;
        this.CheckOut = CheckOut;
        this.price = price;
        this.idPerson = idPerson;
        this.idFeedback = idFeedback;
        this.idRoom = idRoom;
    }

    public int getIdBooking() {
        return idBooking;
    }

    public void setIdBooking(int idBooking) {
        this.idBooking = idBooking;
    }

    public BookingBD() {
    }

    public String getCheckIn() {
        return CheckIn;
    }

    public void setCheckIn(String CheckIn) {
        this.CheckIn = CheckIn;
    }

    public String getCheckOut() {
        return CheckOut;
    }

    public void setCheckOut(String CheckOut) {
        this.CheckOut = CheckOut;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getIdPerson() {
        return idPerson;
    }

    public void setIdPerson(int idPerson) {
        this.idPerson = idPerson;
    }

    public int getIdFeedback() {
        return idFeedback;
    }

    public void setIdFeedback(int idFeedback) {
        this.idFeedback = idFeedback;
    }

    public int getIdRoom() {
        return idRoom;
    }

    public void setIdRoom(int idRoom) {
        this.idRoom = idRoom;
    }
    
    
}
