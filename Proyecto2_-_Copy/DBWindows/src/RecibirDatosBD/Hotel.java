/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package RecibirDatosBD;

import java.util.Date;

/**
 *
 * @author omarm
 */
public class Hotel {
    int idHotel;
    String name;
    int idDistrict;
    Date dateRegister;
    
    public Hotel(int idHotel, String name) {
        this.idHotel = idHotel;
        this.name = name;
    }

    public Hotel(int idHotel, String name, int idDistrict, Date dateRegister) {
        this.idHotel = idHotel;
        this.name = name;
        this.idDistrict = idDistrict;
        this.dateRegister = dateRegister;
    }
    
    public Hotel() {
    }

    public int getIdDistrict() {
        return idDistrict;
    }

    public void setIdDistrict(int idDistrict) {
        this.idDistrict = idDistrict;
    }

    public Date getDateRegister() {
        return dateRegister;
    }

    public void setDateRegister(Date dateRegister) {
        this.dateRegister = dateRegister;
    }
    
    
    public int getIdHotel() {
        return idHotel;
    }

    public void setIdHotel(int idHotel) {
        this.idHotel = idHotel;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
}
