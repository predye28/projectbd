/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Connect;


import Metodos.personConsulta;
import RecibirDatosBD.AmenidadesRoom;
import RecibirDatosBD.BookingBD;
import RecibirDatosBD.CommentaryBD;
import RecibirDatosBD.Hotel;
import RecibirDatosBD.OfferBD;
import RecibirDatosBD.PoliticaCancelacionBD;
import RecibirDatosBD.Room;
import RecibirDatosBD.StarHotel;
import RecibirDatosBD.TypeOfPayment;
import RecibirDatosBD.Venta;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleTypes;

/**
 *
 * @author omarm
 */
public class ConnectDBAdmin {
    private String host = "localhost";
    private String port = "3307";
    private String dbName = "admin";
    private String userName = "root";
    private String password = "dkshertzy18";

    public ConnectDBAdmin() {
    }
    public  ArrayList<Integer> getIdHotelFav(int idPerson) throws SQLException {
         String url= "jdbc:mysql://localhost:3307/admin";
        Connection conexion = DriverManager.getConnection(url, "root", "dkshertzy18");
        CallableStatement cstmt = conexion.prepareCall("{? = call getHotelFav(?)}");
        cstmt.setInt(2, idPerson);
        cstmt.registerOutParameter(1, OracleTypes.VARCHAR);
 
        cstmt.execute();
         String resultado = cstmt.getString(1);           
            // Obtener cada elemento concatenado por separado
            if(resultado  != null){
                     String[] elementos = resultado.split(";");
     
        ArrayList<Integer> arrayFinal = new ArrayList<Integer>();
        String[] elementoActu;
        for (String elemento : elementos) {
            arrayFinal.add(Integer.parseInt(elemento));
        }
        
         return arrayFinal;
                
            }else{
                return null; 
            }
   
    }
    public  Hotel getHotelFull(int idhotel) throws SQLException{
         String url= "jdbc:mysql://localhost:3307/admin";
        Connection conexion = DriverManager.getConnection(url, "root", "dkshertzy18");
        CallableStatement cstmt = conexion.prepareCall("{? = call getHotelFull(?)}");
        cstmt.setInt(2, idhotel);
        cstmt.registerOutParameter(1, OracleTypes.VARCHAR);
 
        cstmt.execute();
         String resultado = cstmt.getString(1);           
            // Obtener cada elemento concatenado por separado
        String[] elementos = resultado.split("\n");
     
        
        Hotel hotelRecibido = new Hotel();
        String[] elementoActu;
        for (String elemento : elementos) {
            elementoActu = elemento.split(", ");
            hotelRecibido.setIdHotel(Integer.parseInt(elementoActu[0]));
            hotelRecibido.setName(elementoActu[1]);
        }
 
        return hotelRecibido;
    }
    public Hotel getHotelFullForId(int idhotel) throws SQLException{
        String url= "jdbc:mysql://localhost:3307/admin";
        Connection conexion = DriverManager.getConnection(url, "root", "dkshertzy18");
        CallableStatement cstmt = conexion.prepareCall("{? = call getHotelFullForId(?)}");
        cstmt.setInt(2, idhotel);
        cstmt.registerOutParameter(1, OracleTypes.VARCHAR);
 
        cstmt.execute();
         String resultado = cstmt.getString(1);           
            // Obtener cada elemento concatenado por separado
        String[] elementos = resultado.split("\n");
     
        
        Hotel hotelRecibido = new Hotel();
        String[] elementoActu;
        for (String elemento : elementos) {
            elementoActu = elemento.split(":");
            hotelRecibido.setIdHotel(Integer.parseInt(elementoActu[0]));
            hotelRecibido.setName(elementoActu[1]);
            hotelRecibido.setIdDistrict(Integer.parseInt(elementoActu[2]));
        }
 
        return hotelRecibido;
    }
    public  ArrayList<personConsulta> getListaDePersonas(int idhotel) throws SQLException{
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        ArrayList<personConsulta> resultPersona = new ArrayList<personConsulta>();
         String url= "jdbc:mysql://localhost:3307/admin";
        Connection conexion = DriverManager.getConnection(url, "root", "dkshertzy18");
        CallableStatement cstmt = conexion.prepareCall("{? = call getPersonasInHotel(?)}");
        cstmt.registerOutParameter(1, OracleTypes.VARCHAR);
         cstmt.setInt(2, idhotel);
 
        cstmt.execute();
        String resultado = cstmt.getString(1);           
        // Obtener cada elemento concatenado por separado
        String[] elementos = resultado.split("\n");
     
         String[] elementoActu;
        for (String elemento : elementos) {
            
            elementoActu = elemento.split(", ");
            
            personConsulta persoConsu = new  personConsulta();
            persoConsu.setName(elementoActu[0]);
            persoConsu.setApellido(elementoActu[1]);
            persoConsu.setCedIdentificacion(elementoActu[2]);
            persoConsu.setNacionalidad(elementoActu[3]);
            
            System.out.println(elementoActu[4]);
            
            //String fechaStringCheckin = dateFormat.format(elementoActu[4]);
            persoConsu.setCheckin(elementoActu[4]);
            //String fechaStringCheckOut = dateFormat.format(elementoActu[5]);
            persoConsu.setCheckout(elementoActu[5]);
            String[] partes = elementoActu[6].split("\\.");
            String parteEntera = partes[0];
            persoConsu.setPrice(Integer.parseInt(parteEntera));
            persoConsu.setNameHabitacion(elementoActu[7]);
            resultPersona.add(persoConsu);
        }
  

        return resultPersona;
    }
    public  ArrayList<Room> getRoomFull(int idHotel) throws SQLException{
         String url= "jdbc:mysql://localhost:3307/admin";
        Connection conexion = DriverManager.getConnection(url, "root", "dkshertzy18");
        CallableStatement cstmt = conexion.prepareCall("{? = call getRoom(?)}");
        cstmt.setInt(2, idHotel);
        cstmt.registerOutParameter(1, OracleTypes.VARCHAR);
 
        cstmt.execute();
         String resultado = cstmt.getString(1);           
            // Obtener cada elemento concatenado por separado
        String[] elementos = resultado.split(";");
        
     

         ArrayList<Room> arrayFin = new ArrayList();
         String[] elementoActu;
        for (String elemento : elementos) {
            elementoActu = elemento.split(":");
             Room ROOM = new  Room();
             ROOM.setIdRoom(Integer.parseInt(elementoActu[0]));
             ROOM.setName(elementoActu[1]);
             String[] partes = elementoActu[2].split("\\.");
            String parteEntera = partes[0];
             ROOM.setPrice(Integer.parseInt(parteEntera));
             ROOM.setCapacity(Integer.parseInt(elementoActu[3]));
            arrayFin.add(ROOM);
        }
  

        return arrayFin;
    }
    public ArrayList<RecibirDatosBD.Hotel> getAllHotel() throws SQLException, ParseException{
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String url= "jdbc:mysql://localhost:3307/admin";
        
        
        
        
        Connection conexion = DriverManager.getConnection(url, "root", "dkshertzy18");
        CallableStatement cstmt = conexion.prepareCall("{? = call getHotelsExpecifics()}");
        cstmt.registerOutParameter(1, OracleTypes.VARCHAR);
 
        cstmt.execute();
         String resultado = cstmt.getString(1);           
            // Obtener cada elemento concatenado por separado
        String[] elementos = resultado.split("\n");
     

         ArrayList<RecibirDatosBD.Hotel> arrayFin = new ArrayList();
         String[] elementoActu;
        for (String elemento : elementos) {
            elementoActu = elemento.split(", ");
             RecibirDatosBD.Hotel Objeto = new  RecibirDatosBD.Hotel();
             Objeto.setIdHotel(Integer.parseInt(elementoActu[0]));
             Objeto.setName(elementoActu[1]);
             Objeto.setIdDistrict(Integer.parseInt(elementoActu[2]));
         
           //  String fechaStringCheckin = dateFormat.format(elementoActu[3]);
             Date fecha = new SimpleDateFormat("yyyy-MM-dd").parse(elementoActu[3]);

    // Formatear la fecha en el nuevo formato
    String fechaStringFormateada = dateFormat.format(fecha);

    // Convertir la fecha formateada en un objeto de tipo Date
    Date fechaFormateada = dateFormat.parse(fechaStringFormateada);
             Objeto.setDateRegister(fechaFormateada);
            arrayFin.add(Objeto);
        }
  

        return arrayFin;
    }
    public ArrayList<TypeOfPayment> getTypeOfPayment() throws SQLException{
        String url= "jdbc:mysql://localhost:3307/admin";
        Connection conexion = DriverManager.getConnection(url, "root", "dkshertzy18");
        CallableStatement cstmt = conexion.prepareCall("{? = call getPayment()}");
        cstmt.registerOutParameter(1, OracleTypes.VARCHAR);
 
        cstmt.execute();
         String resultado = cstmt.getString(1);           
            // Obtener cada elemento concatenado por separado
        String[] elementos = resultado.split(";");
     

         ArrayList<TypeOfPayment> arrayFin = new ArrayList();
         String[] elementoActu;
        for (String elemento : elementos) {
            elementoActu = elemento.split(":");
             TypeOfPayment Objeto = new  TypeOfPayment(Integer.parseInt(elementoActu[0]),elementoActu[1]);
            arrayFin.add(Objeto);
        }
  
        return arrayFin;
    }
    public ArrayList<Venta> getMayorVenta(int idHotel) throws SQLException{
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
         String url= "jdbc:mysql://localhost:3307/admin";
        Connection conexion = DriverManager.getConnection(url, "root", "dkshertzy18");
        CallableStatement cstmt = conexion.prepareCall("{? = call getMayoresVentas(?)}");
        cstmt.setInt(2, idHotel);
        cstmt.registerOutParameter(1, OracleTypes.VARCHAR);
 
        cstmt.execute();
         String resultado = cstmt.getString(1);           
            // Obtener cada elemento concatenado por separado
        String[] elementos = resultado.split("\n");
     
        
         ArrayList<Venta> arrayFin = new ArrayList();
         String[] elementoActu;
        for (String elemento : elementos) {
            elementoActu = elemento.split(", ");
             Venta Objeto = new  Venta();
            // String fechaStringCheckin = dateFormat.format(elementoActu[0]);
             Objeto.setFecha(elementoActu[0]);
             Objeto.setCantidad(Integer.parseInt(elementoActu[1]));
            arrayFin.add(Objeto);
        }
  
        return arrayFin;
    }
    public  ArrayList<Venta> getMenorVenta(int idHotel) throws SQLException{
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String url= "jdbc:mysql://localhost:3307/admin";
        Connection conexion = DriverManager.getConnection(url, "root", "dkshertzy18");
        CallableStatement cstmt = conexion.prepareCall("{? = call getMenoresVentas(?)}");
        cstmt.setInt(2, idHotel);
        cstmt.registerOutParameter(1, OracleTypes.VARCHAR);
 
        cstmt.execute();
         String resultado = cstmt.getString(1);           
            // Obtener cada elemento concatenado por separado
        String[] elementos = resultado.split("\n");
     
        
         ArrayList<Venta> arrayFin = new ArrayList();
         String[] elementoActu;
        for (String elemento : elementos) {
            elementoActu = elemento.split(", ");
             Venta Objeto = new  Venta();
            // String fechaStringCheckin = dateFormat.format(elementoActu[0]);
             Objeto.setFecha(elementoActu[0]);
             Objeto.setCantidad(Integer.parseInt(elementoActu[1]));
            arrayFin.add(Objeto);
        }
  
        return arrayFin;
    }
    public  int insertBookingAndGetBooking(BookingBD book, int idPerson, int idRoom) throws SQLException, ParseException{

        if(book.getIdFeedback()!=0){
             String url= "jdbc:mysql://localhost:3307/admin";
            Connection conexion = DriverManager.getConnection(url, "root", "dkshertzy18");
            CallableStatement stmt = conexion.prepareCall("{ ? = call insertBooking(?,?,?,?,?,?)}");

            stmt.registerOutParameter(1, Types.INTEGER); // Parámetro de salida para el valor de retorno

            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

            java.util.Date parsedDateCheckIn = dateFormat.parse(book.getCheckIn());
            java.sql.Date sqlDateCheckIn = new java.sql.Date(parsedDateCheckIn.getTime());
            stmt.setDate(2, sqlDateCheckIn);

            java.util.Date parsedDateCheckOut = dateFormat.parse(book.getCheckOut());
            java.sql.Date sqlDateCheckout = new java.sql.Date(parsedDateCheckOut.getTime());
            stmt.setDate(3, sqlDateCheckout);

            stmt.setInt(4, book.getPrice());
            stmt.setInt(5, idPerson);
            stmt.setInt(6, book.getIdFeedback());
            stmt.setInt(7, idRoom);

            stmt.execute();
            int returnValue = stmt.getInt(1);
            stmt.close();
            conexion.close();
            return returnValue; 
        }else{
             String url= "jdbc:mysql://localhost:3307/admin";
        Connection con = DriverManager.getConnection(url, "root", "dkshertzy18");
            CallableStatement stmt = con.prepareCall("{ ? = call INSERTBOOKING(?,?,?,?,?,?)}");

            stmt.registerOutParameter(1, Types.INTEGER); // Parámetro de salida para el valor de retorno

            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

            java.util.Date parsedDateCheckIn = dateFormat.parse(book.getCheckIn());
            java.sql.Date sqlDateCheckIn = new java.sql.Date(parsedDateCheckIn.getTime());
            stmt.setDate(2, sqlDateCheckIn);

            java.util.Date parsedDateCheckOut = dateFormat.parse(book.getCheckOut());
            java.sql.Date sqlDateCheckout = new java.sql.Date(parsedDateCheckOut.getTime());
            stmt.setDate(3, sqlDateCheckout);

            stmt.setInt(4, book.getPrice());
            stmt.setInt(5, idPerson);
            stmt.setNull(6, Types.INTEGER);
            stmt.setInt(7, idRoom);

            stmt.execute();
            int returnValue = stmt.getInt(1);
            stmt.close();
            con.close();
            return returnValue; 
        }
        
    }
    public ArrayList<StarHotel> getStarHotel() throws SQLException{
         String url= "jdbc:mysql://localhost:3307/admin";
        Connection conexion = DriverManager.getConnection(url, "root", "dkshertzy18");
        CallableStatement cstmt = conexion.prepareCall("{? = call getStarHotel()}");
        cstmt.registerOutParameter(1, OracleTypes.VARCHAR);
 
        cstmt.execute();
         String resultado = cstmt.getString(1);           
            // Obtener cada elemento concatenado por separado
        String[] elementos = resultado.split(";");
        
         ArrayList<StarHotel> arrayFin = new ArrayList();
         String[] elementoActu;
        for (String elemento : elementos) {
            elementoActu = elemento.split(":");
             StarHotel Objeto = new  StarHotel(Integer.parseInt(elementoActu[0]),elementoActu[1]);

            arrayFin.add(Objeto);
        }
  
        return arrayFin;
    }
    public ArrayList<PoliticaCancelacionBD> getPoliticaCancelacion(int idHotel) throws SQLException{
       String url= "jdbc:mysql://localhost:3307/admin";
        Connection conexion = DriverManager.getConnection(url, "root", "dkshertzy18");
        CallableStatement cstmt = conexion.prepareCall("{? = call getCancellacionP(?)}");
        cstmt.setInt(2, idHotel);
        cstmt.registerOutParameter(1, OracleTypes.VARCHAR);
 
        cstmt.execute();
         String resultado = cstmt.getString(1);           
            // Obtener cada elemento concatenado por separado
        String[] elementos = resultado.split(";");
        
         ArrayList<PoliticaCancelacionBD> arrayFin = new ArrayList();
         String[] elementoActu;
        for (String elemento : elementos) {
            elementoActu = elemento.split(":");
             PoliticaCancelacionBD Objeto = new  PoliticaCancelacionBD();
             Objeto.setId(Integer.parseInt(elementoActu[0]));
             Objeto.setName(elementoActu[1]);
             Objeto.setPorcentaje(Integer.parseInt(elementoActu[2]));
            arrayFin.add(Objeto);
        }
  
        return arrayFin;
    }
    public  int getIdRating(int number) throws SQLException{
        String url= "jdbc:mysql://localhost:3307/admin";
        Connection conexion = DriverManager.getConnection(url, "root", "dkshertzy18");
        
        CallableStatement cstmt = conexion.prepareCall("{? = call insertRating(?)}");
        cstmt.registerOutParameter(1, Types.INTEGER);
        cstmt.setInt(2, number);
        
        cstmt.execute();
        int returnValue = cstmt.getInt(1);
        cstmt.close();
        conexion.close();
        return returnValue;
    }
    public int getIdFeedBack(String commentary, int idRating) throws SQLException{
         String url= "jdbc:mysql://localhost:3307/admin";
        Connection conexion = DriverManager.getConnection(url, "root", "dkshertzy18");
        
        CallableStatement cstmt = conexion.prepareCall("{? = call insertFeedBack(?,?)}");
        cstmt.registerOutParameter(1, Types.INTEGER);
        cstmt.setString(2, commentary);
        cstmt.setInt(3, idRating);
        
        cstmt.execute();
        int returnValue = cstmt.getInt(1);
        cstmt.close();
        conexion.close();
        return returnValue;
    }
    public int getIdHotelInsertHotel(RecibirDatosBD.Hotel hotel) throws SQLException{
         String url= "jdbc:mysql://localhost:3307/admin";
        Connection conexion = DriverManager.getConnection(url, "root", "dkshertzy18");
        CallableStatement cstmt = conexion.prepareCall("{? = call insertHotel(?,?)}");
        cstmt.registerOutParameter(1, Types.INTEGER);
        cstmt.setString(2, hotel.getName());
        cstmt.setInt(3, hotel.getIdDistrict());
        
        cstmt.execute();
        int returnValue = cstmt.getInt(1);
        cstmt.close();
        conexion.close();
        return returnValue;
    }
    public void insertAmenityHotel(String name, int idHotel) throws SQLException{
        String url= "jdbc:mysql://localhost:3307/admin";
        Connection conexion = DriverManager.getConnection(url, "root", "dkshertzy18");
        CallableStatement cstmt = conexion.prepareCall("{call insertAmenityHotel(?,?)}");
        cstmt.setString(1, name);
        cstmt.setInt(2, idHotel);
        
        cstmt.execute();
        cstmt.close();
        conexion.close();
    }
    public void personFavHotel(int idPerson, int idHotel) throws SQLException{
         String url= "jdbc:mysql://localhost:3307/admin";
        Connection conexion = DriverManager.getConnection(url, "root", "dkshertzy18");
        CallableStatement cstmt = conexion.prepareCall("{call personFavHotel(?,?)}");
        cstmt.setInt(1, idPerson);
        cstmt.setInt(2, idHotel);
        
        cstmt.execute();
        cstmt.close();
        conexion.close();
    }
    public int getIdRoomInsertRoom(RecibirDatosBD.Room room) throws SQLException{
         String url= "jdbc:mysql://localhost:3307/admin";
        Connection conexion = DriverManager.getConnection(url, "root", "dkshertzy18");
        CallableStatement cstmt = conexion.prepareCall("{? = call insertRoom(?,?,?,?)}");
        cstmt.registerOutParameter(1, Types.INTEGER);
        cstmt.setString(2, room.getName());
        cstmt.setInt(3, room.getCapacity());
        cstmt.setInt(4, room.getPrice());
        cstmt.setInt(5, room.getIdHotel());
        
        cstmt.execute();
        int returnValue = cstmt.getInt(1);
        cstmt.close();
        conexion.close();
        return returnValue;
    }
    public int getcalcularPromedioCalifiacion(int idHotel) throws SQLException{
         String url= "jdbc:mysql://localhost:3307/admin";
        Connection conexion = DriverManager.getConnection(url, "root", "dkshertzy18");
        CallableStatement cstmt = conexion.prepareCall("{? = call calcularPromedioCalifiacion(?)}");
        cstmt.registerOutParameter(1, Types.INTEGER);
        cstmt.setInt(2, idHotel);
        
        cstmt.execute();
        int returnValue = cstmt.getInt(1);
        cstmt.close();
        conexion.close();
        return returnValue;
    }
    public ArrayList<CommentaryBD> getComentariosHotel(int idHotel) throws SQLException{
         String url= "jdbc:mysql://localhost:3307/admin";
        Connection conexion = DriverManager.getConnection(url, "root", "dkshertzy18");
        CallableStatement cstmt = conexion.prepareCall("{? = call getComentariosHotel(?)}");
        cstmt.setInt(2, idHotel);
        cstmt.registerOutParameter(1, OracleTypes.VARCHAR);
 
        cstmt.execute();
         String resultado = cstmt.getString(1);           
            // Obtener cada elemento concatenado por separado
        String[] elementos = resultado.split("\n");
     

         ArrayList<CommentaryBD> arrayNation = new ArrayList();
         String[] elementoActu;
        for (String elemento : elementos) {
            elementoActu = elemento.split(", ");
             CommentaryBD objeto = new  CommentaryBD();
             objeto.setEstrella(Integer.parseInt(elementoActu[0]));
             objeto.setCommentary(elementoActu[1]);
            arrayNation.add(objeto);
        }
        return arrayNation;
    }
    public void insertAmenityRoom(String name) throws SQLException{
         String url= "jdbc:mysql://localhost:3307/admin";
        Connection conexion = DriverManager.getConnection(url, "root", "dkshertzy18");
        CallableStatement cstmt = conexion.prepareCall("{call insertAmenityRoom(?)}");
        cstmt.setString(1, name);
        
        cstmt.execute();
        cstmt.close();
        conexion.close();
    }
    public ArrayList<AmenidadesRoom> getAmenidyRoom() throws SQLException{
         String url= "jdbc:mysql://localhost:3307/admin";
        Connection conexion = DriverManager.getConnection(url, "root", "dkshertzy18");
        CallableStatement cstmt = conexion.prepareCall("{? = call getAmenityRoom()}");
        cstmt.registerOutParameter(1, OracleTypes.VARCHAR);
 
        cstmt.execute();
         String resultado = cstmt.getString(1);           
            // Obtener cada elemento concatenado por separado
        String[] elementos = resultado.split("\n");
     

         ArrayList<AmenidadesRoom> arrayNation = new ArrayList();
         String[] elementoActu;
        for (String elemento : elementos) {
            elementoActu = elemento.split(", ");
             AmenidadesRoom nationality = new  AmenidadesRoom(Integer.parseInt(elementoActu[0]),elementoActu[1]);
            arrayNation.add(nationality);
        }
  

        return arrayNation;
    }
    
    public  ArrayList<OfferBD> getOfferHotel(int idHotel) throws SQLException{
         String url= "jdbc:mysql://localhost:3307/admin";
        Connection conexion = DriverManager.getConnection(url, "root", "dkshertzy18");
        CallableStatement cstmt = conexion.prepareCall("{? = call getOffer(?)}");

        cstmt.setInt(2, idHotel);
        cstmt.registerOutParameter(1, OracleTypes.VARCHAR);
 
        cstmt.execute();
         String resultado = cstmt.getString(1);           
            // Obtener cada elemento concatenado por separado
        String[] elementos = resultado.split(";");
     

         ArrayList<OfferBD> arrayNation = new ArrayList();
         String[] elementoActu;
        for (String elemento : elementos) {
            elementoActu = elemento.split(":");
             OfferBD objeto = new  OfferBD();
             objeto.setId(Integer.parseInt(elementoActu[0]));
             objeto.setName(elementoActu[1]);
            arrayNation.add(objeto);
        }
  

        return arrayNation;
    }
    public ArrayList<OfferBD> getOfferHotelForName(int idHotel, String name) throws SQLException{
        String url= "jdbc:mysql://localhost:3307/admin";
        Connection conexion = DriverManager.getConnection(url, "root", "dkshertzy18");
        CallableStatement cstmt = conexion.prepareCall("{? = call getOfertasHotelForName(?,?)}");

        cstmt.setInt(2, idHotel);
        cstmt.setString(3, name);
        cstmt.registerOutParameter(1, OracleTypes.VARCHAR);
 
        cstmt.execute();
         String resultado = cstmt.getString(1);           
            // Obtener cada elemento concatenado por separado
        String[] elementos = resultado.split("\n");
     

         ArrayList<OfferBD> arrayNation = new ArrayList();
         String[] elementoActu;
        for (String elemento : elementos) {
            elementoActu = elemento.split(", ");
             OfferBD objeto = new  OfferBD();
             objeto.setName(elementoActu[0]);
             objeto.setDescripcion(elementoActu[1]);
            arrayNation.add(objeto);
        }
  

        return arrayNation;
    }
    public void insertAmenityRoomDeLaTablaNXN(int idRoom, int idAmenity) throws SQLException{
        String url= "jdbc:mysql://localhost:3307/admin";
        Connection conexion = DriverManager.getConnection(url, "root", "dkshertzy18");
        CallableStatement cstmt = conexion.prepareCall("{call insertRoomXAR(?,?)}");
        cstmt.setInt(1, idRoom);
        cstmt.setInt(2, idAmenity);
        cstmt.execute();
        cstmt.close();
        conexion.close();
    }
    public void deleteOffer(int idOffer, int idHotel) throws SQLException{
        String url= "jdbc:mysql://localhost:3307/admin";
        Connection conexion = DriverManager.getConnection(url, "root", "dkshertzy18");
        CallableStatement cstmt = conexion.prepareCall("{call deleteOffer(?,?)}");
        cstmt.setInt(1, idOffer);
        cstmt.setInt(2, idHotel);
        cstmt.execute();
        cstmt.close();
        conexion.close();
    }
    public void insertCancelacion(String name, int porcentaje, int idHotel) throws SQLException{
         String url= "jdbc:mysql://localhost:3307/admin";
        Connection conexion = DriverManager.getConnection(url, "root", "dkshertzy18");
        CallableStatement cstmt = conexion.prepareCall("{call insertCancellacionP(?,?,?)}");
        cstmt.setString(1, name);
        cstmt.setInt(2, porcentaje);
        cstmt.setInt(3, idHotel);
        cstmt.execute();
        cstmt.close();
        conexion.close();
    }
    public void insertOffer(OfferBD offer) throws SQLException, ParseException{
         String url= "jdbc:mysql://localhost:3307/admin";
        Connection conexion = DriverManager.getConnection(url, "root", "dkshertzy18");
        CallableStatement cstmt = conexion.prepareCall("{call insertOffer(?,?,?,?,?,?,?)}");
        
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        
        cstmt.setString(1, offer.getName());
        cstmt.setInt(2, offer.getDiasMinimos());
        cstmt.setString (3, offer.getDescripcion());
        
        java.util.Date parsedDateStar = dateFormat.parse(offer.getStarDate());
        java.sql.Date sqlDateStar = new java.sql.Date(parsedDateStar.getTime());
        cstmt.setDate(4, sqlDateStar);
        java.util.Date parsedDateFinish = dateFormat.parse(offer.getFinishDate());
        java.sql.Date sqlDateFinish = new java.sql.Date(parsedDateFinish.getTime());
        cstmt.setDate(5, sqlDateFinish);
        
        cstmt.setInt(6, offer.getIdRoom());
        cstmt.setInt(7, offer.getIdHotel());
        
        cstmt.execute();
        cstmt.close();
        conexion.close();
    }
  public boolean areKeysPresent() throws SQLException {
    String url = "jdbc:mariadb://localhost:3307/admin";
    
    try (Connection conexion = DriverManager.getConnection(url, "root", "dkshertzy18")) {
        // Verificar si hay registros en la tabla keys_table
        PreparedStatement statement = conexion.prepareStatement("SELECT COUNT(*) AS count FROM keys_table");

        ResultSet resultSet = statement.executeQuery();

        if (resultSet.next()) {
            int count = resultSet.getInt("count");
            return count > 0;
        }
    }

    return false;
}
    public  void saveKeysToDatabase(KeyPair keyPair) throws SQLException {
        // Obtener la conexión a la base de datos
         String url= "jdbc:mysql://localhost:3307/admin";
            
        try (Connection conexion = DriverManager.getConnection(url, "root", "dkshertzy18")) {
            // Insertar las claves en la tabla de la base de datos
            PreparedStatement statement = conexion.prepareStatement("INSERT INTO keys_table (public_key, private_key) VALUES (?, ?)");

            // Obtener la representación de bytes de las claves
            byte[] publicKeyBytes = keyPair.getPublic().getEncoded();
            byte[] privateKeyBytes = keyPair.getPrivate().getEncoded();

            // Establecer los parámetros de la sentencia INSERT
            statement.setBytes(1, publicKeyBytes);
            statement.setBytes(2, privateKeyBytes);

            // Ejecutar la sentencia INSERT
            statement.executeUpdate();
        }
    }
    public  KeyPair loadKeysFromDatabase() throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException {
        // Obtener la conexión a la base de datos
         String url= "jdbc:mysql://localhost:3307/admin";
        try (Connection conexion = DriverManager.getConnection(url, "root", "dkshertzy18")) {
            // Recuperar las claves de la tabla de la base de datos
            PreparedStatement statement = conexion.prepareStatement("SELECT public_key, private_key FROM keys_table");
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                // Obtener los bytes de las claves desde el resultado de la consulta
                byte[] publicKeyBytes = resultSet.getBytes("public_key");
                byte[] privateKeyBytes = resultSet.getBytes("private_key");

                // Construir las claves a partir de los bytes
                KeyFactory keyFactory = KeyFactory.getInstance("RSA");

                X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(publicKeyBytes);
                PublicKey publicKey = keyFactory.generatePublic(publicKeySpec);

                PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(privateKeyBytes);
                PrivateKey privateKey = keyFactory.generatePrivate(privateKeySpec);

                return new KeyPair(publicKey, privateKey);
            }
        }

        return null;
    }
}
