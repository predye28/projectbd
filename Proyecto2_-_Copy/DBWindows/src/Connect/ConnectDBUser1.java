/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Connect;
import RecibirDatosBD.Canton;
import RecibirDatosBD.District;
import RecibirDatosBD.Gender;
import RecibirDatosBD.Nationality;
import RecibirDatosBD.Province;
import RecibirDatosBD.TypeIdentificacion;
import Metodos.person;
import Metodos.user;
import RecibirDatosBD.Mascara;
import java.sql.Statement;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.sql.Types;
import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleTypes;
/**
 *
 * @author omarm
 */
public class ConnectDBUser1 {
    private String host = "localhost";
    private String port = "3307";
    private String dbName = "user1";
    private String userName = "root";
    private String password = "dkshertzy18";

    public ConnectDBUser1() {
    }
    
    public int insertPerson(person per) throws SQLException, ParseException{
       String url= "jdbc:mysql://localhost:3307/user1";

        //String url ="jdbc:mysql://"+ "localhost"+":"+"3307"+"/"+"user1";
       Connection conexion = DriverManager.getConnection(url, "root", "dkshertzy18");
   
       CallableStatement stmt = conexion.prepareCall("{ ? = call insertPerson(?,?,?,?,?,?,?,?,?,?,?,?)}");
       stmt.registerOutParameter(1, Types.VARCHAR);


     

        stmt.setString(2, per.getFirstName());
        stmt.setString(3, per.getSecondName());
        stmt.setString(4, per.getFirstLastName());
        stmt.setString(5, per.getSecondLastName());
        stmt.setString(6, per.getPhoto());

        String dateString = "3/5/2023";
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        java.util.Date parsedDate = dateFormat.parse(per.getBirthdate());
        java.sql.Date sqlDate = new java.sql.Date(parsedDate.getTime());
        stmt.setDate(7, sqlDate);

        stmt.setInt(8, per.getIdDistrict());

        stmt.setInt(9, per.getIdGender());
        stmt.setInt(10, per.getIdTypeId());
        stmt.setString(11, per.getNumIdentification());
        stmt.setString(12, per.getPhoneNumber());
        stmt.setString(13, per.getEmail());


        stmt.execute();
        int returnValue = stmt.getInt(1); // Obtener el valor de retorno
        System.out.println(returnValue);
        //agreagr nationalityX
        CallableStatement stmtN = conexion.prepareCall("{ call INSERTNATIONALITYXP(?,?)}");

        stmtN.setInt(1, returnValue);
        stmtN.setInt(2, per.getIdNationality());
        stmtN.execute();
        //crear user
        CallableStatement stmtU = conexion.prepareCall("{ call insertUser1(?,?,?,?,?)}");

        stmtU.setInt(1, per.getUserIdentificacion());
        stmtU.setString(2, per.getPassword());
        stmtU.setInt(3, returnValue);
        stmtU.setInt(4, 1);
        stmtU.setNull(5, java.sql.Types.INTEGER);
        stmtU.execute();

        System.out.println("LISTO");
        return returnValue; // Retornar el valor de retorno del procedimiento almacenado
}
    public ArrayList<Gender> getGender() throws SQLException{
        String url= "jdbc:mysql://localhost:3307/user1";
        Connection conexion = DriverManager.getConnection(url, "root", "dkshertzy18");
        
       CallableStatement cstmt = conexion.prepareCall("{? = call getGender()}");
       cstmt.registerOutParameter(1, Types.VARCHAR);
       cstmt.execute();
            
       String resultado = cstmt.getString(1);           
            // Obtener cada elemento concatenado por separado
        String[] elementos = resultado.split(";");
     

         ArrayList<Gender> arrayGen = new ArrayList();
         String[] elementoActu;
        for (String elemento : elementos) {
            elementoActu = elemento.split(":");
            Gender genero = new Gender(Integer.parseInt(elementoActu[0]),elementoActu[1]);
            arrayGen.add(genero);
        }
  

        return arrayGen;
    }
      
    
    public  ArrayList<Province> getProvince() throws SQLException{
        String url= "jdbc:mysql://localhost:3307/user1";
        Connection conexion = DriverManager.getConnection(url, "root", "dkshertzy18");
        
        CallableStatement cstmt = conexion.prepareCall("{? = call getProvince()}");
             cstmt.registerOutParameter(1, Types.VARCHAR);
       cstmt.execute();
            
       String resultado = cstmt.getString(1);           
            // Obtener cada elemento concatenado por separado
        String[] elementos = resultado.split(";");
     

         ArrayList<Province> arrayProv = new ArrayList();
         String[] elementoActu;
        for (String elemento : elementos) {
            elementoActu = elemento.split(":");
            Province province = new Province(Integer.parseInt(elementoActu[0]),elementoActu[1]);
            arrayProv.add(province);
        }
  

        return arrayProv;
    }
    public  ArrayList<Canton> getCanton(int id) throws SQLException{
         String url= "jdbc:mysql://localhost:3307/user1";
        Connection conexion = DriverManager.getConnection(url, "root", "dkshertzy18");
        
        CallableStatement cstmt = conexion.prepareCall("{? = call getCanton(?)}");
         cstmt.registerOutParameter(1, Types.VARCHAR);
         cstmt.setInt(2, id);
       cstmt.execute();
            
       String resultado = cstmt.getString(1);           
            // Obtener cada elemento concatenado por separado
        String[] elementos = resultado.split(";");
     

         ArrayList<Canton> arrayCan = new ArrayList();
         String[] elementoActu;
        for (String elemento : elementos) {
            elementoActu = elemento.split(":");
            Canton canton = new Canton(Integer.parseInt(elementoActu[0]),elementoActu[1]);
            arrayCan.add(canton);
        }
  

        return arrayCan;
    }
    public ArrayList<District> getDistrict(int id) throws SQLException{
         String url= "jdbc:mysql://localhost:3307/user1";
        Connection conexion = DriverManager.getConnection(url, "root", "dkshertzy18");
        
        CallableStatement cstmt = conexion.prepareCall("{? = call getDistrict(?)}");
          cstmt.registerOutParameter(1, Types.VARCHAR);
          cstmt.setInt(2, id);
       cstmt.execute();
            
       String resultado = cstmt.getString(1);           
            // Obtener cada elemento concatenado por separado
        String[] elementos = resultado.split(";");
     

         ArrayList<District> arrayDist = new ArrayList();
         String[] elementoActu;
        for (String elemento : elementos) {
            elementoActu = elemento.split(":");
            District district = new District(Integer.parseInt(elementoActu[0]),elementoActu[1]);
            arrayDist.add(district);
        }
  

        return arrayDist;
    }
    public ArrayList<TypeIdentificacion> getTypeId() throws SQLException{
         String url= "jdbc:mysql://localhost:3307/user1";
        Connection conexion = DriverManager.getConnection(url, "root", "dkshertzy18");
        
        CallableStatement cstmt = conexion.prepareCall("{? = call getTypeid()}");
        cstmt.registerOutParameter(1, OracleTypes.VARCHAR);
        cstmt.execute();
          String resultado = cstmt.getString(1);           
            // Obtener cada elemento concatenado por separado
        String[] elementos = resultado.split(";");
     

         ArrayList<TypeIdentificacion> arrayIdentifi = new ArrayList();
         String[] elementoActu;
        for (String elemento : elementos) {
            elementoActu = elemento.split(":");
             TypeIdentificacion tipeidentificacion = new TypeIdentificacion(Integer.parseInt(elementoActu[0]),elementoActu[1],elementoActu[2]);
            arrayIdentifi.add(tipeidentificacion);
        }
  

        return arrayIdentifi;
    }
    public ArrayList<Nationality> getNationality() throws SQLException{
         String url= "jdbc:mysql://localhost:3307/user1";
        Connection conexion = DriverManager.getConnection(url, "root", "dkshertzy18");
        
        CallableStatement cstmt = conexion.prepareCall("{? = call getNationality()}");
        cstmt.registerOutParameter(1, OracleTypes.VARCHAR);
 
        cstmt.execute();
         String resultado = cstmt.getString(1);           
            // Obtener cada elemento concatenado por separado
        String[] elementos = resultado.split(";");
     

         ArrayList<Nationality> arrayNation = new ArrayList();
         String[] elementoActu;
        for (String elemento : elementos) {
            elementoActu = elemento.split(":");
             Nationality nationality = new  Nationality(Integer.parseInt(elementoActu[0]),elementoActu[1]);
            arrayNation.add(nationality);
        }
  

        return arrayNation;
    }
//    public ArrayList<Mascara> getMascara() throws SQLException{
//        String url= "jdbc:mysql://localhost:3307/user1";
//        Connection conexion = DriverManager.getConnection(url, "root", "dkshertzy18");
//        
//        CallableStatement cstmt = conexion.prepareCall("{? = call getTypeid()}");
//        cstmt.registerOutParameter(1, OracleTypes.VARCHAR);
// 
//        cstmt.execute();
//      String resultado = cstmt.getString(1);           
//            // Obtener cada elemento concatenado por separado
//        String[] elementos = resultado.split(";");
//     
//
//         ArrayList<Mascara> arrayMascara = new ArrayList();
//         String[] elementoActu;
//        for (String elemento : elementos) {
//            elementoActu = elemento.split(":");
//             Mascara mascara = new  Mascara(Integer.parseInt(elementoActu[0]),elementoActu[1]);
//            arrayNation.add(nationality);
//        }
//  
//
//        return arrayNation;
//    }
    public int getidPersonFromNumUser(String numUser) throws SQLException{
        
       String url= "jdbc:mysql://localhost:3307/user1";
        Connection conexion = DriverManager.getConnection(url, "root", "dkshertzy18");
        
        CallableStatement cstmt = conexion.prepareCall("{? = call GETIDPERSONFROMUSER(?)}");
        cstmt.registerOutParameter(1, Types.INTEGER);
        cstmt.setString(2, numUser);
        
        cstmt.execute();
        int returnValue = cstmt.getInt(1);
        cstmt.close();
        conexion.close();
        return returnValue;
    }
    public int getIdTypeUserFromNumUser(String numUser) throws SQLException{
       String url= "jdbc:mysql://localhost:3307/user1";
        Connection conexion = DriverManager.getConnection(url, "root", "dkshertzy18");
        
        CallableStatement cstmt = conexion.prepareCall("{? = call GETIDTYPEOFUSER(?)}");
        cstmt.registerOutParameter(1, Types.INTEGER);
        cstmt.setString(2, numUser);
        
        cstmt.execute();
        int returnValue = cstmt.getInt(1);
        cstmt.close();
        conexion.close();
        return returnValue;
    }
    public  user getAllUser(int numIdentification) throws SQLException{
        String url= "jdbc:mysql://localhost:3307/user1";
        Connection conexion = DriverManager.getConnection(url, "root", "dkshertzy18");
        CallableStatement cstmt = conexion.prepareCall("{? = call getAllUser(?)}");
        cstmt.registerOutParameter(1, OracleTypes.VARCHAR);
        cstmt.setInt(2, numIdentification);
        cstmt.execute();
        System.out.println(numIdentification);
        
        String resultado = cstmt.getString(1); 
        
         System.out.println("Esto es el resultado");
        System.out.println(resultado);
        String[] elementos = resultado.split("\n"); 

        user user1 = null;

        for (String elemento : elementos){
            elementos = elemento.split(", "); 
            user1 = new user(Integer.parseInt(elementos[0]), elementos[1], Integer.parseInt(elementos[2]), Integer.parseInt(elementos[3]), Integer.parseInt(elementos[4]));
        }

        cstmt.close();
        conexion.close();
        System.out.println("AQUI");
        System.out.println(user1.getIdTypeOfUser());
        return user1;

//        //ResultSet rs  = ((OracleCallableStatement)cstmt).getCursor(1);
//        ResultSet rs = cstmt.executeQuery();
//        
//         String[] elementos = resultado.split(", ");
//        user user1 = null;
//        while (rs.next()) {
//           // Nationality natio = new Nationality(rs.getInt(1),rs.getString(2));
//            user1 = new user(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getInt(4), rs.getInt(5));
//        }
//        cstmt.close();
//        conexion.close();
//        return user1;
    }
    public void createAdmin(String numIdentificacion, int idHotel) throws SQLException{
        String url= "jdbc:mysql://localhost:3307/user1";
        Connection conexion = DriverManager.getConnection(url, "root", "dkshertzy18");
        
        CallableStatement cstmt = conexion.prepareCall("{call assignAdmin(?,?)}");
        cstmt.setString(1, numIdentificacion);
        cstmt.setInt(2, idHotel);
        
        cstmt.execute();
        cstmt.close();
        conexion.close();
    }
    public boolean isUserOnlyIdentifi(int pidenti) throws SQLException {
        String url= "jdbc:mysql://localhost:3307/user1";
        Connection conexion = DriverManager.getConnection(url, "root", "dkshertzy18");
        CallableStatement cstmt = conexion.prepareCall("{? = call isUserOnlyIdentifi(?)}");
        cstmt.registerOutParameter(1, Types.INTEGER);
        cstmt.setInt(2, pidenti);

        cstmt.execute();
        int resultado = cstmt.getInt(1);
        cstmt.close();
        conexion.close();
        return (resultado == 1); // Convertir a boolean
    }
    public String getPasswordFromUser(int numUser) throws SQLException{
        
        String url= "jdbc:mysql://localhost:3307/user1";
        Connection conexion = DriverManager.getConnection(url, "root", "dkshertzy18");
        CallableStatement cstmt = conexion.prepareCall("{? = call getPasswordFromUser(?)}");
        cstmt.registerOutParameter(1, Types.VARCHAR);
        cstmt.setInt(2, numUser);
        
        cstmt.execute();
        String returnValue = cstmt.getString(1);
        cstmt.close();
        conexion.close();
        return returnValue;
    }
}
