/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
    
 */
package Metodos;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import javax.crypto.Cipher;


/**
 *
 * @author Yessenia
 */
public class cifrado {

    public cifrado() {
    }
    

    public static KeyPair generateKeyPair() throws Exception {
        //Generador de claves RSA
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");

        // Inicializar el generador de claves con un tamaño de clave de 2048 bits
        keyPairGenerator.initialize(2048);
        return keyPairGenerator.generateKeyPair();
    }

    public static byte[] encrypt(String mensaje, PublicKey publicKey) throws Exception {
        // Crear un objeto Cipher
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.ENCRYPT_MODE, publicKey);

        //Mensaje cifrado
        return cipher.doFinal(mensaje.getBytes());
    }
    
    public static String decrypt(byte[] mensajeCifrado, PrivateKey privateKey) throws Exception {
        // Crear un objeto Cipher
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.DECRYPT_MODE, privateKey); 

        // Descifrar el mensaje
        byte[] mensajeDescifrado = cipher.doFinal(mensajeCifrado);

        return new String(mensajeDescifrado);
    }
    //
}


