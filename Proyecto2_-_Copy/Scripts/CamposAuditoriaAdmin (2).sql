-- Creación de campos de auditoría admin
-- Tabla AmenityHotel
ALTER TABLE amenityHotel
ADD creationDate DATE DEFAULT CURRENT_DATE() NOT NULL;

ALTER TABLE amenityHotel
ADD lastUpdate DATE DEFAULT CURRENT_DATE() NOT NULL;    

ALTER TABLE amenityHotel
ADD creationBy VARCHAR(25);

ALTER TABLE amenityHotel
ADD updateBy VARCHAR(25);

-- Tabla AmenityRoom
ALTER TABLE AmenityRoom
ADD creationDate DATE DEFAULT CURRENT_DATE() NOT NULL;

ALTER TABLE AmenityRoom
ADD lastUpdate DATE DEFAULT CURRENT_DATE() NOT NULL;    

ALTER TABLE AmenityRoom
ADD creationBy VARCHAR(25);

ALTER TABLE AmenityRoom
ADD updateBy VARCHAR(25);

-- Tabla backfeed
ALTER TABLE backfeed
ADD creationDate DATE DEFAULT CURRENT_DATE() NOT NULL;

ALTER TABLE backfeed
ADD lastUpdate DATE DEFAULT CURRENT_DATE() NOT NULL;    

ALTER TABLE backfeed
ADD creationBy VARCHAR(25);

ALTER TABLE backfeed
ADD updateBy VARCHAR(25);

-- Tabla booking
ALTER TABLE booking
ADD creationDate DATE DEFAULT CURRENT_DATE() NOT NULL;

ALTER TABLE booking
ADD lastUpdate DATE DEFAULT CURRENT_DATE() NOT NULL;    

ALTER TABLE booking
ADD creationBy VARCHAR(25);

ALTER TABLE booking
ADD updateBy VARCHAR(25);

-- Tabla bookingStar
ALTER TABLE bookingStar
ADD creationDate DATE DEFAULT CURRENT_DATE() NOT NULL;

ALTER TABLE bookingStar
ADD lastUpdate DATE DEFAULT CURRENT_DATE() NOT NULL;    

ALTER TABLE bookingStar
ADD creationBy VARCHAR(25);

ALTER TABLE bookingStar
ADD updateBy VARCHAR(25);

-- Tabla cancellacionP
ALTER TABLE cancellacionP
ADD creationDate DATE DEFAULT CURRENT_DATE() NOT NULL;

ALTER TABLE cancellacionP
ADD lastUpdate DATE DEFAULT CURRENT_DATE() NOT NULL;    

ALTER TABLE cancellacionP
ADD creationBy VARCHAR(25);

ALTER TABLE cancellacionP
ADD updateBy VARCHAR(25);
-- Tabla cancelPxB
ALTER TABLE cancelPxB
ADD creationDate DATE DEFAULT CURRENT_TIMESTAMP NOT NULL;

ALTER TABLE cancelPxB
ADD lastUpdate DATE DEFAULT CURRENT_TIMESTAMP NOT NULL;    

ALTER TABLE cancelPxB
ADD creationBy VARCHAR(25);

ALTER TABLE cancelPxB
ADD updateBy VARCHAR(25);
-------------------------------------------------------
-- Tabla Hotel
ALTER TABLE Hotel
ADD creationDate DATE DEFAULT CURRENT_TIMESTAMP NOT NULL;

ALTER TABLE Hotel
ADD lastUpdate DATE DEFAULT CURRENT_TIMESTAMP NOT NULL;    

ALTER TABLE Hotel
ADD creationBy VARCHAR(25);

ALTER TABLE Hotel
ADD updateBy VARCHAR(25);
-------------------------------------------------------
-- Tabla Offer
ALTER TABLE Offer
ADD creationDate DATE DEFAULT CURRENT_TIMESTAMP NOT NULL;

ALTER TABLE Offer
ADD lastUpdate DATE DEFAULT CURRENT_TIMESTAMP NOT NULL;    

ALTER TABLE Offer
ADD creationBy VARCHAR(25);

ALTER TABLE Offer
ADD updateBy VARCHAR(25);
-------------------------------------------------------
-- Tabla parameter
ALTER TABLE parameter
ADD creationDate DATE DEFAULT CURRENT_TIMESTAMP NOT NULL;

ALTER TABLE parameter
ADD lastUpdate DATE DEFAULT CURRENT_TIMESTAMP NOT NULL;    

ALTER TABLE parameter
ADD creationBy VARCHAR(25);

ALTER TABLE parameter
ADD updateBy VARCHAR(25);
-------------------------------------------------------
-- Tabla personXhotelFav
ALTER TABLE personXhotelFav
ADD creationDate DATE DEFAULT CURRENT_TIMESTAMP NOT NULL;

ALTER TABLE personXhotelFav
ADD lastUpdate DATE DEFAULT CURRENT_TIMESTAMP NOT NULL;    

ALTER TABLE personXhotelFav
ADD creationBy VARCHAR(25);

ALTER TABLE personXhotelFav
ADD updateBy VARCHAR(25);
-------------------------------------------------------
-- Tabla photo
ALTER TABLE photo
ADD creationDate DATE DEFAULT CURRENT_TIMESTAMP NOT NULL;

ALTER TABLE photo
ADD lastUpdate DATE DEFAULT CURRENT_TIMESTAMP NOT NULL;    

ALTER TABLE photo
ADD creationBy VARCHAR(25);

ALTER TABLE photo
ADD updateBy VARCHAR(25);
-------------------------------------------------------
-- Tabla rating
ALTER TABLE rating
ADD creationDate DATE DEFAULT CURRENT_TIMESTAMP NOT NULL;

ALTER TABLE rating
ADD lastUpdate DATE DEFAULT CURRENT_TIMESTAMP NOT NULL;    

ALTER TABLE rating
ADD creationBy VARCHAR(25);

ALTER TABLE rating
ADD updateBy VARCHAR(25);
-------------------------------------------------------
-- Tabla room
ALTER TABLE room
ADD creationDate DATE DEFAULT CURRENT_TIMESTAMP NOT NULL;

ALTER TABLE room
ADD lastUpdate DATE DEFAULT CURRENT_TIMESTAMP NOT NULL;    

ALTER TABLE room
ADD creationBy VARCHAR(25);

ALTER TABLE room
ADD updateBy VARCHAR(25);
-------------------------------------------------------
-- Tabla roomXar
ALTER TABLE roomXar
ADD creationDate DATE DEFAULT CURRENT_TIMESTAMP NOT NULL;

ALTER TABLE roomXar
ADD lastUpdate DATE DEFAULT CURRENT_TIMESTAMP NOT NULL;    

ALTER TABLE roomXar
ADD creationBy VARCHAR(25);

ALTER TABLE roomXar
ADD updateBy VARCHAR(25);
-------------------------------------------------------
-- Tabla typeOfpaymentm
ALTER TABLE typeOfpaymentm
ADD creationDate DATE DEFAULT CURRENT_TIMESTAMP NOT NULL;

ALTER TABLE typeOfpaymentm
ADD lastUpdate DATE DEFAULT CURRENT_TIMESTAMP NOT NULL;    

ALTER TABLE typeOfpaymentm
ADD creationBy VARCHAR(25);

ALTER TABLE typeOfpaymentm
ADD updateBy VARCHAR(25);
-------------------------------------------------------
-- Tabla typeOfpayMxB
ALTER TABLE typeOfpayMxB
ADD creationDate DATE DEFAULT CURRENT_TIMESTAMP NOT NULL;

ALTER TABLE typeOfpayMxB
ADD lastUpdate DATE DEFAULT CURRENT_TIMESTAMP NOT NULL;    

ALTER TABLE typeOfpayMxB
ADD creationBy VARCHAR(25);

ALTER TABLE typeOfpayMxB
ADD updateBy VARCHAR(25);
