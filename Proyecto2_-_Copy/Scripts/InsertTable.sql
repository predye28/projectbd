-- PERSON
INSERT INTO person(id, firstName, secondName, firstLastName, secondLastName, birthDate, idDistrict, idGender, idTypeId, numIdentification)
VALUES (NULL, 'Omar', 'Alexis', 'Madrigal', 'Rodriguez', STR_TO_DATE('14/07/2002', '%d/%m/%Y'), 7, 2, 1, '7-0293-0776');

INSERT INTO phone(id, phoneNumber, idPerson)
VALUES (NULL, '62748637', 2);

INSERT INTO email(id, name, idPerson)
VALUES (NULL, 'omarmr14.02@gmail.com', 2);

INSERT INTO nationalityXP(idPerson, idNationality)
VALUES (2, 3);

INSERT INTO user1(identification, password, idPerson, idTypeOfUser)
VALUES (111111, 'proyecto1', 2, 1);

-- Otra persona
INSERT INTO person(id, firstName, secondName, firstLastName, secondLastName, birthDate, idDistrict, idGender, idTypeId, numIdentification)
VALUES (NULL, 'Pablo', 'Alejandro', 'Castro', 'Umaa', STR_TO_DATE('18/09/2003', '%d/%m/%Y'), 9, 2, 1, '1-0653-0746');

INSERT INTO phone(id, phoneNumber, idPerson)
VALUES (NULL, '87541232', 3);

INSERT INTO email(id, name, idPerson)
VALUES (NULL, 'pabloaa13@gmail.com', 3);

INSERT INTO nationalityXP(idPerson, idNationality)
VALUES (3, 2);

INSERT INTO user1(identification, password, idPerson, idTypeOfUser)
VALUES (123412, 'password1', 3, 1);

DROP FUNCTION IF EXISTS gethotelsExpecifics;
