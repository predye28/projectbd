
ALTER TABLE hotel
ADD
CONSTRAINT fk_hotel0 FOREIGN KEY(idDistrict) REFERENCES user1.district(id); 

ALTER TABLE booking
ADD
CONSTRAINT fk_booking0 FOREIGN KEY(idPerson) REFERENCES user1.person(id); 

ALTER TABLE offer
ADD
CONSTRAINT fk_offer0 FOREIGN KEY(idRoom) REFERENCES room(id); 

ALTER TABLE offer
ADD
CONSTRAINT fk_offer1 FOREIGN KEY(idHotel) REFERENCES hotel(id); 

ALTER TABLE Backfeed
ADD
CONSTRAINT fk_Backfeed FOREIGN KEY(idRating) REFERENCES rating(id); 

admin
ALTER TABLE room
ADD
CONSTRAINT fk_room FOREIGN KEY(idHotel) REFERENCES hotel(id); 

ALTER TABLE roomXAR
ADD
CONSTRAINT fk_roomXAR0 FOREIGN KEY(idRoom) REFERENCES room(id); 

ALTER TABLE roomXAR
ADD
CONSTRAINT fk_roomXAR1 FOREIGN KEY(idAmenityRoom) REFERENCES amenityRoom(id); 

ALTER TABLE amenityHotelXAm
ADD
CONSTRAINT fk_amenityHotelXAm0 FOREIGN KEY(idAmenityHotel) REFERENCES amenityHotel(id); 

ALTER TABLE amenityHotelXAm
ADD
CONSTRAINT fk_amenityHotelXAm1 FOREIGN KEY(idHotel) REFERENCES Hotel(id); 




ALTER TABLE booking
ADD
CONSTRAINT fk_booking1 FOREIGN KEY(idFeedBack) REFERENCES backFeed(id);  

ALTER TABLE booking
ADD
CONSTRAINT fk_booking2 FOREIGN KEY(idBookingStar) REFERENCES bookingStar(id); 

ALTER TABLE booking
ADD
CONSTRAINT fk_booking3 FOREIGN KEY(idRoom) REFERENCES room(id); 



ALTER TABLE typeOfPayMXB
ADD
CONSTRAINT fk_typeOfPayMXB0 FOREIGN KEY(idBooking) REFERENCES booking(id); 

ALTER TABLE typeOfPayMXB
ADD
CONSTRAINT fk_typeOfPayMXB1 FOREIGN KEY(idtypeOfPayMXB) REFERENCES typeOfPaymentM(id); 


ALTER TABLE cancellacionP
ADD
CONSTRAINT fk_cancellacionP FOREIGN KEY(idHotel) REFERENCES hotel(id); 


ALTER TABLE cancelPXB
ADD
CONSTRAINT fk_cancelPXB0 FOREIGN KEY(idCancelP) REFERENCES cancellacionP(id); 

ALTER TABLE cancelPXB
ADD
CONSTRAINT fk_cancelPXB1 FOREIGN KEY(idBooking) REFERENCES booking(id); 

ALTER TABLE photo
ADD
CONSTRAINT fk_photo FOREIGN KEY(idHotel) REFERENCES hotel(id); 


ALTER TABLE personXHotelFav
ADD
CONSTRAINT fk_personXHotelFav0 FOREIGN KEY(idPerson) REFERENCES user1.person(id); 

ALTER TABLE personXHotelFav
ADD
CONSTRAINT fk_personXHotelFav1 FOREIGN KEY(idHotel) REFERENCES hotel(id); 


ALTER TABLE personXPred
ADD
CONSTRAINT fk_personXPred FOREIGN KEY(idHotel) REFERENCES hotel(id); 


ALTER TABLE personXPred
ADD
CONSTRAINT fk_personXPred0 FOREIGN KEY(idPerson) REFERENCES hotel(id); 
