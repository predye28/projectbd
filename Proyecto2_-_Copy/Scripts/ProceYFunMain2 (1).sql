DELIMITER //

CREATE OR REPLACE FUNCTION getHotelFav(pIdPerson INT)
RETURNS CURSOR READS SQL DATA
BEGIN
  DECLARE hotelfav CURSOR FOR
    SELECT pxh.idHotel
    FROM personXhotelFav pxh
    WHERE pxh.idPerson = pIdPerson;

  OPEN hotelfav;
  RETURN hotelfav;
END //

DELIMITER ;
