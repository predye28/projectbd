DELIMITER //

CREATE OR REPLACE FUNCTION getHotelFav(pIdPerson INT)
RETURNS INT
BEGIN
DECLARE hotelId INT;

SELECT pxh.idHotel INTO hotelId
FROM personXhotelFav pxh
WHERE pxh.idPerson = pIdPerson;

RETURN hotelId;
END //

DELIMITER ;