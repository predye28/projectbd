--CONSULTA 1
--falta nacionalidad de la persona y que muestre todas las personas
SELECT p.firstName, p.firstLastName, p.numIdentification, n.name, b.checkin, b.checkout, b.priceadministrator, r.name, h.name
FROM booking b
INNER JOIN room r ON b.idRoom = r.id
INNER JOIN hotel h ON r.idHotel = h.id
INNER JOIN user1.person p ON b.idPerson = p.id
INNER JOIN user1.nationalityXP nxp ON p.id = nxp.idPerson
INNER JOIN user1.nationality n ON nxp.idNationality = n.id
WHERE h.id = 1;
--ORDER BY p.id

--CONSULTA 2
--idHotel = 1
SELECT o.name, o.description, o.billedAmount AS MontoFacturado
FROM offer o
WHERE o.idHotel = 1;
--para agregar el filtro seria en un procedimiento

--CONSULTA 3 promedio
--IDHOTEL = 1
SELECT SUM(rat.name) / COUNT(rat.name) AS promedio
FROM booking b
INNER JOIN room r ON b.idRoom = r.id
INNER JOIN hotel h ON h.id = r.idHotel
INNER JOIN backfeed bf ON bf.id = b.idFeedBack
INNER JOIN rating rat ON rat.id = bf.idRating
WHERE h.id = 1;

--second part
SELECT p.FirstName, p.FirstLastName, b.checkin, b.checkOut, bf.commentary
FROM booking b
INNER JOIN room r ON b.idRoom = r.id
INNER JOIN hotel h ON h.id = r.idHotel
INNER JOIN backfeed bf ON bf.id = b.idFeedBack
INNER JOIN user1.person p ON p.id = b.idPerson
WHERE h.id = 1;

--CONSULTA 4 y 5

--
--pal master
-- Consulta 1
SELECT h.name, r.name, b.checkin, b.priceAdministrator
FROM booking b
INNER JOIN room r ON b.idRoom = r.id
INNER JOIN hotel h ON r.idHotel = h.id
ORDER BY h.id;

-- Consulta 2
SELECT SUM(b.priceAdministrator)
FROM booking b
INNER JOIN room r ON b.idRoom = r.id
INNER JOIN hotel h ON r.idHotel = h.id
WHERE h.id = 2;

