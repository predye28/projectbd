-- A) Listado de personas en un hotel
DELIMITER //

CREATE OR REPLACE FUNCTION getPersonasInHotel(pIdHotel INT) RETURNS VARCHAR(255)
BEGIN
  DECLARE personasHotelResult VARCHAR(255);
  
  SELECT GROUP_CONCAT(CONCAT(p.firstName, ' ', p.firstLastName)) INTO personasHotelResult
  FROM booking b
  INNER JOIN room r ON b.idRoom = r.id
  INNER JOIN hotel h ON r.idHotel = h.id
  INNER JOIN person p ON b.idPerson = p.id
  INNER JOIN nationalityXP nxp ON p.id = nxp.idPerson
  INNER JOIN nationality n ON nxp.idNationality = n.id
  WHERE h.id = pIdHotel;
  
  RETURN personasHotelResult;
END;


DELIMITER ;


-- B) Listado de ofertas filtrado por nombre
DELIMITER //

CREATE OR REPLACE FUNCTION getOfertasHotelForName(pIdHotel INT, pName VARCHAR(255)) RETURNS VARCHAR(255)
BEGIN
  DECLARE ofertasHotelResult VARCHAR(255);
  
  SELECT GROUP_CONCAT(CONCAT(o.name, ': ', o.description) SEPARATOR '; ') INTO ofertasHotelResult
  FROM offer o
  WHERE o.idHotel = pIdHotel AND o.name = pName;
  
  RETURN ofertasHotelResult;
END //

DELIMITER ;


-- C) Promedio de calificación para un hotel
DELIMITER //

CREATE OR REPLACE FUNCTION calcularPromedioCalifiacion(pIdHotel INT) RETURNS DECIMAL(10,2)
BEGIN
  DECLARE promedio DECIMAL(10,2);
  
  SELECT SUM(rat.name) / COUNT(rat.name) INTO promedio
  FROM booking b
  INNER JOIN room r ON b.idRoom = r.id
  INNER JOIN hotel h ON h.id = r.idHotel
  INNER JOIN backfeed bf ON bf.id = b.idFeedBack
  INNER JOIN rating rat ON rat.id = bf.idRating
  WHERE h.id = pIdHotel;
  
  RETURN promedio;
END //

DELIMITER ;

-- Todos los comentarios de un hotel
DELIMITER //

CREATE OR REPLACE FUNCTION getComentariosHotel(pIdHotel INT) RETURNS VARCHAR(255)
BEGIN
  DECLARE comentariosHotelResult VARCHAR(255);
  
  SELECT GROUP_CONCAT(CONCAT(rat.name, ': ', bf.commentary) SEPARATOR '; ') INTO comentariosHotelResult
  FROM booking b
  INNER JOIN room r ON b.idRoom = r.id
  INNER JOIN hotel h ON h.id = r.idHotel
  INNER JOIN backfeed bf ON bf.id = b.idFeedBack
  INNER JOIN rating rat ON rat.id = bf.idRating
  WHERE h.id = pIdHotel;
  
  RETURN comentariosHotelResult;
END //

DELIMITER ;

-- D) Reporte de mayores días de venta
DELIMITER //

CREATE OR REPLACE FUNCTION getMayoresVentas(pIdHotel INT) RETURNS VARCHAR(255)
BEGIN
  DECLARE mayoresVentasHotel VARCHAR(255);

  SELECT GROUP_CONCAT(CONCAT(fecha, ':', total_reservas)) INTO mayoresVentasHotel
  FROM (
      SELECT fecha, COUNT(*) AS total_reservas
      FROM (
          SELECT DATE(b.payday) AS fecha
          FROM booking b
          INNER JOIN room r ON b.idRoom = r.id
          INNER JOIN hotel h ON h.id = r.idHotel
          WHERE h.id = pIdHotel
      ) AS ventas
      GROUP BY fecha
      ORDER BY total_reservas DESC
  ) AS subquery;

  RETURN mayoresVentasHotel;
END //

DELIMITER ;

-- E) Reporte de menores días de venta
DELIMITER //

CREATE OR REPLACE FUNCTION getMenoresVentas(pIdHotel INT) RETURNS VARCHAR(255)
BEGIN
  DECLARE menoresVentasHotel VARCHAR(255);
  
  SELECT GROUP_CONCAT(CONCAT(fecha, ':', total_reservas)) INTO menoresVentasHotel
  FROM (
      SELECT fecha, COUNT(*) AS total_reservas
      FROM (
          SELECT DATE(b.payday) AS fecha
          FROM booking b
          INNER JOIN room r ON b.idRoom = r.id
          INNER JOIN hotel h ON h.id = r.idHotel
          WHERE h.id = pIdHotel
      ) AS ventas
      GROUP BY fecha
      ORDER BY total_reservas ASC
  ) AS subquery;

  RETURN menoresVentasHotel;
END //

DELIMITER ;