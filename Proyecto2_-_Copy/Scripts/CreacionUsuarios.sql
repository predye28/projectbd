CREATE DATABASE admin;
CREATE DATABASE user1;

CREATE USER 'admin1'@'localhost' IDENTIFIED BY 'admin';
CREATE USER 'useer1'@'localhost' IDENTIFIED BY 'proyecto2';

GRANT ALL PRIVILEGES ON ADMIN1.* TO 'admin'@'localhost';
GRANT ALL PRIVILEGES ON admin.* TO 'user1'@'localhost';
--

GRANT SELECT, INSERT, UPDATE, DELETE ON USER1.person TO 'admin'@'localhost';



