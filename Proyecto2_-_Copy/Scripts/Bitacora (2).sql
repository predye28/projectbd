--Tabla bitácora
CREATE TABLE binnacle(
    idBinnacle INT(10) NOT NULL,
    nameScheme VARCHAR(10) NOT NULL,
    nameTable VARCHAR(30) NOT NULL,
    nameField VARCHAR(30) NOT NULL,
    valueBefore VARCHAR(100),
    valueCurrent VARCHAR(100) NOT NULL,
    creationDate DATE DEFAULT CURRENT_DATE,
    lastUpdate DATE DEFAULT CURRENT_DATE,
    creationBy VARCHAR(10) NOT NULL,
    updateBy VARCHAR(10)
);

--Alteración de la pk
ALTER TABLE binnacle
ADD CONSTRAINT pk_idBinnacle PRIMARY KEY(idBinnacle);

--Secuencia del id de la bitácora
CREATE SEQUENCE idBinnacle_seq START WITH 1 INCREMENT BY 1 MINVALUE 0 MAXVALUE 
100000 NO CYCLE;

--Trigger room para las actualizaciones del precio recomendado
CREATE TRIGGER beforeUpDateRecommendPrice BEFORE INSERT OR UPDATE ON admin.room
FOR EACH ROW
BEGIN 
    INSERT INTO binnacle(idBinnacle, nameScheme, nameTable, nameField, lastUpdate, valueBefore, valueCurrent, creationBy)
    VALUES(NEXTVAL('idBinnacle_seq'), 'admin', 'room', 'recommendPrice', CURRENT_DATE, OLD.recommendPrice, NEW.recommendPrice, CURRENT_USER());
END;

--Trigger amenityHotel
CREATE TRIGGER beforeUpDateAmenityHotel BEFORE INSERT OR UPDATE ON amenityHotel
FOR EACH ROW
BEGIN 
    INSERT INTO binnacle(idBinnacle, nameScheme, nameTable, nameField, lastUpdate, valueBefore, valueCurrent, creationBy)
    VALUES(NEXTVAL('idBinnacle_seq'), 'admin', 'amenityHotel', 'name', CURRENT_DATE, OLD.name, NEW.name, CURRENT_USER());
END;

--Trigger amenityRoom
CREATE TRIGGER beforeUpDateAmenityRoom BEFORE INSERT OR UPDATE ON amenityRoom
FOR EACH ROW
BEGIN 
    INSERT INTO binnacle(idBinnacle, nameScheme, nameTable, nameField, lastUpdate, valueBefore, valueCurrent, creationBy)
    VALUES(NEXTVAL('idBinnacle_seq'), 'admin', 'amenityRoom', 'name', CURRENT_DATE, OLD.name, NEW.name, CURRENT_USER());
END;

--Trigger booking checkIn
CREATE TRIGGER beforeUpDateBookingCheckIn BEFORE INSERT OR UPDATE ON booking
FOR EACH ROW
BEGIN 
    INSERT INTO binnacle(idBinnacle, nameScheme, nameTable, nameField, lastUpdate, valueBefore, valueCurrent, creationBy)
    VALUES(NEXTVAL('idBinnacle_seq'), 'admin', 'booking', 'checkIn', CURRENT_DATE, OLD.checkIn, NEW.checkIn, CURRENT_USER());
END;

--Trigger booking checkOut
CREATE TRIGGER beforeUpDateBookingCheckOut BEFORE INSERT OR UPDATE ON booking
FOR EACH ROW
BEGIN 
    INSERT INTO binnacle(idBinnacle, nameScheme, nameTable, nameField, lastUpdate, valueBefore, valueCurrent, creationBy)
    VALUES(NEXTVAL('idBinnacle_seq'), 'admin', 'booking', 'checkOut', CURRENT_DATE, OLD.checkOut, NEW.checkOut, CURRENT_USER());
END;

--Trigger booking priceAdministrator
CREATE TRIGGER beforeUpDateBookingPrice BEFORE INSERT OR UPDATE ON booking
FOR EACH ROW
BEGIN 
    INSERT INTO binnacle(idBinnacle, nameScheme, nameTable, nameField, lastUpdate, valueBefore, valueCurrent, creationBy)
    VALUES(NEXTVAL('idBinnacle_seq'), 'admin', 'booking', 'priceAdministrator', CURRENT_DATE, OLD.priceAdministrator, NEW.priceAdministrator, CURRENT_USER());
END;

--Trigger CancellacionP name
CREATE TRIGGER beforeUpDateCancellacionP BEFORE INSERT OR UPDATE
