-- Agregar los parámetros
INSERT INTO bookingStar(id, name)
VALUES (NEXTVAL(idBookingStar), 1);
INSERT INTO bookingStar(id, name)
VALUES (NEXTVAL(idBookingStar), 2);
INSERT INTO bookingStar(id, name)
VALUES (NEXTVAL(idBookingStar), 3);
INSERT INTO bookingStar(id, name)
VALUES (NEXTVAL(idBookingStar), 4);
INSERT INTO bookingStar(id, name)
VALUES (NEXTVAL(idBookingStar), 5);

-- Después agregar estas dos a booking
UPDATE booking
SET idBookingStar = LAST_INSERT_ID()
WHERE id = 2;


-- PASOS QUE SE DEBEN DE SEGUIR PARA GUARDAR LA CALIFICACION DE ESTRELLAS DE LA RESERVA Y EL FEEDBACK

-- Agregar a bookingStar y backfeed la calificación que ingresó la persona

INSERT INTO rating(id, name)
VALUES (NEXTVAL(idRating), 5);
INSERT INTO backfeed(id, COMMENTARY, idRating)
VALUES (NEXTVAL(idbackFeed), 'Satisfecho con la reserva', LAST_INSERT_ID());

-- Otro backFeed
INSERT INTO rating(id, name)
VALUES (NEXTVAL(idRating), 4);
INSERT INTO backfeed(id, COMMENTARY, idRating)
VALUES (NEXTVAL(idbackFeed), 'Estuvo joya', LAST_INSERT_ID());

INSERT INTO rating(id, name)
VALUES (NEXTVAL(idRating), 3);
INSERT INTO backfeed(id, COMMENTARY, idRating)
VALUES (NEXTVAL(idbackFeed), 'Apetecible', LAST_INSERT_ID());


UPDATE booking
SET idFeedBack = 1
WHERE id = 2;

UPDATE booking
SET idFeedBack = 2
WHERE id = 3;

UPDATE booking
SET idFeedBack = 3
WHERE id = 5;
