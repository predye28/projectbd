
ALTER TABLE user1
ADD
CONSTRAINT fk_user0 FOREIGN KEY(idTypeOfUser) REFERENCES typeOfUser(id); 

ALTER TABLE user1
ADD
CONSTRAINT fk_user2 FOREIGN KEY(idHotel) REFERENCES admin.hotel(id); 

ALTER TABLE person
ADD
CONSTRAINT fk_person0 FOREIGN KEY(idDistrict) REFERENCES district(id); 

ALTER TABLE person
ADD
CONSTRAINT fk_person1 FOREIGN KEY(idGender) REFERENCES gender(id);

ALTER TABLE person
ADD
CONSTRAINT fk_person2 FOREIGN KEY(idTypeId) REFERENCES typeId(id);



ALTER TABLE phone
ADD
CONSTRAINT fk_phone FOREIGN KEY(idPerson) REFERENCES person(id); 

ALTER TABLE email 
ADD
CONSTRAINT fk_email FOREIGN KEY(idPerson) REFERENCES person(id); 

ALTER TABLE province 
ADD
CONSTRAINT fk_province FOREIGN KEY(idCountry) REFERENCES country(id); 

ALTER TABLE canton
ADD
CONSTRAINT fk_canton FOREIGN KEY(idProvince) REFERENCES province(id); 

ALTER TABLE district
ADD
CONSTRAINT fk_district FOREIGN KEY(idCanton) REFERENCES canton(id); 

ALTER TABLE nationalityXP
ADD
CONSTRAINT fk_nationalityXP0 FOREIGN KEY(idPerson) REFERENCES person(id); 

ALTER TABLE nationalityXP
ADD
CONSTRAINT fk_nationalityXP1 FOREIGN KEY(idNationality) REFERENCES nationality(id);


