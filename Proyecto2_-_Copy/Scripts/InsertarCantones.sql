-- UPDATE user1
UPDATE user1
SET idTypeOfUser = 3
WHERE idPerson = 25;


INSERT INTO typeofuser(id, name)
VALUES (1,"User");

INSERT INTO typeofuser(id, name)
VALUES (2,"Admin");


INSERT INTO typeofuser(id, name)
VALUES (3,"Master");

INSERT INTO typeid(id, name, identification)
VALUES (NEXTVAL(idtypeid),"Cédula", "?-0???-0???");
-- SAN JOSE:

INSERT INTO canton(id, name, idProvince)
VALUES (NEXTVAL(idcanton), 'San José', 1);

INSERT INTO canton(id, name, idProvince)
VALUES (NEXTVAL(idcanton), 'Escazú', 1);

INSERT INTO canton(id, name, idProvince)
VALUES (NEXTVAL(idcanton), 'Desamparados', 1);

INSERT INTO canton(id, name, idProvince)
VALUES (NEXTVAL(idcanton), 'Puriscal', 1);

INSERT INTO canton(id, name, idProvince)
VALUES (NEXTVAL(idcanton), 'Tarrazú', 1);

INSERT INTO canton(id, name, idProvince)
VALUES (NEXTVAL(idcanton), 'Aserrí', 1);

INSERT INTO canton(id, name, idProvince)
VALUES (NEXTVAL(idcanton), 'Mora', 1);

INSERT INTO canton(id, name, idProvince)
VALUES (NEXTVAL(idcanton), 'Goicoechea', 1);

INSERT INTO canton(id, name, idProvince)
VALUES (NEXTVAL(idcanton), 'Santa Ana', 1);

INSERT INTO canton(id, name, idProvince)
VALUES (NEXTVAL(idcanton), 'Alajuelita', 1);

INSERT INTO canton(id, name, idProvince)
VALUES (NEXTVAL(idcanton), 'Vázquez de Coronado', 1);

INSERT INTO canton(id, name, idProvince)
VALUES (NEXTVAL(idcanton), 'Acosta', 1);

INSERT INTO canton(id, name, idProvince)
VALUES (NEXTVAL(idcanton), 'Tibás', 1);

INSERT INTO canton(id, name, idProvince)
VALUES (NEXTVAL(idcanton), 'Moravia', 1);

INSERT INTO canton(id, name, idProvince)
VALUES (NEXTVAL(idcanton), 'Montes de Oca', 1);

INSERT INTO canton(id, name, idProvince)
VALUES (NEXTVAL(idcanton), 'Turrubares', 1);

INSERT INTO canton(id, name, idProvince)
VALUES (NEXTVAL(idcanton), 'Dota', 1);

INSERT INTO canton(id, name, idProvince)
VALUES (NEXTVAL(idcanton), 'Curridabat', 1);

INSERT INTO canton(id, name, idProvince)
VALUES (NEXTVAL(idcanton), 'Pérez Zeledón', 1);

INSERT INTO canton(id, name, idProvince)
VALUES (NEXTVAL(idcanton), 'León Cortés Castro', 1);

-- ALAJUELA
INSERT INTO canton(id, name, idProvince)
VALUES (NEXTVAL(idcanton), 'Alajuela', 2);

INSERT INTO canton(id, name, idProvince)
VALUES (NEXTVAL(idcanton), 'San Ramón', 2);

INSERT INTO canton(id, name, idProvince)
VALUES (NEXTVAL(idcanton), 'Grecia', 2);

INSERT INTO canton(id, name, idProvince)
VALUES (NEXTVAL(idcanton), 'San Mateo', 2);

INSERT INTO canton(id, name, idProvince)
VALUES (NEXTVAL(idcanton), 'Atenas', 2);

-- CARTAGO
INSERT INTO canton(id, name, idProvince)
VALUES (NEXTVAL(idcanton), 'Cartago', 3);

INSERT INTO canton(id, name, idProvince)
VALUES (NEXTVAL(idcanton), 'Paraíso', 3);

INSERT INTO canton(id, name, idProvince)
VALUES (NEXTVAL(idcanton), 'La Unión', 3);

-- HEREDIA
INSERT INTO canton(id, name, idProvince)
VALUES (NEXTVAL(idcanton), 'Heredia', 4);

INSERT INTO canton(id, name, idProvince)
VALUES (NEXTVAL(idcanton), 'Barva', 4);

-- HEREDIA
INSERT INTO canton(id, name, idProvince)
VALUES (NEXTVAL(idcanton), 'Santo Domingo', 4);

-- GUANACASTE
INSERT INTO canton(id, name, idProvince)
VALUES (NEXTVAL(idcanton), 'Liberia', 5);

INSERT INTO canton(id, name, idProvince)
VALUES (NEXTVAL(idcanton), 'Nicoya', 5);

INSERT INTO canton(id, name, idProvince)
VALUES (NEXTVAL(idcanton), 'Santa Cruz', 5);

