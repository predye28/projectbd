-- GENDER
INSERT INTO gender(id, name)
VALUES (NEXTVAL(idGender), 'Femenino');

INSERT INTO gender(id, name)
VALUES (NEXTVAL(idGender), 'Masculino');

INSERT INTO gender(id, name)
VALUES (NEXTVAL(idGender), 'Otro');

-- typeId
INSERT INTO typeId(id, mask, identification)
VALUES (DEFAULT, 'Cedula', '0-0000-0000');

-- typeOfUser
INSERT INTO typeOfUser(id, name)
VALUES (DEFAULT, 'User');

INSERT INTO typeOfUser(id, name)
VALUES (DEFAULT, 'Admin');

-- Nationality
INSERT INTO nationality(id, name)
VALUES (DEFAULT, 'Costarricense');

INSERT INTO nationality(id, name)
VALUES (DEFAULT, 'Salvadoreo');

-- UBICACION GEOGRAFICA

-- country
INSERT INTO country(id, name)
VALUES (DEFAULT, 'Costa Rica');

-- province
INSERT INTO province(id, name, idCountry)
VALUES (NEXTVAL(idProvince), 'San José', 1);

INSERT INTO province(id, name, idCountry)
VALUES (NEXTVAL(idProvince), 'Alajuela', 1);

INSERT INTO province(id, name, idCountry)
VALUES (NEXTVAL(idProvince), 'Cartago', 1);

INSERT INTO province(id, name, idCountry)
VALUES (NEXTVAL(idProvince), 'Heredia', 1);

INSERT INTO province(id, name, idCountry)
VALUES (NEXTVAL(idProvince), 'Guanacaste', 1);

INSERT INTO province(id, name, idCountry)
VALUES (NEXTVAL(idProvince), 'Puntarenas', 1);

INSERT INTO province(id, name, idCountry)
VALUES (NEXTVAL(idProvince), 'Limón', 1);
