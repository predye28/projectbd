INSERT INTO nationality(id, name)
VALUES (NEXTVAL(idNationality),'Costarricense');

INSERT INTO nationality(id, name)
VALUES (NEXTVAL(idNationality),'Salvadoreño');

INSERT INTO nationality (id, name)
VALUES (NEXTVAL(idNationality), 'Afgano');

INSERT INTO nationality (id, name)
VALUES (NEXTVAL(idNationality), 'Albanés');

INSERT INTO nationality (id, name)
VALUES (NEXTVAL(idNationality), 'Alemán');

INSERT INTO nationality (id, name)
VALUES (NEXTVAL(idNationality), 'Argentino');

-- Nacionalidades 1-10
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Andorrano');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Angoleño');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Argelino');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Armenio');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Australiano');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Austriaco');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Azerí');

-- Nacionalidades 11-20
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Bahameño');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Bangladesí');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Barbadense');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Belga');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Beliceño');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Beninés');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Bielorruso');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Birmano');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Boliviano');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Bosnio');

-- Nacionalidades 21-30
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Brasileño');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Británico');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Bruneano');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Búlgaro');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Burkinés');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Burundés');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Cabo Verdiano');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Camboyano');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Camerunés');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Camerunés');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Canadiense');

-- Nacionalidades 31-40
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Catarí');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Chadiano');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Chileno');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Chino');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Chipriota');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Colombiano');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Comorense');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Congoleño');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Croata');

-- Nacionalidades 41-50
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Cubano');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Danés');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Dominicano');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Ecuatoriano');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Egipcio');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Emiratí');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Eritreo');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Escocés');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Eslovaco');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Esloveno');

-- Nacionalidades 51-60
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Español');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Estonio');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Etíope');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Filipino');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Finlandés');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Francés');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Gabonés');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Gambiano');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Georgiano');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Ghanés');

-- Nacionalidades 61-70
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Granadino');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Griego');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Guatemalteco');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Guineano');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Guineano-ecuatorial');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Guyanés');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Haitiano');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Holandés');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Hondureño');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Húngaro');

-- Nacionalidades 71-80
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Indio');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Indonesio');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Inglés');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Iraní');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Iraquí');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Irlandés');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Islandés');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Israelí');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Italiano');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality) 'Jamaiquino');

-- Nacionalidades 81-90
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Japonés');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Jordano');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Kazajo');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Keniano');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Kirguís');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Kiribatiano');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Kuwaití');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Laosiano');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Lesotense');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Letón');

-- Nacionalidades 91-100
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Libanés');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Liberiano');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Libio');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Liechtensteiniano');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Lituano');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Luxemburgués');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Malgache');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Malasio');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Malauí');
INSERT INTO nationality (id, name) VALUES (NEXTVAL(idNationality), 'Maldivo');
