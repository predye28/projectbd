CREATE TABLE `parameter`
(
    id INT(6) NOT NULL,
    name VARCHAR(25) NOT NULL,
    value INT(25) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE `KEYS_TABLE`
(
    id INT(6) NOT NULL,
    PUBLIC_KEY LONGBLOB,
    PRIVATE_KEY LONGBLOB,
    PRIMARY KEY (id)
);


CREATE TABLE `typeOfPaymentM`
(
    id INT(6) NOT NULL,
    name VARCHAR(25) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE `offer`
(
    id INT(6) NOT NULL,
    name VARCHAR(25) NOT NULL,
    miniumDay INT(6) NOT NULL,
    description VARCHAR(60) NOT NULL,
    startDate DATE NOT NULL DEFAULT CURRENT_DATE(),
    finishDate DATE NOT NULL DEFAULT CURRENT_DATE(),
    discountCode VARCHAR(25),
    discountPercentage DECIMAL(8,2),
    billedAmount INT(8),
    idRoom INT(6) NOT NULL,
    idHotel INT(6) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE `backFeed`
(
    id INT(6) NOT NULL,
    commentary VARCHAR(30) NOT NULL,
    idRating INT(6) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE `bookingStar`
(
    id INT(6) NOT NULL,
    name INT(5) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE `rating`
(
    id INT(6) NOT NULL,
    name INT(6) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE `room`
(
    id INT(6) NOT NULL,
    name VARCHAR(25) NOT NULL,
    discountCode VARCHAR(25),
    capacity INT(20) NOT NULL,
    recommendPrice DECIMAL(20,2) NOT NULL,
    idHotel INT(6) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE `roomXAR`
(
    idRoom INT(6) NOT NULL,
    idAmenityRoom INT(6) NOT NULL,
    PRIMARY KEY (idRoom, idAmenityRoom)
);

CREATE TABLE `amenityHotelXAm`
(
    idHotel INT(6) NOT NULL,
    idAmenityHotel INT(6) NOT NULL,
    PRIMARY KEY (idHotel, idAmenityHotel)
);

CREATE TABLE `amenityRoom`
(
    id INT(6) NOT NULL,
    name VARCHAR(25) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE `amenityHotel`
(
    id INT(6) NOT NULL,
    name VARCHAR(25) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE `booking`
(
    id INT(6) NOT NULL,
    checkIn DATE NOT NULL DEFAULT CURRENT_DATE(),
    checkOut DATE NOT NULL DEFAULT CURRENT_DATE(),
    priceAdministrator DECIMAL(20,2) NOT NULL,
    payday DATE NOT NULL DEFAULT CURRENT_DATE(),
    idPerson INT(6) NOT NULL,
    idFeedBack INT(6),
    idBookingStar INT(6),
    idRoom INT(6) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE `typeOfPayMXB`
(
    id INT(6) NOT NULL,
    idBooking INT(6) NOT NULL,
    idtypeOfPayMXB INT(6) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE `hotel`
(
    id INT(6) NOT NULL,
    name VARCHAR(25) NOT NULL,
    dateRegister DATE NOT NULL DEFAULT CURRENT_DATE(),
    idDistrict INT(6) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE `personXHotelFAV`
(
    idPerson INT(6) NOT NULL,
    idHotel INT(6) NOT NULL,
    PRIMARY KEY (idPerson, idHotel)
);

CREATE TABLE `cancellacionP`
(
    id INT(6) NOT NULL,
    name VARCHAR(25) NOT NULL,
    days INT(10),
    percentage DECIMAL(25) NOT NULL,
    idHotel INT(6) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE cancelPXB
(
    id INT(6) NOT NULL,
    idCancelP INT(6) NOT NULL,
    idBooking INT(6) NOT NULL
);

CREATE TABLE photo
(
    id INT(6) NOT NULL,
    name VARCHAR(25) NOT NULL,
    idHotel INT(6) NOT NULL
);

CREATE TABLE `personXPred`
(
    idPerson INT(6) NOT NULL,
    idHotel INT(6) NOT NULL,
    PRIMARY KEY (idPerson, idHotel)
);
----

CREATE TABLE user1
(
    identification INT(6) NOT NULL,
    password VARCHAR(25) NOT NULL,
    idPerson INT(6) NOT NULL,
    idTypeOfUser INT(6) NOT NULL,
    idHotel INT(6)
);


CREATE TABLE `TypeOfUser`
(
    id INT(6) NOT NULL,
    name VARCHAR(25) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE `person`
(
    id INT(6) NOT NULL,
    firstName VARCHAR(25) NOT NULL,
    secondName VARCHAR(25) NOT NULL,
    firstLastName VARCHAR(25) NOT NULL,
    secondLastName VARCHAR(25) NOT NULL,
    birthDate DATE NOT NULL,
    photo VARCHAR(25),
    idDistrict INT(6) NOT NULL,
    idGender INT(6) NOT NULL,
    idTypeId INT(6) NOT NULL,
    idHotel INT(6) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE `gender`
(
    id INT(6) NOT NULL,
    name VARCHAR(25) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE `typeId`
(
    id INT(6) NOT NULL,
    mask VARCHAR(25) NOT NULL,
    identification VARCHAR(25) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE `phone`
(
    id INT(6) NOT NULL,
    phoneNumber VARCHAR(25) NOT NULL,
    idPerson INT(6) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE `email`
(
    id INT(6) NOT NULL,
    name VARCHAR(25) NOT NULL,
    idPerson INT(6) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE `country`
(
    id INT(6) NOT NULL,
    name VARCHAR(25) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE `province`
(
    id INT(6) NOT NULL,
    name VARCHAR(25) NOT NULL,
    idCountry INT(6) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE `canton`
(
    id INT(6) NOT NULL,
    name VARCHAR(25) NOT NULL,
    idProvince INT(6) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE `district`
(
    id INT(6) NOT NULL,
    name VARCHAR(25) NOT NULL,
    idCanton INT(6) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE `nationality`
(
    id INT(6) NOT NULL,
    name VARCHAR(25) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE `nationalityXP`
(
    idPerson INT(6) NOT NULL,
    idNationality INT(6) NOT NULL,
    PRIMARY KEY (idPerson, idNationality)
);



