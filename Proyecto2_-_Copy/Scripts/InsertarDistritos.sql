-- Crear secuencia
CREATE SEQUENCE idDistrict_seq;

-- Insertar datos en la tabla district
-- PROVINCIA SAN JOSE

-- CANTON: San Jose
INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Carmen', 1);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Merced', 1);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Hospital', 1);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Catedral', 1);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Zapote', 1);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Francisco de Dos Ros', 1);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'La Uruca', 1);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Mata Redonda', 1);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Pavas', 1);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Hatillo', 1);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Sebastin', 1);

-- CANTON: Escazu
INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Escaz', 2);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Antonio', 2);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Rafael', 2);

-- CANTON: Desamparados
INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Desamparados', 3);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Miguel', 3);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Juan de Dios', 3);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Rafael Arriba', 3);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Antonio', 3);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Frailes', 3);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Patarr', 3);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Cristbal', 3);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Rosario', 3);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Damas', 3);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Rafael Abajo', 3);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Gravilias', 3);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Los Guido', 3);

--CANTON: PURISCAL
INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Santiago', 4);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Mercedes Sur', 4);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Barbacoas', 4);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Grifo Alto', 4);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Rafael', 4);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Candelarita', 4);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Desamparaditos', 4);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Antonio', 4);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Chires', 4);

--CANTON: TARRAZU
INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Marcos', 5);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Lorenzo', 5);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Carlos', 5);

--CANTON: ASERRI
INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Aserr', 6);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Tarbaca', 6);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Vuelta de Jorco', 6);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Gabriel', 6);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Legua', 6);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Monterrey', 6);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Salitrillos', 6);

--CANTON: MORA
INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Coln', 8);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Guayabo', 8);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Tabarcia', 8);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Piedras Negras', 8);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Picagres', 8);

--CANTON: PURISCAL
INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Santiago', 4);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Mercedes Sur', 4);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Barbacoas', 4);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Grifo Alto', 4);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Rafael', 4);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Candelarita', 4);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Desamparaditos', 4);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Antonio', 4);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Chires', 4);

--CANTON: TARRAZU
INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Marcos', 5);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Lorenzo', 5);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Carlos', 5);

--CANTON: ASERRI
INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Aserr', 6);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Tarbaca', 6);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Vuelta de Jorco', 6);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Gabriel', 6);

INSERT INTO district(id, name, idCanton

--CANTON: SANTA ANA
INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Santa Ana', 10);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Salitral', 10);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Pozos', 10);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Uruca', 10);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Piedades', 10);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Brasil', 10);

--CANTON: ALAJUELITA
INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Alajuelita', 11);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Josecito', 11);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Antonio', 11);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Concepción', 11);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Felipe', 11);
--CANTON: ALAJUELITA
INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Alajuelita', 11);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Josecito', 11);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Antonio', 11);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Concepción', 11);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Felipe', 11);

--CANTON: VAZQUEZ DE CORONADO
INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Isidro', 12);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Rafael', 12);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Dulce Nombre de Jesús', 12);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Patalillo', 12);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Cascajal', 12);

--CANTON: ACOSTA
INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Ignacio', 13);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Guaitil', 13);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Palmichal', 13);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Cangrejal', 13);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Sabanillas', 13);

--CANTON: TIBAS
INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Juan', 14);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Cinco Esquinas', 14);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Anselmo Llorente', 14);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'León XIII', 14);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Colima', 14);
--CANTON: MORAVIA
INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Vicente', 15);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Jerónimo', 15);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'La Trinidad', 15);

--CANTON: MONTES DE OCA
INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Pedro', 16);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Sabanilla', 16);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Mercedes', 16);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Rafael', 16);

--CANTON: TURRUBARES
INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Pablo', 17);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Pedro', 17);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Juan de Mata', 17);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Luis', 17);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Carara', 17);

--CANTON: DOTA
INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Santa María', 18);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Jardín', 18);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Copey', 18);

--CANTON: CURRIDABAT
INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Curridabat', 19);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Granadilla', 19);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Sánchez', 19);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Tirrases', 19);

--CANTON: PEREZ ZELEDON
INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Isidro de El General', 20);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'El General', 20);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Daniel Flores', 20);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Rivas', 20);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Pedro', 20);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Platanáres', 20);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Pejibaye', 20);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Cajón', 20);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Barú', 20);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Río Nuevo', 20);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Páramo', 20);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'La Amistad', 20);

--CANTON: LEON CORTES CASTRO
INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Pablo', 21);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Andrés', 21);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Llano Bonito', 21);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Isidro', 21);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Santa Cruz', 21);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Antonio', 21);


--CANTON ALAJUELA
INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Alajuela', 22);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San José', 22);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Carrizal', 22);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Antonio', 22);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'La Guácima', 22);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Isidro', 22);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Sabanilla', 22);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Rafael', 22);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Río Segundo', 22);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Desamparados', 22);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Turrúcares', 22);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Tambor', 22);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'La Garita', 22);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Sarapiquí', 22);

--CANTON: SAN RAMÓN
INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Ramón', 24);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Santiago', 24);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Juan', 24);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Piedades Norte', 24);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Piedades Sur', 24);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Rafael', 24);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Isidro', 24);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Ángeles', 24);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Alfaro', 24);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Volio', 24);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Concepción', 24);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Zapotal', 24);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Peñas Blancas', 24);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Lorenzo', 24);

--CANTON: GRECIA
INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Grecia', 25);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Isidro', 25);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San José', 25);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Roque', 25);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Tacares', 25);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Puente de Piedra', 25);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Bolívar', 25);

--CANTON: SAN MATEO
INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Mateo', 26);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Desmonte', 26);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict)L, 'Jesús María', 26);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Labrador', 26);

--CANTON: ATENAS
INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Atenas', 27);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Jesús', 27);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Mercedes', 27);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Isidro', 27);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Concepción', 27);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San José', 27);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Santa Eulalia', 27);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Escobal', 27);

--CANTON: CARTAGO
INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Oriental', 28);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Occidental', 28);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Carmen', 28);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Nicolás', 28);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Aguacaliente', 28);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Guadalupe', 28);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Corralillo', 28);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Tierra Blanca', 28);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Dulce Nombre', 28);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Llano Grande', 28);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Quebradilla', 28);

--CANTON: LA UNION
INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Tres Ríos', 30);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Diego', 30);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Juan', 30);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Rafael', 30);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Concepción', 30);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Dulce Nombre', 30);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Ramón', 30);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Río Azul', 30);

--CANTON: HEREDIA
INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Heredia', 31);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Mercedes', 31);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Francisco', 31);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Ulloa', 31);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Varablanca', 31);

--CANTON: BARVA
INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Barva', 32);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Pedro', 32);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Pablo', 32);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Roque', 32);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Santa Lucía', 32);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San José de la Montaña', 32);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Puente Salas', 32);


--CANTON: SANTO DOMINGO
INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Santo Domingo', 33);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Vicente', 33);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'San Miguel', 33);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Paracito', 33);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Santo Tomás', 33);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Santa Rosa', 33);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Tures', 33);

INSERT INTO district(id, name, idCanton)
VALUES (NEXTVAL(iddistrict), 'Pará', 33);
