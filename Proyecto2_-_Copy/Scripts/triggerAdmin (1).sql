--Creación Triggers
--Tabla AmenityHotel
DELIMITER //

CREATE TRIGGER beforeInsertAmenityHotel BEFORE INSERT ON AmenityHotel
FOR EACH ROW
BEGIN
    SET NEW.creationBy = CURRENT_USER();
    SET NEW.creationDate = CURRENT_TIMESTAMP();
END//

CREATE TRIGGER beforeUpdateAmenityHotel BEFORE UPDATE ON AmenityHotel
FOR EACH ROW
BEGIN
    SET NEW.lastUpdate = CURRENT_TIMESTAMP();
    SET NEW.updateBy = CURRENT_USER();
END//

DELIMITER ;

--Tabla AmenityRoom
DELIMITER //

CREATE TRIGGER beforeInsertAmenityRoom BEFORE INSERT ON AmenityRoom
FOR EACH ROW
BEGIN
    SET NEW.creationBy = CURRENT_USER();
    SET NEW.creationDate = CURRENT_TIMESTAMP();
END//

CREATE TRIGGER beforeUpdateAmenityRoom BEFORE UPDATE ON AmenityRoom
FOR EACH ROW
BEGIN
    SET NEW.lastUpdate = CURRENT_TIMESTAMP();
    SET NEW.updateBy = CURRENT_USER();
END//

DELIMITER ;

--Tabla Backfeed
DELIMITER //

CREATE TRIGGER beforeInsertBackfeed BEFORE INSERT ON Backfeed
FOR EACH ROW
BEGIN
    SET NEW.creationBy = CURRENT_USER();
    SET NEW.creationDate = CURRENT_TIMESTAMP();
END//

CREATE TRIGGER beforeUpdateBackfeed BEFORE UPDATE ON Backfeed
FOR EACH ROW
BEGIN
    SET NEW.lastUpdate = CURRENT_TIMESTAMP();
    SET NEW.updateBy = CURRENT_USER();
END//

DELIMITER ;

--Tabla Booking
DELIMITER //

CREATE TRIGGER beforeInsertBooking BEFORE INSERT ON Booking
FOR EACH ROW
BEGIN
    SET NEW.creationBy = CURRENT_USER();
    SET NEW.creationDate = CURRENT_TIMESTAMP();
END//

CREATE TRIGGER beforeUpdateBooking BEFORE UPDATE ON Booking
FOR EACH ROW
BEGIN
    SET NEW.lastUpdate = CURRENT_TIMESTAMP();
    SET NEW.updateBy = CURRENT_USER();
END//

DELIMITER ;

--Tabla BookingStar
DELIMITER //

CREATE TRIGGER beforeInsertBookingStar BEFORE INSERT ON BookingStar
FOR EACH ROW
BEGIN
    SET NEW.creationBy = CURRENT_USER();
    SET NEW.creationDate = CURRENT_TIMESTAMP();
END//

CREATE TRIGGER beforeUpdateBookingStar BEFORE UPDATE ON BookingStar
FOR EACH ROW
BEGIN
    SET NEW.lastUpdate = CURRENT_TIMESTAMP();
    SET NEW.updateBy = CURRENT_USER();
END//

DELIMITER ;
--Tabla CancellacionP
DELIMITER //

CREATE TRIGGER beforeInsertCancellacionP BEFORE INSERT ON CancellacionP
FOR EACH ROW
BEGIN
    SET NEW.creationBy = USER();
    SET NEW.creationDate = NOW();
END//

CREATE TRIGGER beforeUpdateCancellacionP BEFORE UPDATE ON CancellacionP
FOR EACH ROW
BEGIN
    SET NEW.lastUpdate = NOW();
    SET NEW.updateby = USER();
END//

DELIMITER ;

--Tabla CancelPxB
DELIMITER //

CREATE TRIGGER beforeInsertCancelPxB BEFORE INSERT ON CancelPxB
FOR EACH ROW
BEGIN
    SET NEW.creationBy = USER();
    SET NEW.creationDate = NOW();
END//

CREATE TRIGGER beforeUpdateCancelPxB BEFORE UPDATE ON CancelPxB
FOR EACH ROW
BEGIN
    SET NEW.lastUpdate = NOW();
    SET NEW.updateby = USER();
END//

DELIMITER ;

--Tabla Hotel
DELIMITER //

CREATE TRIGGER beforeInsertHotel BEFORE INSERT ON Hotel
FOR EACH ROW
BEGIN
    SET NEW.creationBy = USER();
    SET NEW.creationDate = NOW();
END//

CREATE TRIGGER beforeUpdateHotel BEFORE UPDATE ON Hotel
FOR EACH ROW
BEGIN
    SET NEW.lastUpdate = NOW();
    SET NEW.updateby = USER();
END//

DELIMITER ;

--Tabla Offer
DELIMITER //

CREATE TRIGGER beforeInsertOffer BEFORE INSERT ON Offer
FOR EACH ROW
BEGIN
    SET NEW.creationBy = USER();
    SET NEW.creationDate = NOW();
END//

CREATE TRIGGER beforeUpdateOffer BEFORE UPDATE ON Offer
FOR EACH ROW
BEGIN
    SET NEW.lastUpdate = NOW();
    SET NEW.updateby = USER();
END//

DELIMITER ;

--Tabla Parameter
DELIMITER //

CREATE TRIGGER beforeInsertParameter BEFORE INSERT ON Parameter
FOR EACH ROW
BEGIN
    SET NEW.creationBy = USER();
    SET NEW.creationDate = NOW();
END//

CREATE TRIGGER beforeUpdateParameter BEFORE UPDATE ON Parameter
FOR EACH ROW
BEGIN
    SET NEW.lastUpdate = NOW();
    SET NEW.updateby = USER();
END//

DELIMITER ;

--Tabla PersonXhotelFav
DELIMITER //

CREATE TRIGGER beforeInsertPersonXhotelFav BEFORE INSERT ON PersonXhotelFav
FOR EACH ROW
BEGIN
    SET NEW.creationBy = USER();
    SET NEW.creationDate = NOW();
END//

CREATE TRIGGER beforeUpdatePersonXhotelFav BEFORE UPDATE ON PersonXhotelFav
FOR EACH ROW
BEGIN
    SET NEW.lastUpdate = NOW();
    SET NEW.updateby = USER();
END//

DELIMITER ;

--Tabla Photo
DELIMITER //

CREATE TRIGGER beforeInsertPhoto BEFORE INSERT ON Photo
FOR EACH ROW
BEGIN
    SET NEW.creationBy = USER();
    SET NEW.creationDate = NOW();
END//

CREATE TRIGGER beforeUpdatePhoto BEFORE UPDATE ON Photo
FOR EACH ROW
BEGIN
    SET NEW.lastUpdate = NOW();
    SET NEW.updateby = USER();
END//

DELIMITER ;

--Tabla Rating
DELIMITER //

CREATE TRIGGER beforeInsertRating BEFORE INSERT ON Rating
FOR EACH ROW
BEGIN
    SET NEW.creationBy = USER();
    SET NEW.creationDate = NOW();
END//

CREATE TRIGGER beforeUpdateRating BEFORE UPDATE ON Rating
FOR EACH ROW
BEGIN
    SET NEW.lastUpdate = NOW();
    SET NEW.updateby = USER();
END//

DELIMITER ;
--Tabla Room
DELIMITER //
CREATE OR REPLACE TRIGGER beforeInsertRoom
BEFORE INSERT 
ON Room
FOR EACH ROW
BEGIN 
    SET NEW.creationBy = USER();
    SET NEW.creationDate = NOW();
END; //
DELIMITER ;

DELIMITER //
CREATE OR REPLACE TRIGGER beforeUpdateRoom
BEFORE UPDATE 
ON Room
FOR EACH ROW
BEGIN 
    SET NEW.lastUpdate = NOW();
    SET NEW.updateby = USER();
END; //
DELIMITER ;

--Tabla RoomXar
DELIMITER //
CREATE OR REPLACE TRIGGER beforeInsertRoomXar
BEFORE INSERT 
ON RoomXar
FOR EACH ROW
BEGIN 
    SET NEW.creationBy = USER();
    SET NEW.creationDate = NOW();
END; //
DELIMITER ;

DELIMITER //
CREATE OR REPLACE TRIGGER beforeUpdateRoomXar
BEFORE UPDATE 
ON RoomXar
FOR EACH ROW
BEGIN 
    SET NEW.lastUpdate = NOW();
    SET NEW.updateby = USER();
END; //
DELIMITER ;

--Tabla TypeOfPaymentm
DELIMITER //
CREATE OR REPLACE TRIGGER beforeInsertTypeOfPaymentm
BEFORE INSERT 
ON TypeOfPaymentm
FOR EACH ROW
BEGIN 
    SET NEW.creationBy = USER();
    SET NEW.creationDate = NOW();
END; //
DELIMITER ;

DELIMITER //
CREATE OR REPLACE TRIGGER beforeUpdateTypeOfPaymentm
BEFORE UPDATE 
ON TypeOfPaymentm
FOR EACH ROW
BEGIN 
    SET NEW.lastUpdate = NOW();
    SET NEW.updateby = USER();
END; //
DELIMITER ;

--Tabla TypeOfPayMXB
DELIMITER //
CREATE OR REPLACE TRIGGER beforeInsertTypeOfPayMXB
BEFORE INSERT 
ON TypeOfPayMXB
FOR EACH ROW
BEGIN 
    SET NEW.creationBy = USER();
    SET NEW.creationDate = NOW();
END; //
DELIMITER ;

DELIMITER //
CREATE OR REPLACE TRIGGER beforeUpdateTypeOfPayMXB
BEFORE UPDATE 
ON TypeOfPayMXB
FOR EACH ROW
BEGIN 
    SET NEW.lastUpdate = NOW();
    SET NEW.updateby = USER();
END; //
DELIMITER ;
